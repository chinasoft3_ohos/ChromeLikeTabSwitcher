## 1.0.2

更新图片效果

## 1.0.1

更新图片效果

## 1.0.0

发布1.0.0 release版本

## 0.0.1-SNAPSHOT

ChromeLikeTabSwitcher组件openharmony第一个beta版本

#### api差异

* ToolBar相关API未找到，使用DirectionalLayout仿Toolbar重新实现相关功能
* SnackBar相关API未找到，使用StackLayout实现相关逻辑


#### 已实现功能

* 该库基本功能已全部实现

#### 未实现功能

* Tab页面滑动显示时，预览图片无法展示
* 点9图无法加载，导致Tab与Tab之间阴影层级效果无法显示，使用间隔线替代
* Tab滑动到顶部或者底部时，视觉差Z轴偏转效果无法实现
* 下拉手势显示所有Tab页面的功能无法实现
* 左右滑动标题 Swipe手势切换Tab页功能