package de.mrapp.tabswitcher.example;

import de.mrapp.tabswitcher.colorUi.util.SharedPreferencesMgr;
import de.mrapp.tabswitcher.util.PixelUtil;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        PixelUtil.initContext(getApplicationContext());
        SharedPreferencesMgr.getInstance(this);
    }
}
