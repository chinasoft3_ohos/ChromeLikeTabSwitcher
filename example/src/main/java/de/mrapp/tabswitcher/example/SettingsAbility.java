package de.mrapp.tabswitcher.example;

import de.mrapp.tabswitcher.example.slice.SettingsAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SettingsAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SettingsAbilitySlice.class.getName());
    }
}
