/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.example.adapter;

import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.*;

/**
 * 自定义Array适配器
 *
 * @author: zhu_zhonglin
 * @since: 2021/4/26
 */
public class CustomArrayAdapter<T> extends BaseItemProvider {
    private final Object mLock = new Object();

    private final int mResource;

    private final LayoutScatter mInflater;

    private final Context mContext;

    private final List<T> mObjects;

    private final boolean mDarkTheme;

    public CustomArrayAdapter(Context context, int resource) {
        this(context, resource, new ArrayList<>(), false);
    }

    public CustomArrayAdapter(Context context, int resource, List<T> objects) {
        this(context, resource, objects, false);
    }

    public CustomArrayAdapter(Context context, int resource, List<T> objects, boolean darkTheme) {
        super();
        mContext = context;
        mInflater = LayoutScatter.getInstance(context);
        mResource = resource;
        mObjects = objects;
        mDarkTheme = darkTheme;
    }

    /**
     * 添加item
     * @param object item对象
     */
    public void add(T object) {
        synchronized (mLock) {
            mObjects.add(object);
        }
        notifyDataChanged();
    }

    /**
     * 添加多个items
     * @param collection T 集合对象
     */
    public void addAll(Collection<? extends T> collection) {
        synchronized (mLock) {
            mObjects.addAll(collection);
        }
        notifyDataChanged();
    }

    /**
     * 插入item
     * @param object item对象
     * @param index index下标
     */
    public void insert(T object, int index) {
        synchronized (mLock) {
            mObjects.add(index, object);
        }
        notifyDataChanged();
    }

    /**
     * 移除item
     * @param object item对象
     */
    public void remove(T object) {
        synchronized (mLock) {
            mObjects.remove(object);
        }
        notifyDataChanged();
    }

    /**
     * 获取上下文
     * @return Context对象
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * 是否为空
     * @return adapter中是否为空
     */
    public boolean isEmpty() {
        return getCount() == 0;
    }

    @Override
    public int getCount() {
        return mObjects.size();
    }

    @Override
    public T getItem(int position) {
        return mObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        final Component view;
        final Text text;

        if (convertView == null) {
            view = mInflater.parse(mResource, parent, false);
        } else {
            view = convertView;
        }

        try {
            text = (Text) view;
        } catch (ClassCastException e) {
            throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", e);
        }

        final T item = getItem(position);
        if (item instanceof String) {
            text.setText((String) item);
        } else {
            text.setText(item.toString());
        }

        if(mDarkTheme) {
            text.setTextColor(Color.WHITE);
        }else {
            text.setTextColor(new Color(Color.getIntColor("#666666")));
        }
        return view;
    }
}
