/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.example.adapter;

import de.mrapp.tabswitcher.Tab;
import de.mrapp.tabswitcher.example.ResourceTable;
import de.mrapp.ohos_util.multithreading.AbstractDataBinder;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

import java.util.Arrays;
import java.util.Locale;

public class DataBinder extends AbstractDataBinder<CustomArrayAdapter<String>, Tab, ListContainer, Void> {
    private boolean darkTheme;
    public DataBinder(final Context context, boolean darkTheme) {
        super(context.getApplicationContext());
        this.darkTheme = darkTheme;
    }


    @Override
    protected CustomArrayAdapter<String> doInBackground(final Tab key,
                                                        final Void... params) {
        String[] array = new String[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = String.format(Locale.getDefault(), "%s, item %d", key.getTitle(), i + 1);
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        return new CustomArrayAdapter<>(getContext(), ResourceTable.Layout_simple_list_item_1, Arrays.asList(array), darkTheme);
    }

    @Override
    protected void onPostExecute(final ListContainer view,
                                 final CustomArrayAdapter<String> data, final long duration,
                                 final Void... params) {
        if (data != null) {
            view.setItemProvider(data);
        }
    }
}
