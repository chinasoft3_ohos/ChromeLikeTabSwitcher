package de.mrapp.tabswitcher.example.base;

import de.mrapp.tabswitcher.example.ResourceTable;
import de.mrapp.tabswitcher.colorUi.util.ColorUiUtil;
import de.mrapp.tabswitcher.colorUi.util.SharedPreferencesMgr;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public abstract class BaseAbilitySlice extends AbilitySlice {

    protected abstract int getLayoutId();
    protected Component getRootView() {
        return findComponentById(ResourceTable.Id_root);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(getLayoutId());

        int theme = SharedPreferencesMgr.getInstance(this).getInt("theme", 0);
        if(theme == 1) {
            setTheme(ResourceTable.Theme_dark_theme);
        } else {
            setTheme(ResourceTable.Theme_light_theme);
        }
        ColorUiUtil.changeTheme(getRootView(), getTheme());
    }
}
