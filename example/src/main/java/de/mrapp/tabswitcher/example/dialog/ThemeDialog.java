/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.example.dialog;

import de.mrapp.tabswitcher.example.ResourceTable;
import de.mrapp.tabswitcher.colorUi.util.ColorUiUtil;
import de.mrapp.tabswitcher.colorUi.util.SharedPreferencesMgr;
import de.mrapp.tabswitcher.util.PixelUtil;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.global.resource.Resource;

import java.util.Optional;

public class ThemeDialog extends CommonDialog implements Component.ClickedListener {
    private ComponentContainer mComponent;
    private Context mContext;
    private Ability mAbility;
    private Image light_iv, dark_iv;
    private int theme;
    private DialogCallback mCallback;

    public Ability getOwnerAbility() {
        return mAbility;
    }

    public ThemeDialog(Context context, DialogCallback callback) {
        super(context);
        mContext = context;
        if (context instanceof Ability) {
            mAbility = (Ability) context;
        }
        this.mCallback = callback;
        theme = SharedPreferencesMgr.getInstance(context).getInt("theme", 0);
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        mComponent = (ComponentContainer) LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_theme_dialog, null, false);
        mComponent.findComponentById(ResourceTable.Id_theme_dialog_cancel).setClickedListener(this::onClick);
        mComponent.findComponentById(ResourceTable.Id_theme_light).setClickedListener(this::onClick);
        mComponent.findComponentById(ResourceTable.Id_theme_dark).setClickedListener(this::onClick);
        light_iv = (Image) mComponent.findComponentById(ResourceTable.Id_light_iv);
        dark_iv = (Image) mComponent.findComponentById(ResourceTable.Id_dark_iv);
        if (theme == 0) {
            light_iv.setImageElement(convertResToElement(mContext, ResourceTable.Media_theme_check_selected));
            dark_iv.setImageElement(convertResToElement(mContext, ResourceTable.Media_theme_check_unselected_dark));
        } else {
            light_iv.setImageElement(convertResToElement(mContext, ResourceTable.Media_theme_check_unselected_light));
            dark_iv.setImageElement(convertResToElement(mContext, ResourceTable.Media_theme_check_selected));
        }
        setContentCustomComponent(mComponent);

    }

    private Element convertResToElement(Context mContext, int drawableRes) {
        Element element = null;
        try {
            Resource resource = mContext.getResourceManager().getResource(drawableRes);
            element = new PixelMapElement(resource);
            return element;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return element;
    }

    @Override
    public void show() {
        super.show();
        Optional<WindowManager.LayoutConfig> layoutConfigOptional = getWindow().getLayoutConfig();
        if (layoutConfigOptional.isPresent()) {
            WindowManager.LayoutConfig layoutConfig = layoutConfigOptional.get();
            layoutConfig.width = (int) (PixelUtil.screenWidth() * 0.9f);
            layoutConfig.height = (int) (PixelUtil.fp2px(170));
            layoutConfig.alignment = LayoutAlignment.CENTER;
            getWindow().setLayoutConfig(layoutConfig);
        }
        setDialogListener(new DialogListener() {
            @Override
            public boolean isTouchOutside() {
                hide();
                return true;
            }
        });
    }


    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_theme_dialog_cancel:
                this.hide();
                break;
            case ResourceTable.Id_theme_light:
                theme = SharedPreferencesMgr.getInstance(mContext).getInt("theme", 0);
                hide();
                if(theme == 0)
                    return;
                if(mCallback != null) {
                    mCallback.click(0);
                }
                break;
            case ResourceTable.Id_theme_dark:
                theme = SharedPreferencesMgr.getInstance(mContext).getInt("theme", 0);
                hide();
                if(theme == 1)
                    return;
                if(mCallback != null) {
                    mCallback.click(1);
                }
                break;
        }
    }
}