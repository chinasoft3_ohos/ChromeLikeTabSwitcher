package de.mrapp.tabswitcher.example.slice;

import de.mrapp.tabswitcher.example.ResourceTable;
import de.mrapp.tabswitcher.example.base.BaseAbilitySlice;
import de.mrapp.tabswitcher.colorUi.util.ColorUiUtil;
import de.mrapp.tabswitcher.colorUi.util.SharedPreferencesMgr;
import de.mrapp.tabswitcher.example.dialog.DialogCallback;
import de.mrapp.tabswitcher.example.dialog.ThemeDialog;
import de.mrapp.tabswitcher.util.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

public class SettingsAbilitySlice extends BaseAbilitySlice implements Component.ClickedListener {
    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_ability_settings;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().setStatusBarColor(Color.getIntColor("#FF303F9F"));
        initView();
    }

    private void initView() {
        findComponentById(ResourceTable.Id_settings_back_iv).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_item_theme).setClickedListener(this::onClick);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_settings_back_iv:
                onBackPressed();
                break;
            case ResourceTable.Id_item_theme:
                new ThemeDialog(getContext(), new DialogCallback() {
                    @Override
                    public void click(int type) {
                        if (type == 0) {
                            SharedPreferencesMgr.getInstance(getContext()).setInt("theme", 0);
                            setTheme(ResourceTable.Theme_light_theme);
                        } else {
                            SharedPreferencesMgr.getInstance(getContext()).setInt("theme", 1);
                            setTheme(ResourceTable.Theme_dark_theme);
                        }
                        ColorUiUtil.changeTheme(getRootView(), getTheme());
                    }
                }).show();
                break;
        }
    }
}
