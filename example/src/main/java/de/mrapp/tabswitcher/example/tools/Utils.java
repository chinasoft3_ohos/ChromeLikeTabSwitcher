/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.example.tools;

import ohos.agp.components.AttrSet;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.lang.reflect.Method;



public class Utils {

    private static final String TAG = "RippleView";
    private static HiLogLabel sLogLabel;
    private static final int DOMAIN = 0x001;

    private static HiLogLabel createLogLabel() {
        if (sLogLabel == null) {
            sLogLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, TAG);
        }
        return sLogLabel;
    }

    public static void info(String format, Object... objects) {
        HiLog.info(createLogLabel(), format, objects);
    }

    public static void debug(String format, Object... objects) {
        HiLog.debug(createLogLabel(), format, objects);
    }

    public static void error(String format, Object... objects) {
        HiLog.error(createLogLabel(), format, objects);
    }

    public static void warn(String format, Object... objects) {
        HiLog.warn(createLogLabel(), format, objects);
    }

    public static void fatal(String format, Object... objects) {
        HiLog.fatal(createLogLabel(), format, objects);
    }

    public static Integer getIntegerFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
        return value;
    }

    public static float getFloatFromAttr(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static Integer getDimensionFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getDimensionValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
        return value;
    }

    public static int getColorFromAttr(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
        return value;
    }

    public static boolean getBooleanFromAttr(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
        return value;
    }

    public static int getMax(int... radius) {
        if (radius.length == 0) {
            return 0;
        }
        int max = radius[0];
        for (int m : radius) {
            if (m > max) {
                max = m;
            }
        }
        return max;
    }

    public static int red(int color) {
        return (color >> 16) & 0xFF;
    }

    public static int green(int color) {
        return (color >> 8) & 0xFF;
    }

    public static int blue(int color) {
        return color & 0xFF;
    }

    public static String invokeStringValue(String property, String defaultValue) {
        String value = defaultValue;
        try {
            Class clazz = Class.forName("android.os.SystemProperties");
            Method method = clazz.getMethod("get", String.class, String.class);
            value = (String) method.invoke(null, property, defaultValue);
        } catch (ClassNotFoundException classnotfoundexception) {
            error("occured ClassNotFoundException");
        } catch (NoSuchMethodException nosuchmethodexception) {
            error("occured NoSuchMethodException");
        } catch (Exception exception) {
            error("occured Exception");
        }
        return value;
    }

}
