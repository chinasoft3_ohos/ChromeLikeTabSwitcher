/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.example;

import de.mrapp.ohos_util.ArrayUtil;
import de.mrapp.ohos_util.gesture.DragHelper;
import de.mrapp.tabswitcher.example.tools.Utils;
import de.mrapp.util.ClassUtil;
import de.mrapp.util.datastructure.ListenerList;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;

/**
 * ChromeLikeTabSwitcherTest
 *
 * @author: zhu_zhonglin
 * @since: 2021/4/26
 */
public class ChromeLikeTabSwitcherTest {
    /**
     * testArrayUtilindexOf
     */
    @Test
    public void testArrayUtilindexOf() {
        boolean[] booleans = new boolean[]{true, true, false, false, true, false};
        int result = ArrayUtil.indexOf(booleans, false);
        Assert.assertEquals(result, 2);
    }

    /**
     * testArrayUtillastIndexOf
     */
    @Test
    public void testArrayUtillastIndexOf() {
        boolean[] booleans = new boolean[]{true, true, false, false, true, false};
        int result = ArrayUtil.lastIndexOf(booleans, false);
        Assert.assertEquals(result, 5);
    }

    /**
     * testArraycontains
     */
    @Test
    public void testArraycontains() {
        boolean[] booleans = new boolean[]{true, true, false, false, true, false};
        boolean contains = ArrayUtil.contains(booleans, true);
        Assert.assertEquals(true, contains);
    }

    /**
     * testUtilsGetMax
     */
    @Test
    public void testUtilsGetMax() {
        int max = Utils.getMax(2, 1, 5, 7, 8, 3, 8, 20, 22, 13);
        Assert.assertEquals(max, 22);
    }

    /**
     * testListenerListAdd
     */
    @Test
    public void testListenerListAdd() {
        ListenerList<String> strings = new ListenerList<>();
        boolean add = strings.add("111");
        Assert.assertEquals(add, true);
    }

    /**
     * testListenerListContains
     */
    @Test
    public void testListenerListContains() {
        String text = "111";
        ListenerList<String> strings = new ListenerList<>();
        strings.add(text);
        boolean empty = strings.isEmpty();
        Assert.assertEquals(empty, false);
    }

    /**
     * testListenerListSize
     */
    @Test
    public void testListenerListSize() {
        String text = "111";
        ListenerList<String> strings = new ListenerList<>();
        strings.add(text);
        int size = strings.size();
        Assert.assertEquals(size, 1);
    }

    /**
     * testListenerListAddAll
     */
    @Test
    public void testListenerListAddAll() {
        String text = "111";
        String text2 = "1112";
        ListenerList<String> strings = new ListenerList<>();
        ListenerList<String> strings2 = new ListenerList<>();
        strings2.add(text);
        strings2.add(text2);
        strings.addAll(strings2);
        int size = strings.size();
        Assert.assertEquals(size, 2);
    }

    /**
     * testListenerListRemove
     */
    @Test
    public void testListenerListRemove() {
        String text = "111";
        ListenerList<String> strings = new ListenerList<>();
        strings.add(text);
        boolean remove = strings.remove(text);
        Assert.assertEquals(remove, true);
    }

    /**
     * testListenerListRemoveAll
     */
    @Test
    public void testListenerListRemoveAll() {
        String text = "111";
        String text2 = "1112";
        ListenerList<String> strings = new ListenerList<>();
        strings.add(text);
        strings.add(text2);
        ListenerList<String> strings2 = new ListenerList<>();
        strings2.add(text);
        strings.removeAll(strings2);
        int size = strings.size();
        Assert.assertEquals(size, 1);
    }

    /**
     * testListenerListClear
     */
    @Test
    public void testListenerListClear() {
        String text = "111";
        ListenerList<String> strings = new ListenerList<>();
        strings.add(text);
        strings.clear();
        int size = strings.size();
        Assert.assertEquals(size, 0);
    }

    /**
     * testListenerListgetAll
     */
    @Test
    public void testListenerListgetAll() {
        String text = "111";
        ListenerList<String> strings = new ListenerList<>();
        strings.add(text);
        Collection<String> all = strings.getAll();
        int size = all.size();

        Assert.assertEquals(size, 1);
    }

    /**
     * testClassUtilGetTruncatedName
     */
    @Test
    public void testClassUtilGetTruncatedName() {
        String truncatedName = ClassUtil.getTruncatedName(String.class);
        Assert.assertEquals(truncatedName, "j.l.String");
    }

    /**
     * testDragHelpergetThreshold
     */
    @Test
    public void testDragHelpergetThreshold() {
        DragHelper dragHelper = new DragHelper(10);
        int threshold = dragHelper.getThreshold();
        Assert.assertEquals(threshold, 10);
    }

    /**
     * testDragHelperreset
     */
    @Test
    public void testDragHelperreset() {
        DragHelper dragHelper = new DragHelper(10);
        dragHelper.reset(11);
        int threshold = dragHelper.getThreshold();
        Assert.assertEquals(11, threshold);
    }

    /**
     * testsetMaxDragDistance
     */
    @Test
    public void testsetMaxDragDistance() {
        DragHelper dragHelper = new DragHelper(10);
        dragHelper.setMaxDragDistance(100);
        float maxDragDistance = dragHelper.getMaxDragDistance();
        float value = 100f;
        Assert.assertEquals(value, maxDragDistance, 0);
    }

    /**
     * testsetMinDragDistance
     */
    @Test
    public void testsetMinDragDistance() {
        DragHelper dragHelper = new DragHelper(10);
        dragHelper.setMinDragDistance(-20);
        float minDragDistance = dragHelper.getMinDragDistance();
        float value = -20f;
        Assert.assertEquals(value, minDragDistance, 0);
    }
}
