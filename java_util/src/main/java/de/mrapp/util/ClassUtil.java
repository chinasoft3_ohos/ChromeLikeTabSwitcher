package de.mrapp.util;

public class ClassUtil {
    public static String getTruncatedName(Class clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("The class may not be null");
        }
        String fullQualifiedName = clazz.getName();
        String[] qualifiers = fullQualifiedName.split("\\.");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < qualifiers.length; i++) {
            if (i != qualifiers.length - 1) {
                stringBuilder.append(qualifiers[i].substring(0, 1));
                stringBuilder.append(".");
            } else {
                stringBuilder.append(qualifiers[i]);
            }
        }

        return stringBuilder.toString();
    }
}
