package de.mrapp.util;


import java.io.File;
import java.lang.reflect.Constructor;

public class Condition {
    public static final Condition INSTANCE;

    private final void throwException(String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        RuntimeException var4;
        try {
            Constructor constructor = exceptionClass.getConstructor(String.class);
            var4 = (RuntimeException) constructor.newInstance(exceptionMessage);
        } catch (Exception var6) {
            var4 = new RuntimeException(exceptionMessage);
        }
        throw var4;
    }

    public final void ensureTrue(boolean expression, String exceptionMessage) {
        this.ensureTrue(expression, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureTrue(boolean expression, String exceptionMessage, Class exceptionClass) {
        if (!expression) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureFalse(boolean expression, String exceptionMessage) {
        this.ensureFalse(expression, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureFalse(boolean expression, String exceptionMessage, Class exceptionClass) {
        if (expression) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureEqual(Object object1, Object object2, String exceptionMessage) {
        this.ensureEqual(object1, object2, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureEqual(Object object1, Object object2, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        if (object1 == null && object2 != null || object1 != null && object1 != object2) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureNotEqual(Object object1, Object object2, String exceptionMessage) {
        this.ensureNotEqual(object1, object2, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureNotEqual(Object object1, Object object2, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        if (object1 == null && object2 == null || object1 != null && object1 == object2) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureNotNull(Object obj, String exceptionMessage) {
        this.ensureNotNull(obj, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureNotNull(Object obj, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        if (obj == null) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureNotEmpty(CharSequence text, String exceptionMessage) {
        this.ensureNotEmpty(text, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureNotEmpty(CharSequence text, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {

        if (text == null || text.length() == 0) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureHasText(CharSequence text, String exceptionMessage) {
        this.ensureHasText(text, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureHasText(CharSequence text, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        if (text == null || text.length() == 0 || text.toString().matches("\\s+")) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtLeast(short value, short referenceValue, String exceptionMessage) {
        this.ensureAtLeast(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtLeast(short value, short referenceValue, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        if (value < referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtLeast(int value, int referenceValue, String exceptionMessage) {
        this.ensureAtLeast(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtLeast(int value, int referenceValue, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        if (value < referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtLeast(long value, long referenceValue, String exceptionMessage) {
        this.ensureAtLeast(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtLeast(long value, long referenceValue, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        if (value < referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtLeast(float value, float referenceValue, String exceptionMessage) {
        this.ensureAtLeast(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtLeast(float value, float referenceValue, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        if (value < referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtLeast(double value, double referenceValue, String exceptionMessage) {
        this.ensureAtLeast(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtLeast(double value, double referenceValue, String exceptionMessage, Class<? extends RuntimeException> exceptionClass) {
        if (value < referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtMaximum(short value, short referenceValue, String exceptionMessage) {
        this.ensureAtMaximum(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtMaximum(short value, short referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value > referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtMaximum(int value, int referenceValue, String exceptionMessage) {
        this.ensureAtMaximum(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtMaximum(int value, int referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value > referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtMaximum(long value, long referenceValue, String exceptionMessage) {
        this.ensureAtMaximum(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtMaximum(long value, long referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value > referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtMaximum(float value, float referenceValue, String exceptionMessage) {
        this.ensureAtMaximum(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtMaximum(float value, float referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value > referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureAtMaximum(double value, double referenceValue, String exceptionMessage) {
        this.ensureAtMaximum(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureAtMaximum(double value, double referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value > referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureGreater(short value, short referenceValue, String exceptionMessage) {
        this.ensureGreater(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureGreater(short value, short referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value <= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureGreater(int value, int referenceValue, String exceptionMessage) {
        this.ensureGreater(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureGreater(int value, int referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value <= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureGreater(long value, long referenceValue, String exceptionMessage) {
        this.ensureGreater(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureGreater(long value, long referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value <= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureGreater(float value, float referenceValue, String exceptionMessage) {
        this.ensureGreater(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureGreater(float value, float referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value <= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureGreater(double value, double referenceValue, String exceptionMessage) {
        this.ensureGreater(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureGreater(double value, double referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value <= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureSmaller(short value, short referenceValue, String exceptionMessage) {
        this.ensureSmaller(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureSmaller(short value, short referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value >= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureSmaller(int value, int referenceValue, String exceptionMessage) {
        this.ensureSmaller(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureSmaller(int value, int referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value >= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureSmaller(long value, long referenceValue, String exceptionMessage) {
        this.ensureSmaller(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureSmaller(long value, long referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value >= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureSmaller(float value, float referenceValue, String exceptionMessage) {
        this.ensureSmaller(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureSmaller(float value, float referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value >= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureSmaller(double value, double referenceValue, String exceptionMessage) {
        this.ensureSmaller(value, referenceValue, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureSmaller(double value, double referenceValue, String exceptionMessage, Class exceptionClass) {
        if (value >= referenceValue) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureNotEmpty(Iterable iterable, String exceptionMessage) {
        this.ensureNotEmpty(iterable, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureNotEmpty(Iterable iterable, String exceptionMessage, Class exceptionClass) {
        if (!iterable.iterator().hasNext()) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureFileExists(File file, String exceptionMessage) {
        this.ensureFileExists(file, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureFileExists(File file, String exceptionMessage, Class exceptionClass) {

        if (!file.exists()) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureFileIsDirectory(File file, String exceptionMessage) {
        this.ensureFileIsDirectory(file, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureFileIsDirectory(File file, String exceptionMessage, Class exceptionClass) {
        if (!file.isDirectory()) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    public final void ensureFileIsNoDirectory(File file, String exceptionMessage) {
        this.ensureFileIsNoDirectory(file, exceptionMessage, IllegalArgumentException.class);
    }

    public final void ensureFileIsNoDirectory(File file, String exceptionMessage, Class exceptionClass) {
        if (!file.isFile()) {
            this.throwException(exceptionMessage, exceptionClass);
        }

    }

    private Condition() {
    }

    static {
        Condition var0 = new Condition();
        INSTANCE = var0;
    }


}
