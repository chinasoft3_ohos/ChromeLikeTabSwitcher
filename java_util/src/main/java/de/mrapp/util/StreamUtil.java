package de.mrapp.util;

import java.io.Closeable;
import java.io.IOException;

public class StreamUtil {
    public final void close(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                // No need to handle
            }
        }
    }
}
