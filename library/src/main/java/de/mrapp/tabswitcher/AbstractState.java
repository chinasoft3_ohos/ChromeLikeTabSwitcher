package de.mrapp.tabswitcher;

import de.mrapp.tabswitcher.model.Restorable;
import de.mrapp.util.Condition;
import ohos.utils.PacMap;

public abstract class AbstractState implements Restorable {
    /**
     * The tab, the state corresponds to.
     */
    private final Tab tab;

    /**
     * Creates a new state of a specific tab.
     *
     * @param tab The tab, the state should correspond to, as an instance of the class {@link Tab}. The
     *            tab may not be null
     */
    public AbstractState(final Tab tab) {
        Condition.INSTANCE.ensureNotNull(tab, "The tab may not be null");
        this.tab = tab;
    }

    /**
     * Returns the tab, the state corresponds to.
     *
     * @return The tab, the state corresponds to, as an instance of the class {@link Tab}. The tab
     * may not be null
     */

    public Tab getTab() {
        return tab;
    }

    @Override
    public void saveInstanceState(final PacMap outState) {

    }

    @Override
    public void restoreInstanceState(final PacMap savedInstanceState) {

    }

}
