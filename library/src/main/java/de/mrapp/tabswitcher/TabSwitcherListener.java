package de.mrapp.tabswitcher;


public interface TabSwitcherListener {
    /**
     * The method, which is invoked, when the tab switcher has been shown.
     *
     * @param tabSwitcher The observed tab switcher as an instance of the class {@link TabSwitcher}. The tab
     *                    switcher may not be null
     */
    void onSwitcherShown(TabSwitcher tabSwitcher);

    /**
     * The method, which is invoked, when the tab switcher has been hidden.
     *
     * @param tabSwitcher The observed tab switcher as an instance of the class {@link TabSwitcher}. The tab
     *                    switcher may not be null
     */
    void onSwitcherHidden(TabSwitcher tabSwitcher);

    /**
     * The method, which is invoked, when the currently selected tab has been changed.
     *
     * @param tabSwitcher      The observed tab switcher as an instance of the class {@link TabSwitcher}. The tab
     *                         switcher may not be null
     * @param selectedTabIndex The index of the currently selected tab as an {@link Integer} value or -1, if the tab
     *                         switcher does not contain any tabs
     * @param selectedTab      The currently selected tab as an instance of the class {@link Tab} or null, if the
     *                         tab switcher does not contain any tabs
     */
    void onSelectionChanged(TabSwitcher tabSwitcher, int selectedTabIndex,
                            Tab selectedTab);

    /**
     * The method, which is invoked, when a tab has been added to the tab switcher.
     *
     * @param tabSwitcher The observed tab switcher as an instance of the class {@link TabSwitcher}. The tab
     *                    switcher may not be null
     * @param index       The index of the tab, which has been added, as an {@link Integer} value
     * @param tab         The tab, which has been added, as an instance of the class {@link Tab}. The tab may
     *                    not be null
     * @param animation   The animation, which has been used to add the tab, as an instance of the class {@link
     *                    Animation}. The animation may not be null
     */
    void onTabAdded(TabSwitcher tabSwitcher, int index, Tab tab,
                    Animation animation);

    /**
     * The method, which is invoked, when a tab has been removed from the tab switcher.
     *
     * @param tabSwitcher The observed tab switcher as an instance of the class {@link TabSwitcher}. The tab
     *                    switcher may not be null
     * @param index       The index of the tab, which has been removed, as an {@link Integer} value
     * @param tab         The tab, which has been removed, as an instance of the class {@link Tab}. The tab may
     *                    not be null
     * @param animation   The animation, which has been used to remove the tab, as an instance of the class
     *                    {@link Animation}. The animation may not be null
     */
    void onTabRemoved(TabSwitcher tabSwitcher, int index, Tab tab,
                      Animation animation);

    /**
     * The method, which is invoked, when all tabs have been removed from the tab switcher.
     *
     * @param tabSwitcher The observed tab switcher as an instance of the class {@link TabSwitcher}. The tab
     *                    switcher may not be null
     * @param tabs        An array, which contains the tabs, which have been removed, as an array of the type
     *                    {@link Tab} or an empty array, if no tabs have been removed
     * @param animation   The animation, which has been used to remove the tabs, as an instance of the class
     *                    {@link Animation}. The animation may not be null
     */
    void onAllTabsRemoved(TabSwitcher tabSwitcher, Tab[] tabs,
                          Animation animation);
}
