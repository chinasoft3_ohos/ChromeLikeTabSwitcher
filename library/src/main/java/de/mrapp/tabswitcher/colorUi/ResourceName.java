/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.colorUi;

public class ResourceName {
    /**
     * ATTR_NAME
     */
    public static final String ATTR_NAME = "theme_prefix";
    /**
     * BACKGROUND
     */
    public static final String BACKGROUND = "background";
    /**
     * IMAGE_SRC
     */
    public static final String IMAGE_SRC = "image_src";
    /**
     * TEXT_COLOR
     */
    public static final String TEXT_COLOR = "text_color";
    /**
     * TEXT_SIZE
     */
    public static final String TEXT_SIZE = "text_size";
    /**
     * DIVIDER
     */
    public static final String DIVIDER = "divider";
}
