/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.colorUi.util;

import de.mrapp.tabswitcher.colorUi.ColorUiInterface;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.global.resource.solidxml.Theme;


/**
 * Created by chengli on 15/6/10.
 */
public class ColorUiUtil {
    /**
     * 切换应用主题
     *
     * @param rootView rootView根
     * @param theme theme主题
     */
    public static void changeTheme(Component rootView, Theme theme) {
        if (rootView instanceof ColorUiInterface) {
            ((ColorUiInterface) rootView).setTheme(theme);
            if (rootView instanceof ComponentContainer) {
                int count = ((ComponentContainer) rootView).getChildCount();
                for (int i = 0; i < count; i++) {
                    changeTheme(((ComponentContainer) rootView).getComponentAt(i), theme);
                }
            }
        } else {
            if (rootView instanceof ComponentContainer) {
                int count = ((ComponentContainer) rootView).getChildCount();
                for (int i = 0; i < count; i++) {
                    changeTheme(((ComponentContainer) rootView).getComponentAt(i), theme);
                }
            }
        }
    }


}
