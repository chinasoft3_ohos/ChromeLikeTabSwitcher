/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.colorUi.util;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

/**
 * SharedPreferences管理类
 */
public class SharedPreferencesMgr {
    private static SharedPreferencesMgr sharedPreferencesUtils;
    /**
     * 保存在手机里面的文件名
     */
    private String FILE_NAME = "derson";

    private Preferences sPrefs;

    /**
     * Perferences 单例
     *
     * @param context context上下文
     * @return 本地SP对象
     */
    public static SharedPreferencesMgr getInstance(Context context) {
        if (sharedPreferencesUtils == null) {
            sharedPreferencesUtils = new SharedPreferencesMgr(context);
        }
        return sharedPreferencesUtils;
    }

    private SharedPreferencesMgr(Context context) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        sPrefs = databaseHelper.getPreferences(FILE_NAME);
    }

    /**
     * getInt
     * @param key key
     * @param defaultValue 默认值
     * @return int数据
     */
    public int getInt(String key, int defaultValue) {
        return sPrefs.getInt(key, defaultValue);
    }

    /**
     * setInt
     * @param key key
     * @param value value值
     */
    public void setInt(String key, int value) {
        sPrefs.putInt(key, value).flushSync();
    }

    /**
     * getBoolean
     * @param key key
     * @param defaultValue 默认值
     * @return boolean数据
     */
    public boolean getBoolean(String key, boolean defaultValue) {
        return sPrefs.getBoolean(key, defaultValue);
    }

    /**
     * 设置setBoolean
     * @param key key字符
     * @param value value值
     */
    public void setBoolean(String key, boolean value) {
        sPrefs.putBoolean(key, value).flushSync();
    }

    /**
     * getString 获取String数据
     * @param key key
     * @param defaultValue defaultValue默认
     * @return String字符
     */
    public String getString(String key, String defaultValue) {
        if (sPrefs == null) {
            return defaultValue;
        }
        return sPrefs.getString(key, defaultValue);
    }

    /**
     * 设置string
     *
     * @param key   key
     * @param value value
     */
    public void setString(String key, String value) {
        if (sPrefs == null) {
            return;
        }
        sPrefs.putString(key, value).flushSync();
    }

    /**
     * 清除所有
     */
    public void clearAll() {
        if (sPrefs == null) {
            return;
        }
        sPrefs.clear().flushSync();
    }
}
