/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.colorUi.util;

import de.mrapp.tabswitcher.colorUi.ColorUiInterface;
import de.mrapp.tabswitcher.colorUi.ResourceName;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.global.resource.solidxml.Theme;
import ohos.global.resource.solidxml.TypedAttribute;

import java.io.IOException;
import java.util.Optional;

/**
 * Created by chengli on 15/6/8.
 */
public class ViewAttributeUtil {

    /**
     * 获取Attribute
     * @param attr attr属性
     * @return AttributeValue
     */
    public static String getAttributeValue(AttrSet attr) {
        Optional<Attr> op = attr.getAttr(ResourceName.ATTR_NAME);
        if (op.isPresent()) {
            return op.get().getStringValue();
        }
        return "";
    }

    /**
     * buildParam
     * @param param param
     * @param attr attr属性
     * @return param
     */
    public static String buildParam(String param, String attr) {
        return attr.length() <= 0 ? param : (attr + "_" + param);
    }

    /**
     * 应用背景图片
     * @param ci ColorUiInterface
     * @param theme Theme主题
     * @param attr attr属性
     */
    public static void applyBackgroundDrawable(ColorUiInterface ci, Theme theme, String attr) {
        try {
            String key = buildParam(ResourceName.BACKGROUND, attr);
            if (theme.getThemeHash() == null || theme.getThemeHash().get(key) == null) {
                return;
            }
            TypedAttribute ta = theme.getValue(key);
            String origin = ta.getParsedValue();
            if (origin.contains("/graphic/")) {
                (ci.getView()).setBackground(
                        new ShapeElement(ci.getView().getContext(), ta.getResId()));
            } else if (ta.getType() == TypedAttribute.UNDEFINED_ATTR && origin.startsWith("#")) {
                ShapeElement se = new ShapeElement();
                se.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor(origin)));
                (ci.getView()).setBackground(se);
            }

        } catch (NotExistException | WrongTypeException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 应用图片
     * @param ci ColorUiInterface
     * @param theme Theme主题
     * @param attr attr属性
     */
    public static void applyImageDrawable(ColorUiInterface ci, Theme theme, String attr) {
        if (ci instanceof Image) {
            String key = buildParam(ResourceName.IMAGE_SRC, attr);
            if (theme.getThemeHash().get(key) == null) {
                return;
            }
            try {
                TypedAttribute ta = theme.getValue(key);
                // TypedAttribute getType()函数获取media类型错误，返回成了undefined类型
                String origin = ta.getParsedValue();
                if (origin.contains("/media/")) {
                    ((Image) ci.getView()).setImageElement(new PixelMapElement(ta.getMediaResource()));
                } else if (origin.contains("/graphic/")) {
                    ((Image) ci.getView()).setImageElement(new ShapeElement(
                            ci.getView().getContext(), ta.getResId()));
                }
            } catch (NotExistException | WrongTypeException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 应用字体大小
     * @param ci ColorUiInterface
     * @param theme Theme主题
     * @param attr attr属性
     */
    public static void applyTextColor(ColorUiInterface ci, Theme theme, String attr) {
        if (ci instanceof Text) {
            String key = buildParam(ResourceName.TEXT_COLOR, attr);
            if (theme.getThemeHash().get(key) == null) {
                return;
            }
            try {
                String color = theme.getValue(key).getParsedValue();
                ((Text) ci.getView()).setTextColor(new Color(Color.getIntColor(color)));
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 应用字体大小
     * @param ci ColorUiInterface
     * @param theme Theme主题
     * @param attr attr属性
     */
    public static void applyTextSize(ColorUiInterface ci, Theme theme, String attr) {
        if (ci instanceof Text) {
            String key = buildParam(ResourceName.TEXT_SIZE, attr);
            if (theme.getThemeHash().get(key) == null) {
                return;
            }
            try {
                float size = theme.getFloatValue(key, 0);
                ((Text) ci.getView()).setTextSize((int) size);
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
        }
    }

}
