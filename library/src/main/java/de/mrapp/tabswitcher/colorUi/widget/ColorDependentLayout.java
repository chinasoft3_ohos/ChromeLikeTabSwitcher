/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.colorUi.widget;

import de.mrapp.tabswitcher.colorUi.ColorUiInterface;
import de.mrapp.tabswitcher.colorUi.util.ViewAttributeUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;
import ohos.global.resource.solidxml.Theme;


/**
 * Created by chengli on 15/6/11.
 */
public class ColorDependentLayout extends DirectionalLayout implements ColorUiInterface{
    private String attr = "";

    public ColorDependentLayout(Context context) {
        super(context);
    }

    public ColorDependentLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    public ColorDependentLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    @Override
    public Component getView() {
        return this;
    }

    @Override
    public void setTheme(Theme themeId) {
        ViewAttributeUtil.applyBackgroundDrawable(this, themeId, attr);
    }
}
