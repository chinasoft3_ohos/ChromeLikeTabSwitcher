/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.colorUi.widget;

import de.mrapp.tabswitcher.colorUi.ColorUiInterface;
import de.mrapp.tabswitcher.colorUi.ResourceName;
import de.mrapp.tabswitcher.colorUi.util.ViewAttributeUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.global.resource.solidxml.Theme;
import ohos.global.resource.solidxml.TypedAttribute;

import java.io.IOException;

import static de.mrapp.tabswitcher.colorUi.util.ViewAttributeUtil.buildParam;


/**
 * Created by chengli on 15/6/8.
 */
public class ColorListView extends ListContainer implements ColorUiInterface {
    private String attr = "";

    public ColorListView(Context context) {
        super(context);
    }

    public ColorListView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    public ColorListView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    @Override
    public Component getView() {
        return this;
    }

    @Override
    public void setTheme(Theme theme) {
        ViewAttributeUtil.applyBackgroundDrawable(this, theme, attr);

        String key = buildParam(ResourceName.DIVIDER, attr);
        if (theme.getThemeHash().get(key) == null) {
            return;
        }

        try {
            TypedAttribute ta = theme.getValue(key);
            String origin = ta.getParsedValue();

            if (origin.contains("/graphic/")) {
                this.setBoundary(
                        new ShapeElement(getContext(), ta.getResId()));
            } else if (ta.getType() == TypedAttribute.UNDEFINED_ATTR && origin.startsWith("#")) {
                this.setBoundaryColor(new Color(Color.getIntColor(origin)));
            } else {
                this.setBoundaryColor(Color.TRANSPARENT);
            }
        } catch (NotExistException | WrongTypeException | IOException e) {
            e.printStackTrace();
        }
    }
}
