package de.mrapp.tabswitcher.drawable;

import de.mrapp.tabswitcher.*;
import de.mrapp.tabswitcher.util.DimensionUtil;
import de.mrapp.tabswitcher.util.PixelUtil;
import de.mrapp.util.Condition;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.*;
import ohos.app.Context;
import ohos.global.resource.Resource;

public class TabSwitcherDrawable extends PixelMapElement {

    /**
     * The size of the drawable in pixels.
     */
    private final int size;

    /**
     * The drawable, which is shown as the background.
     */
    private final Element background;


    /**
     * Creates a new drawable, which allows to display the number of tabs, which are currently
     * contained by a {@link TabSwitcher}.
     *
     * @param context The context, which should be used by the drawable, as an instance of the class {@link
     *                Context}. The context may not be null
     * @param resource resource
     */
    public TabSwitcherDrawable(final Context context, Resource resource) {
        super(resource);
        Condition.INSTANCE.ensureNotNull(context, "The context may not be null");
        size = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(context.getString(ResourceTable.String_tab_switcher_drawable_size)));
        background = convertResToElement(context, ResourceTable.Media_tab_switcher_drawable_background);

        //setColorFilter替换
        setColorMatrix(0.5f);
    }

    public void setColorMatrix(float matrix) {
        float[] srcMatrix = new float[] {
                matrix, 0, 0, 0, 0,
                0, matrix, 0, 0, 0,
                0, 0, matrix, 0, 0,
                0, 0, 0, 1, 0,
        };
        ColorMatrix colorMatrix = new ColorMatrix(srcMatrix);
        setColorMatrix(colorMatrix);
    }

    private Element convertResToElement(Context mContext, int drawableRes) {
        Element element = null;
        try {
            Resource resource = mContext.getResourceManager().getResource(drawableRes);
            element = new PixelMapElement(resource);
            return element;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return element;
    }

    @Override
    public int getWidth() {
        return size;
    }

    @Override
    public int getHeight() {
        return size;
    }

    @Override
    public final void setAlpha(final int alpha) {
        background.setAlpha(alpha);
    }

    /* @Override
    public final void setColorFilter(final ColorFilter colorFilter) {
        background.setColorFilter(colorFilter);
        paint.setColorFilter(colorFilter);
    }

   @Override
    public final int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }*/

}
