package de.mrapp.tabswitcher.gesture;

import de.mrapp.tabswitcher.TabSwitcher;
import ohos.agp.utils.RectFloat;

public abstract class AbstractDragGestureEventHandler extends AbstractTouchEventHandler {
    /**
     * The bounds of the onscreen area, the handler takes into consideration for handling
     * touch events.
     */
    private final RectFloat touchableArea;

    /**
     * Creates a new handler, which can be managed by a {@link TouchEventDispatcher} in order to
     * dispatch touch events to it.
     *
     * @param tabSwitcher
     *         The tab switcher, the event handler belongs to, as an instance of the class {@link
     *         TabSwitcher}. The tab switcher may not be null
     * @param dragThreshold
     *         The threshold of the drag helper, which is used to recognize drag gestures, in pixels
     *         as an {@link Integer} value The threshold must be at least 0
     * @param touchableArea
     *         The bounds of the onscreen area, the handler should take into consideration for
     *         handling touch events, as an instance of the class {@link RectFloat} or null, if the are
     *         should not be restricted
     */
    public AbstractDragGestureEventHandler(final TabSwitcher tabSwitcher,
                                           final int dragThreshold,
                                           final RectFloat touchableArea) {
        super(MAX_PRIORITY, tabSwitcher, dragThreshold);
        this.touchableArea = touchableArea;
    }

    @Override
    public RectFloat getTouchableArea() {
        return touchableArea;
    }
}
