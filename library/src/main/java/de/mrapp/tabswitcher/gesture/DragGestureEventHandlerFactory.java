package de.mrapp.tabswitcher.gesture;

import de.mrapp.tabswitcher.*;
import de.mrapp.tabswitcher.util.DimensionUtil;
import de.mrapp.tabswitcher.util.PixelUtil;
import de.mrapp.util.Condition;

public class DragGestureEventHandlerFactory {
    /**
     * The tab switcher, the event handler are created for.
     */
    private final TabSwitcher tabSwitcher;

    /**
     * Creates a new factory, which allows to create instances of the class {@link
     * AbstractDragGestureEventHandler}.
     *
     * @param tabSwitcher The tab switcher, the event handler should be created for, as an instance of the
     *                    class {@link TabSwitcher}. The tab switcher may not be null
     */
    public DragGestureEventHandlerFactory(final TabSwitcher tabSwitcher) {
        Condition.INSTANCE.ensureNotNull(tabSwitcher, "The tab switcher may not be null");
        this.tabSwitcher = tabSwitcher;
    }

    /**
     * Creates and returns the event handler, which corresponds to a specific drag gesture.
     *
     * @param dragGesture The drag gesture, the event handler should be created from, as an instance of the
     *                    class {@link DragGesture}. The drag gesture may not be null
     * @return The event handler, which has been created, as an instance of the class {@link
     * AbstractTouchEventHandler}. The event handler may not be null
     * @throws IllegalArgumentException
     */

    public final AbstractTouchEventHandler fromGesture(final DragGesture dragGesture) {
        Condition.INSTANCE.ensureNotNull(dragGesture, "The drag gesture may not be null");

        if (dragGesture instanceof SwipeGesture) {
            int vp24 = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_swipe_gesture_threshold)));
            int dragThreshold = dragGesture.getThreshold() != -1 ? dragGesture.getThreshold() : vp24;
            return new SwipeGestureEventHandler(tabSwitcher, dragThreshold,
                    dragGesture.getTouchableArea(),
                    ((SwipeGesture) dragGesture).getAnimationDuration());
        } else if (dragGesture instanceof PullDownGesture) {
            int vp12 = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_pull_down_gesture_threshold)));
            int dragThreshold = dragGesture.getThreshold() != -1 ? dragGesture.getThreshold() : vp12;
            return new PullDownGestureEventHandler(tabSwitcher, dragThreshold,
                    dragGesture.getTouchableArea());
        }

        throw new IllegalArgumentException(
                "Unsupported drag gesture: " + dragGesture.getClass().getSimpleName());
    }
}
