package de.mrapp.tabswitcher.layout;

import de.mrapp.tabswitcher.TabSwitcher;
import de.mrapp.tabswitcher.model.AbstractItem;
import de.mrapp.util.Condition;
import ohos.agp.animation.AnimatorProperty;

public abstract class AbstractArithmetics implements Arithmetics {

    /**
     * The tab switcher, the arithmetics are calculated for.
     */
    private final TabSwitcher tabSwitcher;

    /**
     * Returns the tab switcher, the arithmetics are calculated for.
     *
     * @return The tab switcher, the arithmetics are calculated for, as an instance of the class
     * {@link TabSwitcher}. The tab switcher may not be null
     */

    protected final TabSwitcher getTabSwitcher() {
        return tabSwitcher;
    }

    /**
     * Creates a new class, which provides methods, which allow to calculate the position, size and
     * rotation of a {@link TabSwitcher}'s tabs.
     *
     * @param tabSwitcher The tab switcher, the arithmetics should be calculated for, as an instance of the
     *                    class {@link TabSwitcher}. The tab switcher may not be null
     */
    public AbstractArithmetics(final TabSwitcher tabSwitcher) {
        Condition.INSTANCE.ensureNotNull(tabSwitcher, "The tab switcher may not be null");
        this.tabSwitcher = tabSwitcher;
    }

    @Override
    public final float getTabContainerSize(final Axis axis) {
        return getTabContainerSize(axis, true);
    }

    @Override
    public final void animatePosition(final Axis axis,
                                      final AnimatorProperty animator,
                                      final AbstractItem item, final float position) {
        animatePosition(axis, animator, item, position, false);
    }

    @Override
    public final float getScale(final AbstractItem item) {
        return getScale(item, false);
    }
}
