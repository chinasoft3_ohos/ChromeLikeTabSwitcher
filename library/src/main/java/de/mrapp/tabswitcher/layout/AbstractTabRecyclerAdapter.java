package de.mrapp.tabswitcher.layout;

import de.mrapp.tabswitcher.*;
import de.mrapp.tabswitcher.iterator.AbstractItemIterator;
import de.mrapp.tabswitcher.iterator.ItemIterator;
import de.mrapp.tabswitcher.menu.OnMenuItemClickListener;
import de.mrapp.tabswitcher.model.*;
import de.mrapp.tabswitcher.util.LogUtil;
import de.mrapp.materialview.library.CircularProgressBar;
import de.mrapp.util.Condition;
import de.mrapp.ohos_util.logging.LogLevel;
import de.mrapp.ohos_util.view.AbstractViewRecycler;
import de.mrapp.ohos_util.view.AttachedViewRecycler;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.app.Context;

public abstract class AbstractTabRecyclerAdapter
        extends AbstractViewRecycler.Adapter<AbstractItem, Integer>
        implements Tab.Callback, Model.Listener {


    /**
     * The view type of a tab.
     */
    private static final int TAB_VIEW_TYPE = 0;

    /**
     * The tab switcher, the tabs belong to.
     */
    private final TabSwitcher tabSwitcher;

    /**
     * The model, which belongs to the tab switcher.
     */
    private final TabSwitcherModel model;

    /*
     * The style, which allows to retrieve style attributes of the tab switcher.
     */
    private final TabSwitcherStyle style;

    /**
     * The view recycler, the adapter is bound to.
     */
    private AttachedViewRecycler<AbstractItem, Integer> viewRecycler;

    /**
     * Adapts the title of a tab.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose title should be adapted, as an
     *                instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptTitle(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        AbstractTabViewHolder viewHolder = tabItem.getViewHolder();
        viewHolder.titleTextView.setText((String) tab.getTitle());
    }

    /**
     * Adapts the icon of a tab.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose icon should be adapted, as an
     *                instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptIcon(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        AbstractTabViewHolder viewHolder = tabItem.getViewHolder();
        Element icon = style.getTabIcon(tab);
        viewHolder.iconImageView.setImageElement(icon);
    }

    /**
     * Adapts the visibility of a tab's close button.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose close button should be adapted, as
     *                an instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptCloseButtonVisibility(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        AbstractTabViewHolder viewHolder = tabItem.getViewHolder();
        viewHolder.closeButton.setVisibility(tab.isCloseable() ? Component.VISIBLE : Component.HIDE);
        viewHolder.closeButton.setTag(tab.isCloseable());
//        viewHolder.closeButton.setTag(R.id.tag_visibility, tab.isCloseable());
        viewHolder.closeButton.setClickedListener(
                tab.isCloseable() ? createCloseButtonClickListener(viewHolder.closeButton, tab) :
                        null);
        viewHolder.closeButton.setTouchEventListener((component, touchEvent) -> true);
    }

    /**
     * Adapts the icon of a tab's close button.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose close button icon should be
     *                adapted, as an instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptCloseButtonIcon(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        Element icon = style.getTabCloseButtonIcon(tab);
        AbstractTabViewHolder viewHolder = tabItem.getViewHolder();
        viewHolder.closeButton.setImageElement(icon);
    }

    /**
     * Adapts the background color of a tab.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose background should be adapted, as an
     *                instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptBackgroundColor(final TabItem tabItem) {
    }

    /**
     * Adapts the text color of a tab's title.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose title should be adapted, as an
     *                instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptTitleTextColor(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        Color colorStateList = style.getTabTitleTextColor(tab);
        LogUtil.loge("===titleTextColor: " + (colorStateList != null));
        AbstractTabViewHolder viewHolder = tabItem.getViewHolder();
        if (colorStateList != null) {
            viewHolder.titleTextView.setTextColor(colorStateList);
        }
    }

    /**
     * Adapts the visibility of a tab's progress bar.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose progress bar should be adapted, as
     *                an instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptProgressBarVisibility(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        AbstractTabViewHolder viewHolder = tabItem.getViewHolder();
        LogUtil.loge("=== viewHolder " + (viewHolder.progressBar != null));
        viewHolder.progressBar.setVisibility(tab.isProgressBarShown() ? Component.VISIBLE : Component.HIDE);
        viewHolder.iconImageView.setVisibility(tab.isProgressBarShown() ? Component.HIDE : Component.VISIBLE);
    }

    /**
     * Adapts the color of a tab's progress bar.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose progress bar should be adapted, as
     *                an instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptProgressBarColor(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        AbstractTabViewHolder viewHolder = tabItem.getViewHolder();
        int color = style.getTabProgressBarColor(tab);
        viewHolder.progressBar.setColor(color);
    }

    /**
     * Adapts the selection state of a tab's views.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose selection state should be adapted,
     *                as an instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptSelectionState(final TabItem tabItem) {
        boolean selected = model.getSelectedTab() == tabItem.getTab();
        tabItem.getView().setSelected(selected);
        AbstractTabViewHolder viewHolder = tabItem.getViewHolder();
        viewHolder.titleTextView.setSelected(selected);
        viewHolder.closeButton.setSelected(selected);
    }

    /**
     * Adapts the appearance of all currently inflated tabs, depending on whether they are currently
     * selected, or not.
     */
    private void adaptAllSelectionStates() {
        AbstractItemIterator iterator =
                new ItemIterator.Builder(model, getViewRecyclerOrThrowException()).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() && item instanceof TabItem) {
                TabItem tabItem = (TabItem) item;
                adaptSelectionState(tabItem);
                adaptBackgroundColor(tabItem);
            }
        }
    }

    /**
     * Creates and returns a listener, which allows to close a specific tab, when its close button
     * is clicked.
     *
     * @param closeButton The tab's close button as an instance of the class {@link Image}. The button
     *                    may not be null
     * @param tab         The tab, which should be closed, as an instance of the class {@link Tab}. The tab may
     *                    not be null
     * @return The listener, which has been created, as an instance of the class {@link
     * Component.ClickedListener}. The listener may not be null
     */

    private Component.ClickedListener createCloseButtonClickListener(
            final Image closeButton, final Tab tab) {
        return new Component.ClickedListener() {

            @Override
            public void onClick(final Component v) {
                TabItem tabItem = getTabItem(tab);

                if (tabItem != null) {
                    State state = tabItem.getTag().getState();

                    if (state == State.FLOATING || state == State.STACKED_START_ATOP) {
                        if (notifyOnCloseTab(tab)) {
                            closeButton.setClickedListener(null);
                            tabSwitcher.removeTab(tab);
                        }
                    }
                }
            }

        };
    }

    /**
     * Notifies all listeners, that a tab is about to be closed by clicking its close button.
     *
     * @param tab The tab, which is about to be closed, as an instance of the class {@link Tab}. The
     *            tab may not be null
     * @return True, if the tab should be closed, false otherwise
     */
    private boolean notifyOnCloseTab(final Tab tab) {
        boolean result = true;

        for (TabCloseListener listener : model.getTabCloseListeners()) {
            result &= listener.onCloseTab(tabSwitcher, tab);
        }

        return result;
    }

    /**
     * Returns the tab switcher, which contains the tabs.
     *
     * @return The tab switcher, which contains the tabs, as an instance of the class {@link
     * TabSwitcher}. The tab switcher may not be null
     */

    protected final TabSwitcher getTabSwitcher() {
        return tabSwitcher;
    }

    /**
     * Returns the model of the tab switcher.
     *
     * @return The model of the tab switcher as an instance of the class {@link TabSwitcherModel}.
     * The model may not be null
     */

    protected final TabSwitcherModel getModel() {
        return model;
    }

    /**
     * Returns the style, which allow to retrieve style attributes of the tab switcher.
     *
     * @return The style, which allows to retrieve style attributes of the tab switcher, as an
     * instance of the class {@link TabSwitcherStyle}. The style may not be null
     */

    protected final TabSwitcherStyle getStyle() {
        return style;
    }

    /**
     * Returns the tab item, which corresponds to a specific tab.
     *
     * @param tab The tab, whose tab item should be returned, as an instance of the class {@link Tab}.
     *            The tab may not be null
     * @return The tab item, which corresponds to the given tab, as an instance of the class {@link
     * TabItem} or null, if no view, which visualizes the tab, is currently inflated
     */
    protected final TabItem getTabItem(final Tab tab) {
        int index = model.indexOf(tab);

        if (index != -1) {
            TabItem tabItem = TabItem.create(model, getViewRecyclerOrThrowException(), index);

            if (tabItem.isInflated()) {
                return tabItem;
            }
        }

        return null;
    }

    /**
     * Returns the view recycler, the adapter is bound to, or throws an {@link
     * IllegalStateException}, if no view recycler has been set.
     *
     * @return The view recycler, the adapter is bound to, as an instance of the class
     * AttachedViewRecycler. The view recycler may not be null
     */

    protected final AttachedViewRecycler<AbstractItem, Integer> getViewRecyclerOrThrowException() {
        Condition.INSTANCE.ensureNotNull(viewRecycler, "No view recycler has been set",
                IllegalStateException.class);
        return viewRecycler;
    }

    /**
     * The method, which is invoked on implementing subclasses, when the background color of a tab
     * has been changed.
     *
     * @param color   The color, which has been set, as an {@link Integer} value
     * @param tabItem The tab item, which corresponds to the tab, whose background color has been changed,
     *                as an instance of the class {@link TabItem}. The tab item may not be null
     */
    protected void onAdaptBackgroundColor(final int color,
                                          final TabItem tabItem) {

    }

    /**
     * The method, which is invoked on implementing subclasses in order to inflate the view, which
     * is used to visualize tabs.
     *
     * @param inflater   The layout inflater, which should be used, as an instance of the class {@link
     *                   LayoutScatter}. The layout inflater may not be null
     * @param parent     The parent of the view, which should be inflated, as an instance of the class {@link
     *                   ComponentContainer} or null, if no parent is available
     * @param viewHolder The view holder, which should hold references to the child views of the view, which
     *                   should be inflated, as an instance of the class {@link AbstractTabViewHolder}. The
     *                   view holder may not be null
     * @return The view, which has been inflated, as an instance of the class {@link Component}. The view
     * may not be null
     */

    protected abstract Component onInflateTabView(final LayoutScatter inflater,
                                                  final ComponentContainer parent,
                                                  final AbstractTabViewHolder viewHolder);

    /**
     * The method, which is invoked on implementing subclasses in order to adapt the appearance of a
     * view, which is used to visualize a tab.
     *
     * @param view    The view, which is used to visualize the tab, as an instance of the class {@link
     *                Component}. The view may not be null
     * @param tabItem The tab item, which corresponds to the tab, which is visualized by the given view, as
     *                an instance of the class {@link TabItem}. The tab item may not be null
     * @param params  An array, which may contain optional parameters, as an array of the generic type
     *                ParamType or an empty array, if no optional parameters are available
     */
    @SuppressWarnings("unchecked")
    protected abstract void onShowTabView(final Component view, final TabItem tabItem,
                                          final Integer... params);

    /**
     * The method, which is invoked on implementing subclasses in order to create the view holder,
     * which should be associated with an inflated view.
     *
     * @return The view holder, which has been created, as an instance of the class {@link
     * AbstractTabViewHolder}. The view holder may not be null
     */

    protected abstract AbstractTabViewHolder onCreateTabViewHolder();

    /**
     * The method, which is invoked on implementing subclasses in order to retrieve the layout,
     * which is used by the tab switcher.
     *
     * @return The layout, which is used by the tab switcher, as a value of the enum {@link Layout}.
     * The layout may not be null
     */

    protected abstract Layout getLayout();

    /**
     * Creates a new view recycler adapter, which allows to inflate the views, which are used to
     * visualize the tabs of a {@link TabSwitcher}.
     *
     * @param tabSwitcher The tab switcher as an instance of the class {@link TabSwitcher}. The tab switcher
     *                    may not be null
     * @param model       The model, which belongs to the tab switcher, as an instance of the class {@link
     *                    TabSwitcherModel}. The model may not be null
     * @param style       The style, which allows to retrieve style attributes of the tab switcher switcher, as
     *                    an instance of the class {@link TabSwitcherStyle}. The style may not be null
     */
    public AbstractTabRecyclerAdapter(final TabSwitcher tabSwitcher,
                                      final TabSwitcherModel model,
                                      final TabSwitcherStyle style) {
        Condition.INSTANCE.ensureNotNull(tabSwitcher, "The tab switcher may not be null");
        Condition.INSTANCE.ensureNotNull(model, "The model may not be null");
        Condition.INSTANCE.ensureNotNull(style, "The style may not be null");
        this.tabSwitcher = tabSwitcher;
        this.model = model;
        this.style = style;
        this.viewRecycler = null;
    }

    /**
     * Sets the view recycler, which allows to inflate the views, which are used to visualize tabs.
     *
     * @param viewRecycler The view recycler, which should be set, as an instance of the class
     *                     AttachedViewRecycler. The view recycler may not be null
     */
    public final void setViewRecycler(
            final AttachedViewRecycler<AbstractItem, Integer> viewRecycler) {
        Condition.INSTANCE.ensureNotNull(viewRecycler, "The view recycler may not be null");
        this.viewRecycler = viewRecycler;
    }

    @Override
    public void onLogLevelChanged(final LogLevel logLevel) {

    }

    @Override
    public final void onDecoratorChanged(final TabSwitcherDecorator decorator) {

    }

    @Override
    public final void onSwitcherShown() {

    }

    @Override
    public final void onSwitcherHidden() {

    }

    @Override
    public final void onSelectionChanged(final int previousIndex, final int index,
                                         final Tab selectedTab,
                                         final boolean switcherHidden) {
        adaptAllSelectionStates();
    }

    @Override
    public final void onTabAdded(final int index, final Tab tab,
                                 final int previousSelectedTabIndex, final int selectedTabIndex,
                                 final boolean selectionChanged,
                                 final boolean switcherVisibilityChanged,
                                 final Animation animation) {
        if (selectionChanged) {
            adaptAllSelectionStates();
        }
    }

    @Override
    public final void onAllTabsAdded(final int index, final Tab[] tabs,
                                     final int previousSelectedTabIndex, final int selectedTabIndex,
                                     final boolean selectionChanged,
                                     final Animation animation) {
        if (selectionChanged) {
            adaptAllSelectionStates();
        }
    }

    @Override
    public final void onTabRemoved(final int index, final Tab tab,
                                   final int previousSelectedTabIndex, final int selectedTabIndex,
                                   final boolean selectionChanged,
                                   final Animation animation) {
        if (selectionChanged) {
            adaptAllSelectionStates();
        }
    }

    @Override
    public final void onAllTabsRemoved(final Tab[] tabs,
                                       final Animation animation) {

    }

    @Override
    public void onPaddingChanged(final int left, final int top, final int right, final int bottom) {

    }

    @Override
    public void onApplyPaddingToTabsChanged(final boolean applyPaddingToTabs) {

    }

    @Override
    public final void onTabIconChanged(final Element icon) {
        ItemIterator iterator =
                new ItemIterator.Builder(model, getViewRecyclerOrThrowException()).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() && item instanceof TabItem) {
                adaptIcon((TabItem) item);
            }
        }
    }


    @Override
    public void onTabBackgroundColorChanged(final Color colorStateList) {
        ItemIterator iterator =
                new ItemIterator.Builder(model, getViewRecyclerOrThrowException()).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() && item instanceof TabItem) {
                adaptBackgroundColor((TabItem) item);
            }
        }
    }

    @Override
    public void onTabContentBackgroundColorChanged(final int color) {

    }

    @Override
    public final void onTabTitleColorChanged(final Color colorStateList) {
        ItemIterator iterator =
                new ItemIterator.Builder(model, getViewRecyclerOrThrowException()).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() && item instanceof TabItem) {
                adaptTitleTextColor((TabItem) item);
            }
        }
    }

    @Override
    public final void onTabCloseButtonIconChanged(final Element icon) {
        ItemIterator iterator =
                new ItemIterator.Builder(model, getViewRecyclerOrThrowException()).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() && item instanceof TabItem) {
                adaptCloseButtonIcon((TabItem) item);
            }
        }
    }

    @Override
    public final void onTabProgressBarColorChanged(final int color) {
        ItemIterator iterator =
                new ItemIterator.Builder(model, getViewRecyclerOrThrowException()).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() && item instanceof TabItem) {
                adaptProgressBarColor((TabItem) item);
            }
        }
    }

    @Override
    public final void onAddTabButtonVisibilityChanged(final boolean visible) {

    }

    @Override
    public void onAddTabButtonColorChanged(final Color colorStateList) {

    }

    @Override
    public final void onToolbarVisibilityChanged(final boolean visible) {

    }

    @Override
    public final void onToolbarTitleChanged(final CharSequence title) {

    }

    @Override
    public final void onToolbarNavigationIconChanged(final Element icon,
                                                     final Component.ClickedListener listener) {

    }

    @Override
    public final void onToolbarMenuInflated(final int resourceId,
                                            final OnMenuItemClickListener listener) {

    }

    @Override
    public final void onEmptyViewChanged(final Component view, final long animationDuration) {

    }

    @Override
    public final void onTitleChanged(final Tab tab) {
        TabItem tabItem = getTabItem(tab);

        if (tabItem != null) {
            adaptTitle(tabItem);
        }
    }

    @Override
    public final void onIconChanged(final Tab tab) {
        TabItem tabItem = getTabItem(tab);

        if (tabItem != null) {
            adaptIcon(tabItem);
        }
    }

    @Override
    public final void onCloseableChanged(final Tab tab) {
        TabItem tabItem = getTabItem(tab);

        if (tabItem != null) {
            adaptCloseButtonVisibility(tabItem);
        }
    }

    @Override
    public final void onCloseButtonIconChanged(final Tab tab) {
        TabItem tabItem = getTabItem(tab);

        if (tabItem != null) {
            adaptCloseButtonIcon(tabItem);
        }
    }

    @Override
    public final void onBackgroundColorChanged(final Tab tab) {
        TabItem tabItem = getTabItem(tab);

        if (tabItem != null) {
            adaptBackgroundColor(tabItem);
        }
    }

    @Override
    public void onContentBackgroundColorChanged(final Tab tab) {

    }

    @Override
    public final void onTitleTextColorChanged(final Tab tab) {
        TabItem tabItem = getTabItem(tab);

        if (tabItem != null) {
            adaptTitleTextColor(tabItem);
        }
    }

    @Override
    public final void onProgressBarVisibilityChanged(final Tab tab) {
        TabItem tabItem = getTabItem(tab);

        if (tabItem != null) {
            adaptProgressBarVisibility(tabItem);
        }
    }

    @Override
    public final void onProgressBarColorChanged(final Tab tab) {
        TabItem tabItem = getTabItem(tab);

        if (tabItem != null) {
            adaptProgressBarColor(tabItem);
        }
    }


    @Override
    public int getViewType(final AbstractItem item) {
        if (item instanceof TabItem) {
            return TAB_VIEW_TYPE;
        } else {
            throw new IllegalArgumentException("Unknown item type");
        }
    }

    @SuppressWarnings("unchecked")


    @Override
    public Component onInflateView(final LayoutScatter inflater,
                                   final ComponentContainer parent, final AbstractItem item,
                                   final int viewType, final Integer... params) {

        LogUtil.loge("=== onInflateView");
        if (viewType == TAB_VIEW_TYPE) {
            TabItem tabItem = (TabItem) item;
            AbstractTabViewHolder viewHolder = onCreateTabViewHolder();
            Component view = onInflateTabView(inflater, parent, viewHolder);
            viewHolder.titleContainer = (ComponentContainer) view.findComponentById(ResourceTable.Id_tab_title_container);
            viewHolder.titleTextView = (Text) view.findComponentById(ResourceTable.Id_tab_title_text_view);
            viewHolder.iconImageView = (Image) view.findComponentById(ResourceTable.Id_tab_icon_image_view);
            viewHolder.progressBar = (CircularProgressBar) view.findComponentById(ResourceTable.Id_tab_progress_bar);
            LogUtil.loge(" inflate progressBar" + (viewHolder.progressBar != null));
            viewHolder.closeButton = (Image) view.findComponentById(ResourceTable.Id_close_tab_button);
            //设置tag 不能指定id, 故通过不同Component 来设置
            /*view.setTag(R.id.tag_view_holder, viewHolder);
            tabItem.setViewHolder(viewHolder);
            item.setView(view);
            view.setTag(R.id.tag_properties, item.getTag());*/

            view.setTag(viewHolder);
            tabItem.setViewHolder(viewHolder);
            item.setView(view);
            viewHolder.titleContainer.setTag(item.getTag());
            return view;
        } else {
            throw new IllegalArgumentException("Unknown view type");
        }
    }

    @SuppressWarnings("unchecked")

    @Override
    public void onShowView(final Context context, final Component view,
                           final AbstractItem item, final boolean inflated,
                           final Integer... params) {
        if (item instanceof TabItem) {
            LogUtil.loge("=== onShowView AbstractTabRecyclerAdapter");
            TabItem tabItem = (TabItem) item;
            AbstractTabViewHolder viewHolder = (AbstractTabViewHolder) view.getTag();

            if (!tabItem.isInflated()) {
                LogUtil.loge("=== onShowView isInflated");
                tabItem.setView(view);
                tabItem.setViewHolder(viewHolder);
                //设置tag 不能指定id, 故通过不同Component 来设置
                //view.setTag(R.id.tag_properties, tabItem.getTag());
                viewHolder.titleContainer = (ComponentContainer) view.findComponentById(ResourceTable.Id_tab_title_container);
                viewHolder.titleContainer.setTag(tabItem.getTag());
            }

            Tab tab = tabItem.getTab();
            tab.addCallback(this);
            adaptBackgroundColor(tabItem);
            adaptTitle(tabItem);
            adaptTitleTextColor(tabItem);
            adaptIcon(tabItem);
            adaptProgressBarVisibility(tabItem);
            adaptCloseButtonVisibility(tabItem);
            adaptCloseButtonIcon(tabItem);
            adaptSelectionState(tabItem);
            onShowTabView(view, tabItem, params);
        } else {
            throw new IllegalArgumentException("Unknown item type");
        }
    }


    @Override
    public void onRemoveView(final Component view, final AbstractItem item) {
        if (item instanceof TabItem) {
            TabItem tabItem = (TabItem) item;
            Tab tab = tabItem.getTab();
            tab.removeCallback(this);
//            view.setTag(R.id.tag_properties, null);
        } else {
            throw new IllegalArgumentException("Unknown item type");
        }
    }
}
