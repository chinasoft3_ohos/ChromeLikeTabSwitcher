/*
 * Copyright 2016 - 2020 Michael Rapp
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package de.mrapp.tabswitcher.layout;

import de.mrapp.tabswitcher.Animation;
import de.mrapp.tabswitcher.StatefulTabSwitcherDecorator;
import de.mrapp.tabswitcher.Tab;
import de.mrapp.tabswitcher.TabSwitcher;
import de.mrapp.tabswitcher.TabSwitcherDecorator;
import de.mrapp.tabswitcher.TabSwitcherListener;
import de.mrapp.tabswitcher.model.Restorable;
import de.mrapp.tabswitcher.util.LogUtil;
import de.mrapp.util.Condition;
import de.mrapp.ohos_util.view.AbstractViewRecycler;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.app.Context;
import ohos.utils.PacMap;
import ohos.utils.PlainArray;

import java.util.Optional;

/**
 * A view recycler adapter, which allows to inflate the views, which are associated with the tabs of
 * a {@link TabSwitcher}, by encapsulating a {@link TabSwitcherDecorator}.
 *
 * @author Michael Rapp
 * @since 0.1.0
 */
public class ContentRecyclerAdapter extends AbstractViewRecycler.Adapter<Tab, Void>
        implements Restorable, TabSwitcherListener {

    /**
     * The name of the extra, which is used to store the saved instance states of previously removed
     * associated views within a bundle.
     */
    private static final String SAVED_INSTANCE_STATES_EXTRA =
            ContentRecyclerAdapter.class.getName() + "::SavedInstanceStates";

    /**
     * The tab switcher, which contains the tabs, the associated views, which are inflated by the
     * adapter, correspond to.
     */
    private final TabSwitcher tabSwitcher;

    /**
     * The decorator, which is used to inflate the associated views.
     */
    private final TabSwitcherDecorator decorator;

    /**
     * A sparse array, which manages the saved instance states of previously removed associated
     * views.
     */
    private PlainArray<PacMap> savedInstanceStates;

    /**
     * Puts the parameter {@link Tab#WAS_SHOWN_PARAMETER} into a specific bundle. If the bundle is
     * null, a new bundle is created.
     *
     * @param parameters The bundle, the parameter should be put into, as an instance of the class {@link
     *                   PacMap} or null
     * @return The bundle, the parameter has been put into, as an instance of the clas {@link
     * PacMap}. The bundle may not be null
     */

    private PacMap setWasShownParameter(final PacMap parameters) {
        PacMap result = parameters;

        if (result == null) {
            result = new PacMap();
        }

        result.putBooleanValue(Tab.WAS_SHOWN_PARAMETER, true);
        return result;
    }

    /**
     * Creates a new view recycler adapter, which allows to inflate views, which are associated with
     * the tabs of a {@link TabSwitcher}, by encapsulating a {@link TabSwitcherDecorator}.
     *
     * @param tabSwitcher The tab switcher, which contains the tabs, whose associated views are inflated by the
     *                    adapter, correspond to, as an instance of the class {@link TabSwitcher}. The tab
     *                    switcher may not be null
     * @param decorator   The decorator, which should be used to inflate the associated views, as an instance
     *                    of the class {@link TabSwitcherDecorator}. The decorator may not be null
     */
    public ContentRecyclerAdapter(final TabSwitcher tabSwitcher,
                                  final TabSwitcherDecorator decorator) {
        Condition.INSTANCE.ensureNotNull(tabSwitcher, "The tab switcher may not be null");
        Condition.INSTANCE.ensureNotNull(decorator, "The decorator may not be null");
        this.tabSwitcher = tabSwitcher;
        tabSwitcher.addListener(this);
        this.decorator = decorator;
        this.savedInstanceStates = new PlainArray<>();
    }

    /**
     * Clears the saved state of a specific tab.
     *
     * @param tab The tab, whose saved state should be cleared, as an instance of the class {@link
     *            Tab}. The tab may not be null
     */
    public void clearSavedState(final Tab tab) {
        Condition.INSTANCE.ensureNotNull(tab, "The tab may not be null");
        savedInstanceStates.remove(tab.hashCode());
    }

    /**
     * Clears the saved states of all tabs.
     */
    public void clearAllSavedStates() {
        savedInstanceStates.clear();
    }


    @Override
    public final Component onInflateView(final LayoutScatter inflater,
                                         final ComponentContainer parent, final Tab item,
                                         final int viewType, final Void... params) {
        LogUtil.loge("=== onInflateView");
        int index = tabSwitcher.indexOf(item);
        Component view = decorator.inflateView(inflater, parent, item, index);
        view.setLayoutConfig(new StackLayout.LayoutConfig(StackLayout.LayoutConfig.MATCH_PARENT,
                StackLayout.LayoutConfig.MATCH_PARENT));
        return view;
    }

    @Override
    public final void onShowView(final Context context, final Component view,
                                 final Tab item, final boolean inflated,
                                 final Void... params) {
        LogUtil.loge("=== onShowView ContentRecyclerAdapter");
        int index = tabSwitcher.indexOf(item);
        PacMap savedInstanceState = null;
        PacMap parameters = item.getParameters();

        if (parameters != null && parameters.getBooleanValue(Tab.WAS_SHOWN_PARAMETER, false)) {
            Optional<PacMap> optionalPacMap = savedInstanceStates.get(item.hashCode());
            if (optionalPacMap.isPresent()) {
                savedInstanceState = savedInstanceStates.get(item.hashCode()).get();
            }
        }

        item.setParameters(setWasShownParameter(parameters));
        decorator.applyDecorator(context, tabSwitcher, view, item, index, savedInstanceState,
                inflated);
    }

    @Override
    public final void onRemoveView(final Component view, final Tab item) {
        int index = tabSwitcher.indexOf(item);
        PacMap outState = decorator.saveInstanceState(view, item, index);
        savedInstanceStates.put(item.hashCode(), outState);
    }

    @Override
    public final int getViewTypeCount() {
        return decorator.getViewTypeCount();
    }

    @Override
    public final int getViewType(final Tab item) {
        int index = tabSwitcher.indexOf(item);
        return decorator.getViewType(item, index);
    }

    @Override
    public final void saveInstanceState(final PacMap outState) {
        outState.putPlainArray(SAVED_INSTANCE_STATES_EXTRA, savedInstanceStates);
    }

    @Override
    public final void restoreInstanceState(final PacMap savedInstanceState) {
        if (savedInstanceState != null) {
            savedInstanceStates = (PlainArray<PacMap>) savedInstanceState.getPlainArray(SAVED_INSTANCE_STATES_EXTRA);
        }
    }

    @Override
    public final void onSwitcherShown(final TabSwitcher tabSwitcher) {

    }

    @Override
    public final void onSwitcherHidden(final TabSwitcher tabSwitcher) {

    }

    @Override
    public final void onSelectionChanged(final TabSwitcher tabSwitcher,
                                         final int selectedTabIndex,
                                         final Tab selectedTab) {

    }

    @Override
    public final void onTabAdded(final TabSwitcher tabSwitcher, final int index,
                                 final Tab tab, final Animation animation) {

    }

    @Override
    public final void onTabRemoved(final TabSwitcher tabSwitcher, final int index,
                                   final Tab tab, final Animation animation) {
        if (tabSwitcher.areSavedStatesClearedWhenRemovingTabs()) {
            clearSavedState(tab);
            TabSwitcherDecorator decorator = tabSwitcher.getDecorator();

            if (decorator instanceof StatefulTabSwitcherDecorator) {
                ((StatefulTabSwitcherDecorator) decorator).clearState(tab);
            }
        }
    }

    @Override
    public final void onAllTabsRemoved(final TabSwitcher tabSwitcher,
                                       final Tab[] tabs,
                                       final Animation animation) {
        if (tabSwitcher.areSavedStatesClearedWhenRemovingTabs()) {
            clearAllSavedStates();

            TabSwitcherDecorator decorator = tabSwitcher.getDecorator();

            if (decorator instanceof StatefulTabSwitcherDecorator) {
                ((StatefulTabSwitcherDecorator) decorator).clearAllStates();
            }
        }
    }

}