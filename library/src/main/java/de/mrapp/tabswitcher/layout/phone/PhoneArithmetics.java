package de.mrapp.tabswitcher.layout.phone;

import de.mrapp.tabswitcher.Layout;
import de.mrapp.tabswitcher.ResourceTable;
import de.mrapp.tabswitcher.TabSwitcher;
import de.mrapp.tabswitcher.layout.AbstractArithmetics;
import de.mrapp.tabswitcher.layout.AbstractDragTabsEventHandler;
import de.mrapp.tabswitcher.model.AbstractItem;
import de.mrapp.tabswitcher.util.DimensionUtil;
import de.mrapp.tabswitcher.util.PixelUtil;
import de.mrapp.tabswitcher.view.Toolbar;
import de.mrapp.util.Condition;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.utils.LayoutAlignment;
import ohos.global.resource.Element;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class PhoneArithmetics extends AbstractArithmetics {
    /**
     * The height of a tab's title container in pixels.
     */
    private final int tabTitleContainerHeight;

    /**
     * The inset of tabs in pixels.
     */
    private final int tabInset;

    /**
     * The number of tabs, which are contained by a stack.
     */
    private final int stackedTabCount;

    /**
     * The space between tabs, which are part of a stack, in pixels.
     */
    private final float stackedTabSpacing;

    /**
     * The pivot when overshooting at the end.
     */
    private final float endOvershootPivot;

    /**
     * Modifies a specific axis depending on the orientation of the tab switcher.
     *
     * @param axis The original axis as a value of the enum {@link Axis}. The axis may not be null
     * @return The orientation invariant axis as a value of the enum {@link Axis}. The orientation
     * invariant axis may not be null
     */

    private Axis getOrientationInvariantAxis(final Axis axis) {
        if (axis == Axis.Y_AXIS) {
            return Axis.DRAGGING_AXIS;
        } else if (axis == Axis.X_AXIS) {
            return Axis.ORTHOGONAL_AXIS;
        } else if (getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE) {
            return axis == Axis.DRAGGING_AXIS ? Axis.ORTHOGONAL_AXIS : Axis.DRAGGING_AXIS;
        } else {
            return axis;
        }
    }

    /**
     * Returns the default pivot of an item on a specific axis.
     *
     * @param axis The axis as a value of the enum {@link Axis}. The axis may not be null
     * @param item The item, whose pivot should be returned, as an instance of the class {@link
     *             AbstractItem}. The item may not be null
     * @return The pivot of the given item on the given axis as a {@link Float} value
     */
    private float getDefaultPivot(final Axis axis, final AbstractItem item) {
        if (axis == Axis.DRAGGING_AXIS || axis == Axis.Y_AXIS) {
            return getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ?
                    getSize(axis, item) / 2f : 0;
        } else {
            return getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ? 0 :
                    getSize(axis, item) / 2f;
        }
    }

    /**
     * Returns the pivot of an item on a specific axis, when it is swiped.
     *
     * @param axis The axis as a value of the enum {@link Axis}. The axis may not be null
     * @param item The item, whose pivot should be returned, as an instance of the class {@link
     *             AbstractItem}. The item may not be null
     * @return The pivot of the given item on the given axis as a {@link Float} value
     */
    private float getPivotWhenSwiping(final Axis axis, final AbstractItem item) {
        if (axis == Axis.DRAGGING_AXIS || axis == Axis.Y_AXIS) {
            return endOvershootPivot;
        } else {
            return getDefaultPivot(axis, item);
        }
    }

    /**
     * Returns the pivot of an item on a specific axis, when overshooting at the start.
     *
     * @param axis The axis as a value of the enum {@link Axis}. The axis may not be null
     * @param item The item, whose pivot should be returned, as an instance of the class {@link
     *             AbstractItem}. The item may not be null
     * @return The pivot of the given item on the given axis as a {@link Float} value
     */
    private float getPivotWhenOvershootingAtStart(final Axis axis,
                                                  final AbstractItem item) {
        return getSize(axis, item) / 2f;
    }

    /**
     * Returns the pivot of an item on a specific axis, when overshooting at the end.
     *
     * @param axis The axis as a value of the enum {@link Axis}. The axis may not be null
     * @param item The item, whose pivot should be returned, as an instance of the class {@link
     *             AbstractItem}. The item may not be null
     * @return The pivot of the given item on the given axis as a {@link Float} value
     */
    private float getPivotWhenOvershootingAtEnd(final Axis axis,
                                                final AbstractItem item) {
        if (axis == Axis.DRAGGING_AXIS || axis == Axis.Y_AXIS) {
            return getTabSwitcher().getCount() > 1 ? endOvershootPivot : getSize(axis, item) / 2f;
        } else {
            return getSize(axis, item) / 2f;
        }
    }

    /**
     * Creates a new class, which provides methods, which allow to calculate the position, size and
     * rotation of a {@link TabSwitcher}'s tabs, when using the smartphone layout.
     *
     * @param tabSwitcher The tab switcher, the arithmetics should be calculated for, as an instance of the
     *                    class {@link TabSwitcher}. The tab switcher may not be null
     */
    public PhoneArithmetics(final TabSwitcher tabSwitcher) {
        super(tabSwitcher);
        int vp48 = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_tab_title_container_height)));
        tabTitleContainerHeight = vp48;
        tabInset = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_tab_inset)));
        int tabCount = 0;
        try {
            Element element = tabSwitcher.getContext().getResourceManager().getElement(ResourceTable.Integer_phone_stacked_tab_count);
            tabCount = element.getInteger();
        } catch (Exception e) {
            e.printStackTrace();
        }
        stackedTabCount = tabCount;
        stackedTabSpacing = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_stacked_tab_spacing)));
        endOvershootPivot = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_end_overshoot_pivot)));
    }

    @Override
    public final int getTabSwitcherPadding(final Axis axis, final int gravity) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE
                .ensureTrue(gravity == LayoutAlignment.START || gravity == LayoutAlignment.END, "Invalid gravity");

        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            return gravity == LayoutAlignment.START ? getTabSwitcher().getPaddingTop() :
                    getTabSwitcher().getPaddingBottom();
        } else {
            return gravity == LayoutAlignment.START ? getTabSwitcher().getPaddingLeft() :
                    getTabSwitcher().getPaddingRight();
        }
    }

    @Override
    public final float getTabContainerSize(final Axis axis, final boolean includePadding) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        ComponentContainer tabContainer = getTabSwitcher().getTabContainer();
        assert tabContainer != null;
        StackLayout.LayoutConfig layoutParams =
                (StackLayout.LayoutConfig) tabContainer.getLayoutConfig();
        int padding = !includePadding ? (getTabSwitcherPadding(axis, LayoutAlignment.START) +
                getTabSwitcherPadding(axis, LayoutAlignment.END)) : 0;
        Toolbar[] toolbars = getTabSwitcher().getToolbars();

        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            int toolbarSize =
                    !includePadding && getTabSwitcher().areToolbarsShown() && toolbars != null ?
                            toolbars[0].getHeight() - tabInset : 0;
            return tabContainer.getHeight() - layoutParams.getMarginTop() - layoutParams.getMarginBottom() -
                    padding - toolbarSize;
        } else {
            return tabContainer.getWidth() - layoutParams.getMarginLeft() - layoutParams.getMarginRight() -
                    padding;
        }
    }

    @Override
    public final float getTouchPosition(final Axis axis,
                                        final TouchEvent event) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(event, "The motion event may not be null");

        MmiPoint point = event.getPointerPosition(event.getIndex());
        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            return point.getY();
        } else {
            return point.getX();
        }
    }

    @Override
    public final float getPosition(final Axis axis, final AbstractItem item) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(item, "The item may not be null");
        Component view = item.getView();

        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            Toolbar[] toolbars = getTabSwitcher().getToolbars();
            return (float) ((double)view.getTranslationY() -
                                (getTabSwitcher().areToolbarsShown() && getTabSwitcher().isSwitcherShown() &&
                                        toolbars != null ?
                                        (double)(toolbars[TabSwitcher.PRIMARY_TOOLBAR_INDEX].getHeight()) - (double)tabInset :
                                        0) - (double)getTabSwitcherPadding(axis, LayoutAlignment.START));
        } else {
            StackLayout.LayoutConfig layoutParams =
                    (StackLayout.LayoutConfig) view.getLayoutConfig();
            return (float) ((double)view.getTranslationX() - (double)layoutParams.getMarginLeft() - (double)(getTabSwitcher().getPaddingLeft() / 2d) +
                                getTabSwitcher().getPaddingRight() / 2d +
                                (getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE &&
                                        getTabSwitcher().isSwitcherShown() ?
                                        stackedTabCount * stackedTabSpacing / 2d : 0));
        }
    }

    @Override
    public final void setPosition(final Axis axis, final AbstractItem item,
                                  final float position) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(item, "The item may not be null");
        Component view = item.getView();

        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            Toolbar[] toolbars = getTabSwitcher().getToolbars();
            view.setTranslationY((float) ((getTabSwitcher().areToolbarsShown() && getTabSwitcher().isSwitcherShown() &&
                                toolbars != null ?
                                toolbars[TabSwitcher.PRIMARY_TOOLBAR_INDEX].getHeight() - (double)tabInset : 0) +
                    (double)getTabSwitcherPadding(axis, LayoutAlignment.START) + position));
        } else {
            StackLayout.LayoutConfig layoutParams =
                    (StackLayout.LayoutConfig) view.getLayoutConfig();
            view.setTranslationX((float) (position + (double)layoutParams.getMarginLeft() + getTabSwitcher().getPaddingLeft() / 2d -
                                getTabSwitcher().getPaddingRight() / 2d -
                                (getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE &&
                                        getTabSwitcher().isSwitcherShown() ?
                                        stackedTabCount * stackedTabSpacing / 2d : 0)));
        }
    }

    @Override
    public final void animatePosition(final Axis axis,
                                      final AnimatorProperty animator,
                                      final AbstractItem item, final float position,
                                      final boolean includePadding) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(animator, "The animator may not be null");
        Condition.INSTANCE.ensureNotNull(item, "The item may not be null");

        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            Toolbar[] toolbars = getTabSwitcher().getToolbars();
            animator.moveToY((float) ((getTabSwitcher().areToolbarsShown() && getTabSwitcher().isSwitcherShown() &&
                                toolbars != null ?
                                toolbars[TabSwitcher.PRIMARY_TOOLBAR_INDEX].getHeight() - (double)tabInset : 0) +
                                (includePadding ? getTabSwitcherPadding(axis, LayoutAlignment.START) : 0) + position));
        } else {
            Component view = item.getView();
            StackLayout.LayoutConfig layoutParams =
                    (StackLayout.LayoutConfig) view.getLayoutConfig();
            animator.moveToX((float) ((double)position + layoutParams.getMarginLeft() + (includePadding ?
                                getTabSwitcher().getPaddingLeft() / 2d -
                                        getTabSwitcher().getPaddingRight() / 2d : 0) -
                                (getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE &&
                                        getTabSwitcher().isSwitcherShown() ?
                                        stackedTabCount * stackedTabSpacing / 2d : 0)));
        }
    }

    @Override
    public final float getScale(final AbstractItem item, final boolean includePadding) {
        Condition.INSTANCE.ensureNotNull(item, "The item may not be null");
        Component view = item.getView();
        StackLayout.LayoutConfig layoutParams = (StackLayout.LayoutConfig) view.getLayoutConfig();
        float width = view.getWidth();
        float targetWidth = (float) ((double)width + layoutParams.getMarginLeft() + layoutParams.getMarginRight() -
                        (includePadding ?
                                getTabSwitcher().getPaddingLeft() + getTabSwitcher().getPaddingRight() :
                                0) - (getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ?
                        stackedTabCount * stackedTabSpacing : 0));
        if (width == 0 || targetWidth == 0)
            return 1;
        return targetWidth / width;
    }

    @Override
    public final void setScale(final Axis axis, final AbstractItem item,
                               final float scale) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(item, "The item may not be null");
        Component view = item.getView();

        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            view.setScaleY(scale);
        } else {
            view.setScaleX(scale);
        }
    }

    @Override
    public final void animateScale(final Axis axis,
                                   final AnimatorProperty animator,
                                   final float scale) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(animator, "The animator may not be null");

        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            animator.scaleY(scale);
        } else {
            animator.scaleX(scale);
        }
    }

    @Override
    public final float getSize(final Axis axis, final AbstractItem item) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(item, "The item may not be null");
        Component view = item.getView();

        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            return view.getHeight() * getScale(item);
        } else {
            return view.getWidth() * getScale(item);
        }
    }

    @Override
    public final float getPivot(final Axis axis, final AbstractItem item,
                                final AbstractDragTabsEventHandler.DragState dragState) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(item, "The item may not be null");
        Condition.INSTANCE.ensureNotNull(dragState, "The drag state may not be null");

        if (dragState == AbstractDragTabsEventHandler.DragState.SWIPE) {
            return getPivotWhenSwiping(axis, item);
        } else if (dragState == AbstractDragTabsEventHandler.DragState.OVERSHOOT_START) {
            return getPivotWhenOvershootingAtStart(axis, item);
        } else if (dragState == AbstractDragTabsEventHandler.DragState.OVERSHOOT_END) {
            return getPivotWhenOvershootingAtEnd(axis, item);
        } else {
            return getDefaultPivot(axis, item);
        }
    }

    @Override
    public final void setPivot(final Axis axis, final AbstractItem item,
                               final float pivot) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(item, "The item may not be null");
        Component view = item.getView();
        StackLayout.LayoutConfig layoutParams = (StackLayout.LayoutConfig) view.getLayoutConfig();

        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
            float newPivot = (float) ((double)pivot - layoutParams.getMarginTop() - (double)tabTitleContainerHeight);
            view.setTranslationY((float) ((double)view.getTranslationY() +
                                ((double)view.getPivotY() - (double)newPivot) * (1 - (double)view.getScaleY())));
            view.setPivotY(newPivot);
        } else {
            float newPivot = (float) ((double)pivot - layoutParams.getMarginLeft());
            view.setTranslationX((float) ((double)view.getTranslationX() +
                                (view.getPivotX() - (double)newPivot) * (1 - (double)view.getScaleX())));
            view.setPivotX(newPivot);
        }
    }

    @Override
    public final float getRotation(final Axis axis, final AbstractItem item) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(item, "The view may not be null");
        Component view = item.getView();

//        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
        return view.getRotation();
//        } else {
//            return view.getRotation();
//        }
    }

    @Override
    public final void setRotation(final Axis axis, final AbstractItem item,
                                  final float angle) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(item, "The item may not be null");
        Component view = item.getView();

//        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
        view.setRotation(
                getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ? -1 * angle : angle);
//        } else {
//            view.setRotation(
//                    getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ? -1 * angle : angle);
//        }
    }

    @Override
    public final void animateRotation(final Axis axis,
                                      final AnimatorProperty animator,
                                      final float angle) {
        Condition.INSTANCE.ensureNotNull(axis, "The axis may not be null");
        Condition.INSTANCE.ensureNotNull(animator, "The animator may not be null");

//        if (getOrientationInvariantAxis(axis) == Axis.DRAGGING_AXIS) {
        animator.rotate(
                getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ? -1 * angle : angle);
//        } else {
//            animator.rotate(
//                    getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ? -1 * angle : angle);
//        }
    }
}
