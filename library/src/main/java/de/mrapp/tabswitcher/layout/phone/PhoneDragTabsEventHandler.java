package de.mrapp.tabswitcher.layout.phone;

import de.mrapp.tabswitcher.Layout;
import de.mrapp.tabswitcher.ResourceTable;
import de.mrapp.tabswitcher.TabSwitcher;
import de.mrapp.tabswitcher.iterator.AbstractItemIterator;
import de.mrapp.tabswitcher.iterator.ItemIterator;
import de.mrapp.tabswitcher.layout.AbstractDragTabsEventHandler;
import de.mrapp.tabswitcher.layout.Arithmetics;
import de.mrapp.tabswitcher.model.AbstractItem;
import de.mrapp.tabswitcher.model.State;
import de.mrapp.tabswitcher.model.TabItem;
import de.mrapp.tabswitcher.util.DimensionUtil;
import de.mrapp.tabswitcher.util.PixelUtil;
import de.mrapp.tabswitcher.view.Toolbar;
import de.mrapp.util.Condition;
import de.mrapp.ohos_util.gesture.DragHelper;
import de.mrapp.ohos_util.view.AttachedViewRecycler;
import ohos.agp.utils.LayoutAlignment;

public class PhoneDragTabsEventHandler extends AbstractDragTabsEventHandler<PhoneDragTabsEventHandler.Callback> {
    /**
     * Defines the interface, a class, which should be notified about the events of a drag handler,
     * must implement.
     */
    public interface Callback extends AbstractDragTabsEventHandler.Callback {

        /**
         * The method, which is invoked, when tabs are overshooting at the start.
         *
         * @param position The position of the first tab in pixels as a {@link Float} value
         */
        void onStartOvershoot(float position);

        /**
         * The method, which is invoked, when the tabs should be tilted when overshooting at the
         * start.
         *
         * @param angle The angle, the tabs should be tilted by, in degrees as a {@link Float} value
         */
        void onTiltOnStartOvershoot(float angle);

        /**
         * The method, which is invoked, when the tabs should be tilted when overshooting at the
         * end.
         *
         * @param angle The angle, the tabs should be tilted by, in degrees as a {@link Float} value
         */
        void onTiltOnEndOvershoot(float angle);

    }

    /**
     * The view recycler, which allows to inflate the views, which are used to visualize the tabs,
     * whose positions and states are calculated by the drag handler.
     */
    private final AttachedViewRecycler<AbstractItem, ?> viewRecycler;

    /**
     * The drag helper, which is used to recognize drag gestures when overshooting.
     */
    private final DragHelper overshootDragHelper;

    /**
     * The maximum overshoot distance in pixels.
     */
    private final int maxOvershootDistance;

    /**
     * The maximum angle, tabs can be rotated by, when overshooting at the start, in degrees.
     */
    private final float maxStartOvershootAngle;

    /**
     * The maximum angle, tabs can be rotated by, when overshooting at the end, in degrees.
     */
    private final float maxEndOvershootAngle;

    /**
     * The number of tabs, which are contained by a stack.
     */
    private final int stackedTabCount;

    /**
     * The inset of tabs in pixels.
     */
    private final int tabInset;

    /**
     * Notifies the callback, that tabs are overshooting at the start.
     *
     * @param position The position of the first tab in pixels as a {@link Float} value
     */
    private void notifyOnStartOvershoot(final float position) {
        if (getCallback() != null) {
            getCallback().onStartOvershoot(position);
        }
    }

    /**
     * Notifies the callback, that the tabs should be tilted when overshooting at the start.
     *
     * @param angle The angle, the tabs should be tilted by, in degrees as a {@link Float} value
     */
    private void notifyOnTiltOnStartOvershoot(final float angle) {
        if (getCallback() != null) {
            getCallback().onTiltOnStartOvershoot(angle);
        }
    }

    /**
     * Notifies the callback, that the tabs should be titled when overshooting at the end.
     *
     * @param angle The angle, the tabs should be tilted by, in degrees as a {@link Float} value
     */
    private void notifyOnTiltOnEndOvershoot(final float angle) {
        if (getCallback() != null) {
            getCallback().onTiltOnEndOvershoot(angle);
        }
    }

    /**
     * Creates a new drag handler, which allows to calculate the position and state of tabs on touch
     * events, when using the smartphone layout.
     *
     * @param tabSwitcher  The tab switcher, whose tabs' positions and states should be calculated by the drag
     *                     handler, as an instance of the class {@link TabSwitcher}. The tab switcher may not be
     *                     null
     * @param arithmetics  The arithmetics, which should be used to calculate the position, size and rotation of
     *                     tabs, as an instance of the type {@link Arithmetics}. The arithmetics may not be
     *                     null
     * @param viewRecycler The view recycler, which allows to inflate the views, which are used to visualize the
     *                     tabs, whose positions and states should be calculated by the tab switcher, as an
     *                     instance of the class AttachedViewRecycler. The view recycler may not be null
     */
    public PhoneDragTabsEventHandler(final TabSwitcher tabSwitcher,
                                     final Arithmetics arithmetics,
                                     final AttachedViewRecycler<AbstractItem, ?> viewRecycler) {
        super(tabSwitcher, arithmetics, true);
        Condition.INSTANCE.ensureNotNull(viewRecycler, "The view recycler may not be null");
        this.viewRecycler = viewRecycler;
        this.overshootDragHelper = new DragHelper(0);
        this.tabInset = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_tab_inset)));
        int tab_count = 0;
        int max_start_overshoot_angle = 0;
        int max_end_overshoot_angle = 0;
        try {
            tab_count = tabSwitcher.getContext().getResourceManager().getElement(ResourceTable.Integer_phone_stacked_tab_count).getInteger();
            max_start_overshoot_angle = tabSwitcher.getContext().getResourceManager().getElement(ResourceTable.Integer_max_start_overshoot_angle).getInteger();
            max_end_overshoot_angle = tabSwitcher.getContext().getResourceManager().getElement(ResourceTable.Integer_max_end_overshoot_angle).getInteger();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.stackedTabCount = tab_count;
        int vp48 = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_max_overshoot_distance)));
        this.maxOvershootDistance = vp48;
        this.maxStartOvershootAngle = max_start_overshoot_angle;
        this.maxEndOvershootAngle = max_end_overshoot_angle;
    }

    @Override
    protected final AbstractItem getFocusedItem(final float position) {
        AbstractItemIterator iterator =
                new ItemIterator.Builder(getTabSwitcher(), viewRecycler).create();
        AbstractItem tabItem;

        while ((tabItem = iterator.next()) != null) {
            if (tabItem.getTag().getState() == State.FLOATING ||
                    tabItem.getTag().getState() == State.STACKED_START_ATOP) {
                Toolbar[] toolbars = getTabSwitcher().getToolbars();
                float toolbarHeight = getTabSwitcher().getLayout() != Layout.PHONE_LANDSCAPE &&
                        getTabSwitcher().areToolbarsShown() && toolbars != null ?
                        toolbars[TabSwitcher.PRIMARY_TOOLBAR_INDEX].getHeight() - tabInset : 0;
                float viewPosition =
                        (float) (getArithmetics().getPosition(Arithmetics.Axis.DRAGGING_AXIS, tabItem) + (double)toolbarHeight +
                                                        getArithmetics()
                                                                .getTabSwitcherPadding(Arithmetics.Axis.DRAGGING_AXIS, LayoutAlignment.START));

                if (viewPosition <= position) {
                    return tabItem;
                }
            }
        }

        return null;
    }

    @Override
    protected final float onOvershootStart(final float dragPosition,
                                           final float overshootThreshold) {
        float result = overshootThreshold;
        overshootDragHelper.update(dragPosition);
        float overshootDistance = overshootDragHelper.getDragDistance();

        if (overshootDistance < 0) {
            float absOvershootDistance = Math.abs(overshootDistance);
            float startOvershootDistance = 1;
            if (getTabSwitcher().getCount() >= stackedTabCount) {
                startOvershootDistance = maxOvershootDistance;
            }else if (getTabSwitcher().getCount() > 1) {
                startOvershootDistance = (float) maxOvershootDistance / (float) getTabSwitcher().getCount();
            }else {
                startOvershootDistance = 1;
            }

            if (absOvershootDistance <= startOvershootDistance) {
                float ratio =
                        Math.max(0, Math.min(1, absOvershootDistance / startOvershootDistance));
                AbstractItemIterator iterator =
                        new ItemIterator.Builder(getTabSwitcher(), viewRecycler).create();
                AbstractItem tabItem = iterator.getItem(0);
                float currentPosition = tabItem.getTag().getPosition();
                float position = (float) ((double)currentPosition - (currentPosition * ratio));
                notifyOnStartOvershoot(position);
            } else {
                float ratio =
                        (float) (((double)absOvershootDistance - (double)startOvershootDistance) / maxOvershootDistance);

                if (ratio >= 1) {
                    overshootDragHelper.setMinDragDistance(overshootDistance);
                    result = (float) ((double)dragPosition + maxOvershootDistance + (double)startOvershootDistance);
                }

                notifyOnTiltOnStartOvershoot(
                        Math.max(0, Math.min(1, ratio)) * maxStartOvershootAngle);
            }
        }

        return result;
    }

    @Override
    protected final float onOvershootEnd(final float dragPosition, final float overshootThreshold) {
        float result = overshootThreshold;
        overshootDragHelper.update(dragPosition);
        float overshootDistance = overshootDragHelper.getDragDistance();
        float ratio = overshootDistance / maxOvershootDistance;

        if (ratio >= 1) {
            overshootDragHelper.setMaxDragDistance(overshootDistance);
            result = (float) ((double)dragPosition - maxOvershootDistance);
        }

        notifyOnTiltOnEndOvershoot(Math.max(0, Math.min(1, ratio)) *
                -(getTabSwitcher().getCount() > 1 ? maxEndOvershootAngle : maxStartOvershootAngle));
        return result;
    }

    @Override
    protected final void onOvershootReverted() {
        overshootDragHelper.reset();
    }

    @Override
    protected final void onReset() {
        if (overshootDragHelper != null) {
            overshootDragHelper.reset();
        }
    }

    @Override
    protected final boolean isSwipeThresholdReached(final TabItem swipedTabItem) {
        return Math.abs(getArithmetics().getPosition(Arithmetics.Axis.ORTHOGONAL_AXIS, swipedTabItem)) >
                getArithmetics().getTabContainerSize(Arithmetics.Axis.ORTHOGONAL_AXIS) / 6f;
    }
}
