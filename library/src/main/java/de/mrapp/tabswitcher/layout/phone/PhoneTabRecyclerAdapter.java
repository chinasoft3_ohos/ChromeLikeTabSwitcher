/*
 * Copyright 2016 - 2020 Michael Rapp
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package de.mrapp.tabswitcher.layout.phone;

import de.mrapp.tabswitcher.*;
import de.mrapp.tabswitcher.iterator.ItemIterator;
import de.mrapp.tabswitcher.layout.AbstractTabRecyclerAdapter;
import de.mrapp.tabswitcher.layout.AbstractTabViewHolder;
import de.mrapp.tabswitcher.model.AbstractItem;
import de.mrapp.tabswitcher.model.TabItem;
import de.mrapp.tabswitcher.model.TabSwitcherModel;
import de.mrapp.tabswitcher.model.TabSwitcherStyle;
import de.mrapp.tabswitcher.util.DimensionUtil;
import de.mrapp.tabswitcher.util.LogUtil;
import de.mrapp.tabswitcher.util.PixelUtil;
import de.mrapp.util.Condition;
import de.mrapp.ohos_util.logging.LogLevel;
import de.mrapp.ohos_util.multithreading.AbstractDataBinder;
import de.mrapp.ohos_util.view.ViewRecycler;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.utils.Pair;

/**
 * A view recycler adapter, which allows to inflate the views, which are used to visualize the tabs
 * of a {@link TabSwitcher}, when using the smartphone layout.
 *
 * @author Michael Rapp
 * @since 0.1.0
 */
public class PhoneTabRecyclerAdapter extends AbstractTabRecyclerAdapter
        implements AbstractDataBinder.Listener<PixelMap, Tab, Image, TabItem> {

    /**
     * The view recycler, which allows to inflate the views, which are associated with tabs.
     */
    private final ViewRecycler<Tab, Void> tabViewRecycler;

    /**
     * The data binder, which allows to render previews of tabs.
     */
    private final AbstractDataBinder<PixelMap, Tab, Image, TabItem> dataBinder;

    /**
     * The inset of tabs in pixels.
     */
    private final int tabInset;

    /**
     * The width of the border, which is shown around the preview of tabs, in pixels.
     */
    private final int tabBorderWidth;

    /**
     * The height of the view group, which contains a tab's title and close button, in pixels.
     */
    private final int tabTitleContainerHeight;

    /**
     * Inflates the view, which is associated with a tab, and adds it to the view hierarchy.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose associated view should be inflated,
     *                as an instance of the class {@link TabItem}. The tab item may not be null
     */
    private void addContentView(final TabItem tabItem) {
        LogUtil.loge("=== addContentView PhoneTabRecyclerAdapter");
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) tabItem.getViewHolder();
        Component view = viewHolder.content;
        Tab tab = tabItem.getTab();
        if (view == null) {
            ComponentContainer parent = viewHolder.contentContainer;
            Pair<Component, ?> pair = tabViewRecycler.inflate(tab, parent);
            view = pair.f;
            ComponentContainer.LayoutConfig layoutParams =
                    new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
            Rect padding = getPadding();
            layoutParams.setMargins(padding.left, padding.top, padding.right, padding.bottom);
            LogUtil.loge("+++" + view.toString() + ": ");
            parent.addComponent(view, 0, layoutParams);
            viewHolder.content = view;
        } else {
            tabViewRecycler.getAdapter().onShowView(getModel().getContext(), view, tab, false);
        }

        viewHolder.previewImageView.setVisibility(Component.HIDE);
        viewHolder.previewImageView.setPixelMap(null);
        viewHolder.borderView.setVisibility(Component.HIDE);
    }

    /**
     * Renders and displays the preview of a tab.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose preview should be rendered, as an
     *                instance of the class {@link TabItem}. The tab item may not be null
     */
    private void renderPreview(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) tabItem.getViewHolder();
        viewHolder.borderView
                .setVisibility(getModel().isSwitcherShown() ? Component.VISIBLE : Component.HIDE);

        if (viewHolder.content != null) {
            tabViewRecycler.getAdapter().onRemoveView(viewHolder.content, tab);
            dataBinder.load(tab, viewHolder.previewImageView, false, tabItem);
            removeContentView(viewHolder, tab);
        } else {
            dataBinder.load(tab, viewHolder.previewImageView, tabItem);
        }
    }

    /**
     * Removes the view, which is associated with a tab, from its parent.
     *
     * @param viewHolder The view holder, which stores references to the tab's views, as an instance of the
     *                   class {@link PhoneTabViewHolder}. The view holder may not be null
     * @param tab        The tab, whose associated view should be removed, as an instance of the class {@link
     *                   Tab}. The tab may not be null
     */
    private void removeContentView(final PhoneTabViewHolder viewHolder,
                                   final Tab tab) {
        if (viewHolder.contentContainer.getChildCount() > 2) {
            viewHolder.contentContainer.removeComponentAt(0);
        }

        viewHolder.content = null;
        tabViewRecycler.remove(tab);
    }

    private void adaptTabBorderBackground(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        Element element = getStyle().getTabBorderBackground(tab);
        if(element == null)
            return;
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) tabItem.getViewHolder();
        viewHolder.tabContainer.setBackground(element);
    }

    /**
     * Adapts the visibility of a tab's background.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose backgrounds's visibility should be
     *                adapted, as an instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptBackgroundVisibility(final TabItem tabItem) {
        Component view = tabItem.getView();
        Element background = view.getBackgroundElement();
        if (background != null) {
            background.setAlpha(getModel().isSwitcherShown() ? 0xFF : 0x0);
        }
    }

    /**
     * Adapts the background color of a tab's content.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose content's background should be
     *                adapted, as an instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptTitleBackgroundColor(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        int color = getStyle().getTabTitleBackgroundColor(tab);
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) tabItem.getViewHolder();
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(rgbColor);
        viewHolder.titleContainer.setBackground(element);
    }

    /**
     * Adapts the background color of a tab's content.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose content's background should be
     *                adapted, as an instance of the class {@link TabItem}. The tab item may not be null
     */
    private void adaptContentBackgroundColor(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        int color = getStyle().getTabContentBackgroundColor(tab);
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) tabItem.getViewHolder();
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(rgbColor);
        viewHolder.contentContainer.setBackground(element);
    }

    private void adaptContentBorderBackground(final TabItem tabItem) {
        Tab tab = tabItem.getTab();
        Element element = getStyle().getTabContentBorderBackground(tab);
        if(element == null)
            return;
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) tabItem.getViewHolder();
        viewHolder.borderView.setBackground(element);
    }

    /**
     * Adapts the log level.
     */
    private void adaptLogLevel() {
        dataBinder.setLogLevel(getModel().getLogLevel());
    }

    /**
     * Adapts the padding of a tab.
     *
     * @param viewHolder The view holder, which stores references to the tab's views, as an instance of the
     *                   class {@link PhoneTabViewHolder}. The view holder may not be null
     */
    private void adaptPadding(final PhoneTabViewHolder viewHolder) {
        Rect padding = getPadding();

        if (viewHolder.content != null) {
            ComponentContainer.LayoutConfig contentLayoutParams = viewHolder.content.getLayoutConfig();
            contentLayoutParams
                    .setMargins(padding.left, padding.top, padding.right, padding.bottom);
        }

        ComponentContainer.LayoutConfig previewLayoutParams =
                viewHolder.previewImageView.getLayoutConfig();
        previewLayoutParams.setMargins(padding.left, padding.top, padding.right, padding.bottom);
    }

    /**
     * Returns the padding of a tab.
     *
     * @return A rect, which contains the left, top, right and bottom padding of a tab, as an
     * instance of the class {@link Rect}. The rect may not be null
     */

    private Rect getPadding() {
        if (getModel().isPaddingAppliedToTabs()) {
            return new Rect(getModel().getPaddingLeft(), getModel().getPaddingTop(),
                    getModel().getPaddingRight(), getModel().getPaddingBottom());
        } else {
            return new Rect(0, 0, 0, 0);
        }
    }

    /**
     * Creates a new view recycler adapter, which allows to inflate the views, which are used to
     * visualize the tabs of a {@link TabSwitcher}.
     *
     * @param tabSwitcher     The tab switcher as an instance of the class {@link TabSwitcher}. The tab switcher
     *                        may not be null
     * @param model           The model, which belongs to the tab switcher, as an instance of the class {@link
     *                        TabSwitcherModel}. The model may not be null
     * @param style           The style, which allows to retrieve style attributes of the tab switcher, as an
     *                        instance of the class {@link TabSwitcherStyle}. The style may not be null
     * @param tabViewRecycler The view recycler, which allows to inflate the views, which are associated with tabs,
     *                        as an instance of the class ViewRecycler. The view recycler may not be null
     */
    public PhoneTabRecyclerAdapter(final TabSwitcher tabSwitcher,
                                   final TabSwitcherModel model,
                                   final TabSwitcherStyle style,
                                   final ViewRecycler<Tab, Void> tabViewRecycler) {
        super(tabSwitcher, model, style);
        Condition.INSTANCE.ensureNotNull(tabViewRecycler, "The tab view recycler may not be null");
        this.tabViewRecycler = tabViewRecycler;
        this.dataBinder = new PreviewDataBinder(tabSwitcher, tabViewRecycler, model);
        this.dataBinder.addListener(this);
        Context context = tabSwitcher.getContext();
        this.tabInset = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(context.getString(ResourceTable.String_tab_inset)));

        this.tabBorderWidth = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(context.getString(ResourceTable.String_tab_border_width)));

        this.tabTitleContainerHeight = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(context.getString(ResourceTable.String_tab_title_container_height)));
        adaptLogLevel();
    }

    /**
     * Removes all previously rendered previews from the cache.
     */
    public final void clearCachedPreviews() {
        dataBinder.clearCache();
    }

    @Override
    protected final void onAdaptBackgroundColor(final int color,
                                                final TabItem tabItem) {
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) tabItem.getViewHolder();
        Element border = viewHolder.borderView.getBackgroundElement();
        border.setStateColorList(null, new int[]{color});
        border.setStateColorMode(BlendMode.MULTIPLY);
//        border.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
    }


    @Override
    protected final Component onInflateTabView(final LayoutScatter inflater,
                                               final ComponentContainer parent,
                                               final AbstractTabViewHolder viewHolder) {
        Component view = inflater.parse(ResourceTable.Layout_phone_tab, parent, false);
//        Element backgroundDrawable = convertResToElement(context, ResourceTable.Media_phone_tab_background);
//        ViewUtil.setBackground(view, backgroundDrawable);
        int padding = tabInset + tabBorderWidth;
        view.setPadding(padding, tabInset, padding, padding);

        ((PhoneTabViewHolder) viewHolder).tabContainer = (DirectionalLayout) view.findComponentById(ResourceTable.Id_tab_container);
        (viewHolder).titleContainer = (DirectionalLayout) view.findComponentById(ResourceTable.Id_tab_title_container);
        ((PhoneTabViewHolder) viewHolder).contentContainer = (ComponentContainer) view.findComponentById(ResourceTable.Id_content_container);
        ((PhoneTabViewHolder) viewHolder).previewImageView = (Image) view.findComponentById(ResourceTable.Id_preview_image_view);
        adaptPadding((PhoneTabViewHolder) viewHolder);
        ((PhoneTabViewHolder) viewHolder).borderView = view.findComponentById(ResourceTable.Id_border_view);
//        Element borderDrawable = convertResToElement(getModel().getContext(), ResourceTable.Media_phone_tab_border);
//        ViewUtil.setBackground(((PhoneTabViewHolder) viewHolder).borderView, borderDrawable);
        return view;
    }


    @Override
    protected final void onShowTabView(final Component view, final TabItem tabItem,
                                       final Integer... params) {
        StackLayout.LayoutConfig layoutParams =
                new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        int borderMargin = -(tabInset + tabBorderWidth);
        int topMargin = -(tabInset + tabTitleContainerHeight);
        int bottomMargin = params.length > 0 && params[0] != -1 ? params[0] : borderMargin;

        adaptTabBorderBackground(tabItem);
        adaptTitleBackgroundColor(tabItem);
        adaptContentBackgroundColor(tabItem);
        adaptBackgroundVisibility(tabItem);
        adaptContentBorderBackground(tabItem);

        LogUtil.loge("===onShowTabView-> " + " borderMargin: "+ borderMargin + " topMargin: " + topMargin + " bottomMargin: " + bottomMargin );
        LogUtil.loge("===switcherShown:" +  getModel().isSwitcherShown());
        if (!getModel().isSwitcherShown()) {
            layoutParams.setMarginLeft(borderMargin);
            layoutParams.setMarginTop(topMargin);
            layoutParams.setMarginRight(borderMargin);
            layoutParams.setMarginBottom(bottomMargin);
            view.setLayoutConfig(layoutParams);
            addContentView(tabItem);
        } else {
            borderMargin = 0;
            topMargin = 30;
            bottomMargin = 0;
            layoutParams.setMarginLeft(borderMargin);
            layoutParams.setMarginTop(topMargin);
            layoutParams.setMarginRight(borderMargin);
            layoutParams.setMarginBottom(bottomMargin);
            view.setLayoutConfig(layoutParams);
            renderPreview(tabItem);
        }
    }


    @Override
    protected final AbstractTabViewHolder onCreateTabViewHolder() {
        return new PhoneTabViewHolder();
    }


    @Override
    protected final Layout getLayout() {
        return Layout.PHONE_PORTRAIT;
    }

    @Override
    public final void onRemoveView(final Component view, final AbstractItem item) {
        if (item instanceof TabItem) {
            TabItem tabItem = (TabItem) item;
            PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) view.getTag();
            Tab tab = tabItem.getTab();
            removeContentView(viewHolder, tab);

            if (!dataBinder.isCached(tab)) {
                Element drawable = viewHolder.previewImageView.getImageElement();
                viewHolder.previewImageView.setPixelMap(null);

                if (drawable instanceof PixelMapElement) {
                    PixelMap bitmap = ((PixelMapElement) drawable).getPixelMap();

                    if (bitmap != null && !bitmap.isReleased()) {
                        bitmap.release();
                    }
                }
            } else {
                viewHolder.previewImageView.setPixelMap(null);
            }

            super.onRemoveView(view, tabItem);
        } else {
            throw new IllegalArgumentException("Unknown item type");
        }
    }

    @Override
    public final void onLogLevelChanged(final LogLevel logLevel) {
        adaptLogLevel();
    }

    @Override
    public final void onPaddingChanged(final int left, final int top, final int right,
                                       final int bottom) {
        ItemIterator iterator =
                new ItemIterator.Builder(getModel(), getViewRecyclerOrThrowException()).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() && item instanceof TabItem) {
                AbstractTabViewHolder viewHolder = ((TabItem) item).getViewHolder();
                adaptPadding((PhoneTabViewHolder) viewHolder);
            }
        }
    }

    @Override
    public final void onApplyPaddingToTabsChanged(final boolean applyPaddingToTabs) {
        ItemIterator iterator =
                new ItemIterator.Builder(getModel(), getViewRecyclerOrThrowException()).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() && item instanceof TabItem) {
                AbstractTabViewHolder viewHolder = ((TabItem) item).getViewHolder();
                adaptPadding((PhoneTabViewHolder) viewHolder);
            }
        }
    }

    @Override
    public final void onTabContentBackgroundColorChanged(final int color) {
        ItemIterator iterator =
                new ItemIterator.Builder(getModel(), getViewRecyclerOrThrowException()).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() && item instanceof TabItem) {
                adaptContentBackgroundColor((TabItem) item);
            }
        }
    }

    @Override
    public final void onContentBackgroundColorChanged(final Tab tab) {
        TabItem tabItem = getTabItem(tab);

        if (tabItem != null) {
            adaptContentBackgroundColor(tabItem);
        }
    }

    @Override
    public final boolean onLoadData(
            final AbstractDataBinder<PixelMap, Tab, Image, TabItem> dataBinder,
            final Tab key, final TabItem... params) {
        boolean result = true;

        for (TabPreviewListener listener : getModel().getTabPreviewListeners()) {
            result &= listener.onLoadTabPreview(getTabSwitcher(), key);
        }

        return result;
    }

    @Override
    public final void onFinished(
            final AbstractDataBinder<PixelMap, Tab, Image, TabItem> dataBinder,
            final Tab key, final PixelMap data, final Image view,
            final TabItem... params) {

    }

    @Override
    public final void onCanceled(
            final AbstractDataBinder<PixelMap, Tab, Image, TabItem> dataBinder) {

    }

}