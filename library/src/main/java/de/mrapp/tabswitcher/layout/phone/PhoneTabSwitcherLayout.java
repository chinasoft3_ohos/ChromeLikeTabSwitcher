package de.mrapp.tabswitcher.layout.phone;

import de.mrapp.tabswitcher.*;
import de.mrapp.tabswitcher.gesture.TouchEventDispatcher;
import de.mrapp.tabswitcher.iterator.AbstractItemIterator;
import de.mrapp.tabswitcher.iterator.ArrayItemIterator;
import de.mrapp.tabswitcher.iterator.ItemIterator;
import de.mrapp.tabswitcher.layout.AbstractDragTabsEventHandler;
import de.mrapp.tabswitcher.layout.AbstractTabRecyclerAdapter;
import de.mrapp.tabswitcher.layout.AbstractTabSwitcherLayout;
import de.mrapp.tabswitcher.layout.Arithmetics;
import de.mrapp.tabswitcher.model.*;
import de.mrapp.tabswitcher.util.DimensionUtil;
import de.mrapp.tabswitcher.util.LogUtil;
import de.mrapp.tabswitcher.util.PixelUtil;
import de.mrapp.tabswitcher.view.Toolbar;
import de.mrapp.util.Condition;
import de.mrapp.ohos_util.view.AbstractViewRecycler;
import de.mrapp.ohos_util.view.AttachedViewRecycler;
import de.mrapp.ohos_util.view.ViewRecycler;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.utils.Pair;

import java.util.Collections;

public class PhoneTabSwitcherLayout extends AbstractTabSwitcherLayout
        implements PhoneDragTabsEventHandler.Callback {

    /**
     * A layout listener, which encapsulates another listener, which is notified, when the listener
     * has been invoked a specific number of times.
     */
    private static class CompoundLayoutListener implements ComponentTreeObserver.WindowBoundListener {

        /**
         * The number of times, the listener must still be invoked, until the encapsulated listener
         * is notified.
         */
        private int count;

        /**
         * The encapsulated listener;
         */
        private final ComponentTreeObserver.WindowBoundListener listener;

        /**
         * Creates a new layout listener, which encapsulates another listener, which is notified,
         * when the listener has been invoked a specific number of times.
         *
         * @param count    The number of times, the listener should be invoked until the encapsulated
         *                 listener is notified, as an {@link Integer} value. The count must be greater than
         *                 0
         * @param listener The encapsulated listener, which should be notified, when the listener has been
         *                 notified the given number of times, as an instance of the type {@link
         *                 ComponentTreeObserver.WindowBoundListener} or null, if no listener should be notified
         */
        CompoundLayoutListener(final int count, final ComponentTreeObserver.WindowBoundListener listener) {
            Condition.INSTANCE.ensureGreater(count, 0, "The count must be greater than 0");
            this.count = count;
            this.listener = listener;
        }

        @Override
        public void onWindowBound() {
            if (--count == 0) {
                if (listener != null) {
                    listener.onWindowBound();
                }
            }
        }

        @Override
        public void onWindowUnbound() {

        }
    }

    /**
     * The ratio, which specifies the maximum space between the currently selected tab and its
     * predecessor in relation to the default space.
     */
    private static final float SELECTED_TAB_SPACING_RATIO = 1.5f;

    /**
     * The ratio, which specifies the minimum space between two neighboring tabs in relation to the
     * maximum space.
     */
    private static final float MIN_TAB_SPACING_RATIO = 0.375f;

    /**
     * The number of tabs, which are contained by a stack.
     */
    private final int stackedTabCount;

    /**
     * The inset of tabs in pixels.
     */
    private final int tabInset;

    /**
     * The width of the border, which is drawn around the preview of tabs.
     */
    private final int tabBorderWidth;

    /**
     * The height of a tab's title container in pixels.
     */
    private final int tabTitleContainerHeight;

    /**
     * The maximum camera distance, when tilting a tab, in pixels.
     */
    private final int maxCameraDistance;

    /**
     * The alpha of a tab, when it is swiped.
     */
    private final float swipedTabAlpha;

    /**
     * The scale of a tab, when it is swiped.
     */
    private final float swipedTabScale;

    /**
     * The duration of the animation, which is used to show the switcher.
     */
    private final long showSwitcherAnimationDuration;

    /**
     * The duration of the animation, which is used to hide the switcher.
     */
    private final long hideSwitcherAnimationDuration;

    /**
     * The duration of the animation, which is used to show or hide the toolbar.
     */
    private final long toolbarVisibilityAnimationDuration;

    /**
     * The delay of the animation, which is used to show or hide the toolbar.
     */
    private final long toolbarVisibilityAnimationDelay;

    /**
     * The duration of the animation, which is used to swipe tabs.
     */
    private final long swipeAnimationDuration;

    /**
     * The duration of the animation, which is used to relocate tabs.
     */
    private final long relocateAnimationDuration;

    /**
     * The duration of the animation, which is used to revert overshoots.
     */
    private final long revertOvershootAnimationDuration;

    /**
     * The duration of a reveal animation.
     */
    private final long revealAnimationDuration;

    /**
     * The duration of a peek animation.
     */
    private final long peekAnimationDuration;

    /**
     * The duration of the fade animation, which is used to show or hide the empty view.
     */
    private final long emptyViewAnimationDuration;

    /**
     * The maximum angle, tabs can be rotated by, when overshooting at the start, in degrees.
     */
    private final float maxStartOvershootAngle;

    /**
     * The maximum angle, tabs can be rotated by, when overshooting at the end, in degrees.
     */
    private final float maxEndOvershootAngle;

    /**
     * The distance between two neighboring tabs when being swiped in pixels.
     */
    private final int swipedTabDistance;

    /**
     * The drag handler, which is used by the layout.
     */
    private PhoneDragTabsEventHandler dragHandler;

    /**
     * The view recycler, which allows to recycle the views, which are associated with tabs.
     */
    private ViewRecycler<Tab, Void> contentViewRecycler;

    /**
     * The adapter, which allows to inflate the views, which are used to visualize tabs.
     */
    private PhoneTabRecyclerAdapter tabRecyclerAdapter;

    /**
     * The view recycler, which allows to recycle the views, which are used to visualize tabs.
     */
    private AttachedViewRecycler<AbstractItem, Integer> tabViewRecycler;

    /**
     * The view group, which contains the tab switcher's tabs.
     */
    private ComponentContainer tabContainer;

    /**
     * The toolbar, which is shown, when the tab switcher is shown.
     */
    private Toolbar toolbar;

    /**
     * The view, which is shown, when the tab switcher is empty.
     */
    private Component emptyView;

    /**
     * The bottom margin of a view, which visualizes a tab.
     */
    private int tabViewBottomMargin;

    /**
     * The animation, which is used to show or hide the toolbar.
     */
    private AnimatorProperty toolbarAnimation;

    /**
     * Adapts the decorator.
     */
    private void adaptDecorator() {
        tabRecyclerAdapter.clearCachedPreviews();
    }

    /**
     * Adapts the visibility of the view, which is shown, when the tab switcher is empty.
     *
     * @param animationDuration The duration of the fade animation, which should be used to show or hide the view, in
     *                          milliseconds as a {@link Long} value
     */
    private void adaptEmptyView(final long animationDuration) {
        detachEmptyView();

        if (getModel().isEmpty()) {
            emptyView = getModel().getEmptyView();

            if (emptyView != null) {
                emptyView.setAlpha(0);
                StackLayout.LayoutConfig layoutParams =
                        new StackLayout.LayoutConfig(emptyView.getLayoutConfig().width,
                                emptyView.getLayoutConfig().height);
                layoutParams.alignment = LayoutAlignment.CENTER;
                getTabSwitcher().addComponent(emptyView, 0, layoutParams);
                AnimatorProperty animation = emptyView.createAnimatorProperty();
                LogUtil.loge("=== adaptEmptyView" + animation.toString());
                animation.setDuration(
                        animationDuration == -1 ? emptyViewAnimationDuration : animationDuration);
                animation.alpha(1);
                animation.start();
            }
        }
    }

    /**
     * Detaches the view, which is shown, when the tab switcher is empty.
     */
    private void detachEmptyView() {
        if (emptyView != null) {
            getTabSwitcher().removeComponent(emptyView);
            emptyView = null;
        }
    }

    /**
     * Adapts the margin of the toolbar, which is shown, when the tab switcher is shown.
     */
    private void adaptToolbarMargin() {
        StackLayout.LayoutConfig layoutParams =
                (StackLayout.LayoutConfig) toolbar.getLayoutConfig();
        layoutParams.setMargins(getModel().getPaddingLeft(), getModel().getPaddingTop(),
                getModel().getPaddingRight(), 0);
    }

    /**
     * Calculates and returns the delays of subsequently started animations, depending on the
     * animations' durations.
     *
     * @param animationDuration The duration of the animations in milliseconds as a {@link Long} value
     * @return The delay, which has been calculated, as a {@link Long} value
     */
    private long calculateAnimationDelay(final long animationDuration) {
        return Math.round(animationDuration * 0.4);
    }

    /**
     * Calculates the position of a tab in relation to the position of its predecessor.
     *
     * @param predecessorPosition The position of the predecessor in pixels as a {@link Float} value
     * @param maxTabSpacing       The maximum space between two neighboring tabs in pixels as a {@link Float} value
     * @return The position, which has been calculated, as a {@link Float} value
     */
    private float calculateSuccessorPosition(final float predecessorPosition,
                                             final float maxTabSpacing) {
        float ratio = Math.min(1,
                predecessorPosition / calculateAttachedPosition(getTabSwitcher().getCount()));
        float minTabSpacing = calculateMinTabSpacing();
        return (float) ((double)predecessorPosition - minTabSpacing - (ratio * ((double)maxTabSpacing - minTabSpacing)));
    }

    /**
     * Calculates and returns the position of a tab, when it is swiped.
     *
     * @return The position, which has been calculated, in pixels as an {@link Float} value
     */
    private float calculateSwipePosition() {
        return getArithmetics().getTabContainerSize(Arithmetics.Axis.ORTHOGONAL_AXIS, true);
    }

    /**
     * Calculates and returns the maximum space between a specific tab and its predecessor. The
     * maximum space is greater for the currently selected tab.
     *
     * @param item The item, which corresponds to the tab, the maximum space should be returned for, as
     *             an instance of the class {@link AbstractItem} or null, if the default maximum space
     *             should be returned
     * @return The maximum space between the given tab and its predecessor in pixels as a {@link
     * Float} value
     */
    private float calculateMaxTabSpacing(final AbstractItem item) {
        float totalSpace = getArithmetics().getTabContainerSize(Arithmetics.Axis.DRAGGING_AXIS, false);
        float maxTabSpacing;
        int count = getModel().getCount();

        if (count <= 2) {
            maxTabSpacing = totalSpace * 0.66f;
        } else if (count == 3) {
            maxTabSpacing = totalSpace * 0.33f;
        } else if (count == 4) {
            maxTabSpacing = totalSpace * 0.3f;
        } else {
            maxTabSpacing = totalSpace * 0.25f;
        }

        return count > 4 && item != null &&
                ((TabItem) item).getTab() == getTabSwitcher().getSelectedTab() ?
                maxTabSpacing * SELECTED_TAB_SPACING_RATIO : maxTabSpacing;
    }

    /**
     * Calculates and returns the minimum space between two neighboring tabs.
     *
     * @return The minimum space between two neighboring tabs in pixels as a {@link Float} value
     */
    private float calculateMinTabSpacing() {
        return calculateMaxTabSpacing(null) * MIN_TAB_SPACING_RATIO;
    }

    /**
     * Calculates and returns the bottom margin of a specific tab.
     *
     * @param item The item, which corresponds to the tab, whose bottom margin should be calculated, as
     *             an instance of the class {@link AbstractItem}. The item may not be null
     * @return The bottom margin, which has been calculated, in pixels as an {@link Integer} value
     */
    private int calculateBottomMargin(final AbstractItem item) {
        Component view = item.getView();
        float scale =  getArithmetics().getScale(item, true);
        int viewHeight = view.getHeight();
        float tabHeight = (viewHeight - 2 * tabInset) *scale;
        float containerHeight = getArithmetics().getTabContainerSize(Arithmetics.Axis.Y_AXIS, false);
        int stackHeight = getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ? 0 :
                getStackedTabCount() * getStackedTabSpacing();
        return (int) Math.round((double)tabHeight + tabInset + stackHeight - (double)containerHeight);
    }

    /**
     * Animates the bottom margin of a specific view.
     *
     * @param view     The view, whose bottom margin should be animated, as an instance of the class {@link
     *                 Component}. The view may not be null
     * @param margin   The bottom margin, which should be set by the animation, as an {@link Integer} value
     * @param duration The duration of the animation in milliseconds as a {@link Long} value
     * @param delay    The delay of the animation in milliseconds as a {@link Long} value
     */
    private void animateBottomMargin(final Component view, final int margin, final long duration, final long delay) {
        StackLayout.LayoutConfig layoutParams = (StackLayout.LayoutConfig) view.getLayoutConfig();
        final int initialMargin = layoutParams.getMarginBottom();
        AnimatorValue animation = new AnimatorValue();
        int endValue = margin - initialMargin;
        LogUtil.loge("===endValue", endValue + "");
        animation.setDuration(duration);
        animation.setStateChangedListener(new AnimationListenerWrapper(null));
        animation.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animation.setDelay(delay);
        animation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {

            @Override
            public void onUpdate(AnimatorValue animatorValue, float percent) {
                StackLayout.LayoutConfig layoutParams =
                        (StackLayout.LayoutConfig) view.getLayoutConfig();
                int animatedValue = (int) (endValue * percent);
                layoutParams.setMarginBottom(initialMargin + animatedValue);
                view.setLayoutConfig(layoutParams);
            }
        });

        animation.start();
    }

    /**
     * Animates the visibility of the toolbar, which is shown, when the tab switcher is shown.
     *
     * @param visible True, if the toolbar should become visible, false otherwise
     * @param delay   The delay of the animation in milliseconds as a {@link Long} value
     */
    private void animateToolbarVisibility(final boolean visible, final long delay) {
        if (toolbarAnimation != null) {
            toolbarAnimation.cancel();
        }

        toolbarAnimation = toolbar.createAnimatorProperty();
        toolbarAnimation.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        toolbarAnimation.setDuration(toolbarVisibilityAnimationDuration);
        toolbarAnimation.setStateChangedListener(createToolbarAnimationListener(visible));
        toolbarAnimation.setDelay(delay);
        toolbarAnimation.alpha(visible ? 1 : 0);
        toolbarAnimation.start();
    }

    /**
     * Shows the tab switcher in an animated manner.
     *
     * @param referenceTabIndex    The index of the tab, which is used as a reference, when restoring the positions of
     *                             tabs, as an {@link Integer} value or -1, if the positions of tabs should not be
     *                             restored
     * @param referenceTabPosition The position of tab, which is used as a reference, when restoring the positions of
     *                             tabs, in relation to the available space as a {@link Float} value or -1, if the
     *                             positions of tabs should not be restored
     */
    private void animateShowSwitcher(final int referenceTabIndex,
                                     final float referenceTabPosition) {
        LogUtil.loge("=== animateShowSwitcher");
        AbstractItem[] items = calculateInitialItems(referenceTabIndex, referenceTabPosition);
        AbstractItemIterator iterator = new InitialItemIteratorBuilder(items).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (((TabItem) item).getTab() == getModel().getSelectedTab() || item.isVisible()) {
                tabViewRecycler.inflate(item);
                //首次没有调用animateShowSwitcher，导致第一个位置不正确
//                Component view = item.getView();
                //!ViewCompat.isLaidOut(view)
//                if (view.getWidth() > 0 && view.getHeight() > 0) {
//                    view.getComponentTreeObserver().addWindowBoundListener(
//                            new LayoutListenerWrapper(view, createShowSwitcherLayoutListener(item)));
//                } else {
                    animateShowSwitcher(item, createUpdateViewAnimationListener(item));
//                }
            }
        }

        animateToolbarVisibility(getModel().areToolbarsShown(), toolbarVisibilityAnimationDelay);
    }

    /**
     * Calculates and returns the items, which correspond to the tabs, when the tab switcher is
     * shown initially.
     *
     * @param referenceTabIndex    The index of the tab, which is used as a reference, when restoring the positions of
     *                             tabs, as an {@link Integer} value or -1, if the positions of tabs should not be
     *                             restored
     * @param referenceTabPosition The position of tab, which is used as a reference, when restoring the positions of
     *                             tabs, in relation to the available space as a {@link Float} value or -1, if the
     *                             positions of tabs should not be restored
     * @return An array, which contains the items, as an array of the type {@link AbstractItem}. The
     * array may not be null
     */

    private AbstractItem[] calculateInitialItems(final int referenceTabIndex,
                                                 final float referenceTabPosition) {
        dragHandler.reset();
        setFirstVisibleIndex(-1);
        AbstractItem[] items = new AbstractItem[getModel().getCount()];

        if (!getModel().isEmpty()) {
            int selectedTabIndex = getModel().getSelectedTabIndex();
            float attachedPosition = calculateAttachedPosition(getModel().getCount());
            int referenceIndex =
                    referenceTabIndex != -1 && referenceTabPosition != -1 ? referenceTabIndex :
                            selectedTabIndex;
            float referencePosition = referenceTabIndex != -1 && referenceTabPosition != -1 ?
                    referenceTabPosition *
                            getArithmetics().getTabContainerSize(Arithmetics.Axis.DRAGGING_AXIS, false) :
                    attachedPosition;
            referencePosition =
                    Math.min(calculateMaxEndPosition(referenceIndex), referencePosition);
            AbstractItemIterator iterator =
                    new InitialItemIteratorBuilder(items).start(referenceIndex).create();
            AbstractItem item;

            while ((item = iterator.next()) != null) {
                AbstractItem predecessor = iterator.previous();
                float position;

                if (item.getIndex() == getModel().getCount() - 1) {
                    position = 0;
                } else if (item.getIndex() == referenceIndex) {
                    position = referencePosition;
                } else {
                    position = calculateSuccessorPosition(item, predecessor);
                }

                State predecessorState = null;
                if (item.getIndex() == referenceIndex && referenceIndex > 0) {
                    predecessorState = State.FLOATING;
                } else if (predecessor != null) {
                    predecessorState = predecessor.getTag().getState();
                }
                Pair<Float, State> pair = clipPosition(item.getIndex(), position, predecessorState);
                item.getTag().setPosition(pair.f);
                item.getTag().setState(pair.s);

                if (getFirstVisibleIndex() == -1 && pair.s != State.STACKED_END &&
                        pair.s != State.HIDDEN) {
                    setFirstVisibleIndex(item.getIndex());
                }

                if (pair.s == State.STACKED_START || pair.s == State.STACKED_START_ATOP) {
                    break;
                }
            }

            boolean overshooting = referenceIndex == getModel().getCount() - 1 ||
                    isOvershootingAtEnd(AbstractDragTabsEventHandler.DragState.NONE, iterator);
            iterator = new InitialItemIteratorBuilder(items).reverse(true).start(referenceIndex - 1)
                    .create();
            float minTabSpacing = calculateMinTabSpacing();
            float defaultTabSpacing = calculateMaxTabSpacing(null);
            AbstractItem selectedItem =
                    TabItem.create(getModel(), tabViewRecycler, selectedTabIndex);
            float maxTabSpacing = calculateMaxTabSpacing(selectedItem);
            AbstractItem currentReferenceItem = iterator.getItem(referenceIndex);

            while ((item = iterator.next()) != null &&
                    (overshooting || item.getIndex() < referenceIndex)) {
                float currentTabSpacing = calculateMaxTabSpacing(currentReferenceItem);
                AbstractItem predecessor = iterator.peek();
                Pair<Float, State> pair;

                if (overshooting) {
                    float position;

                    if (referenceIndex > item.getIndex()) {
                        position = (float) ((double)maxTabSpacing +
                                                        (((double)getModel().getCount() - 1 - item.getIndex() - 1) *
                                                                (double)defaultTabSpacing));
                    } else {
                        position =
                                (getModel().getCount() - 1 - item.getIndex()) * defaultTabSpacing;
                    }

                    pair = clipPosition(item.getIndex(), position, predecessor);
                } else if (referencePosition >= (double)attachedPosition - currentTabSpacing) {
                    float position;

                    if (selectedTabIndex > item.getIndex() && selectedTabIndex <= referenceIndex) {
                        position = (float) ((double)referencePosition + maxTabSpacing +
                                                        ((referenceIndex - item.getIndex() - 1) * defaultTabSpacing));
                    } else {
                        position = (float) ((double)referencePosition +
                                                        ((referenceIndex - item.getIndex()) * defaultTabSpacing));
                    }

                    pair = clipPosition(item.getIndex(), position, predecessor);
                } else {
                    AbstractItem successor = iterator.previous();
                    float successorPosition = successor.getTag().getPosition();
                    float position = (float) (((double)attachedPosition * ((double)successorPosition + minTabSpacing)) /
                                                ((double)minTabSpacing + attachedPosition - currentTabSpacing));
                    pair = clipPosition(item.getIndex(), position, predecessor);

                    if ((double)pair.f >= (double)attachedPosition - (double)currentTabSpacing) {
                        currentReferenceItem = item;
                        referencePosition = pair.f;
                        referenceIndex = item.getIndex();
                    }
                }

                item.getTag().setPosition(pair.f);
                item.getTag().setState(pair.s);

                if ((getFirstVisibleIndex() == -1 || getFirstVisibleIndex() > item.getIndex()) &&
                        pair.s == State.FLOATING) {
                    setFirstVisibleIndex(item.getIndex());
                }
            }
        }

        dragHandler.setCallback(this);
        return items;
    }

    /**
     * Adds all tabs, which are contained by an array, to the tab switcher.
     *
     * @param index     The index, the first tab should be added at, as an {@link Integer} value
     * @param tabs      The array, which contains the tabs, which should be added, as an array of the type
     *                  {@link Tab}. The array may not be null
     * @param animation The animation, which should be used to add the tabs, as an instance of the class
     *                  {@link Animation}. The animation may not be null
     */
    private void addAllTabs(final int index, final Tab[] tabs,
                            final Animation animation) {
        LogUtil.loge("=== addAllTabs");
        if (tabs.length > 0) {
            if (getModel().isSwitcherShown()) {
                SwipeAnimation swipeAnimation =
                        animation instanceof SwipeAnimation ? (SwipeAnimation) animation :
                                new SwipeAnimation.Builder().create();
                AbstractItem[] items = new AbstractItem[tabs.length];
                ComponentTreeObserver.WindowBoundListener compoundListener = new CompoundLayoutListener(tabs.length,
                        createSwipeLayoutListener(items, swipeAnimation));

                for (int i = 0; i < tabs.length; i++) {
                    Tab tab = tabs[i];
                    AbstractItem item = TabItem.create(getModel(), index + i, tab);
                    items[i] = item;
                    inflateView(item, compoundListener);
                }
            } else if (!getModel().isSwitcherShown()) {
                toolbar.setAlpha(0);

                if (getModel().getSelectedTab() == tabs[0]) {
                    AbstractItem item = TabItem.create(getTabSwitcher(), tabViewRecycler, index);
                    inflateView(item, createAddSelectedTabLayoutListener(item));
                }
            }
        }
    }

    /**
     * Animates the position and size of a specific tab in order to show the tab switcher.
     *
     * @param item     item
     * @param listener listener
     */
    private void animateShowSwitcher(final AbstractItem item,
                                     final Animator.StateChangedListener listener) {
        animateShowSwitcher(item, showSwitcherAnimationDuration,
                Animator.CurveType.ACCELERATE_DECELERATE, listener);
    }

    /**
     * Animates the position and size of a specific tab in order to show the tab switcher.
     *
     * @param item      The item, which corresponds to the tab, which should be animated, as an instance of
     *                  the class {@link AbstractItem}. The item may not be null
     * @param duration  The duration of the animation in milliseconds as a {@link Long} value
     * @param curveType The curveType, which should be used by the animation, as an instance of the type
     *                  {@link Animator.CurveType}. The curveType may not be null
     * @param listener  The listener, which should be notified about the animation's progress, as an instance
     *                  of the type {@link Animator.StateChangedListener} or null, if no listener should be notified
     */
    private void animateShowSwitcher(final AbstractItem item, final long duration,
                                     final int curveType,
                                     final Animator.StateChangedListener listener) {
        Component view = item.getView();
        StackLayout.LayoutConfig layoutParams = (StackLayout.LayoutConfig) view.getLayoutConfig();
        view.setTranslationX(layoutParams.getMarginLeft());
        view.setTranslationY(layoutParams.getMarginTop());
        getArithmetics().setScale(Arithmetics.Axis.DRAGGING_AXIS, item, 1);
        getArithmetics().setScale(Arithmetics.Axis.ORTHOGONAL_AXIS, item, 1);
        getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item,
                getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
        getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                getArithmetics().getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
        float scale = getArithmetics().getScale(item, true);
        int selectedTabIndex = getModel().getSelectedTabIndex();

        if (item.getIndex() < selectedTabIndex) {
            getArithmetics().setPosition(Arithmetics.Axis.DRAGGING_AXIS, item,
                    getArithmetics().getTabContainerSize(Arithmetics.Axis.DRAGGING_AXIS));
        } else if (item.getIndex() > selectedTabIndex) {
            getArithmetics().setPosition(Arithmetics.Axis.DRAGGING_AXIS, item,
                    getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ? 0 :
                            layoutParams.getMarginTop());
        }

        if (tabViewBottomMargin == -1) {
            tabViewBottomMargin = calculateBottomMargin(item);
        }
        animateBottomMargin(view, tabViewBottomMargin, duration, 0);
        AnimatorProperty animation = view.createAnimatorProperty();
        LogUtil.loge("=== animateShowSwitcher" + animation.toString());
        animation.setDuration(duration);
        animation.setCurveType(curveType);
        animation.setStateChangedListener(new AnimationListenerWrapper(listener));
        getArithmetics().animateScale(Arithmetics.Axis.DRAGGING_AXIS, animation, scale);
        getArithmetics().animateScale(Arithmetics.Axis.ORTHOGONAL_AXIS, animation, scale);
        getArithmetics().animatePosition(Arithmetics.Axis.DRAGGING_AXIS, animation, item, item.getTag().getPosition(), true);
        LogUtil.loge("=== dragging_axis: " + item.getTag().getPosition() + "");
        getArithmetics().animatePosition(Arithmetics.Axis.ORTHOGONAL_AXIS, animation, item, 0, true);
        animation.start();
    }

    /**
     * Hides the tab switcher in an animated manner.
     */
    private void animateHideSwitcher() {
        LogUtil.loge("=== animateHideSwitcher");
        dragHandler.setCallback(null);
        ItemIterator iterator =
                new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated()) {
                animateHideSwitcher(item, item.getIndex() == getModel().getSelectedTabIndex() ?
                        createHideSwitcherAnimationListener() : null);
            } else if (((TabItem) item).getTab() == getModel().getSelectedTab()) {
                inflateAndUpdateView(item, false, createHideSwitcherLayoutListener(item));
            }
        }

        animateToolbarVisibility(getModel().areToolbarsShown() && getModel().isEmpty(), 0);
    }

    /**
     * Animates the position and size of a specific tab in order to hide the tab switcher.
     *
     * @param item     The item, which corresponds to the tab, which should be animated, as an instance of
     *                 the class {@link AbstractItem}. The item may not be null
     * @param listener The listener, which should be notified about the animation's progress, as an instance
     *                 of the type {@link Animator.StateChangedListener} or null, if no listener should be notified
     */
    private void animateHideSwitcher(final AbstractItem item,
                                     final Animator.StateChangedListener listener) {
        animateHideSwitcher(item, hideSwitcherAnimationDuration, Animator.CurveType.ACCELERATE_DECELERATE, 0, listener);
    }

    /**
     * Animates the position and size of a specific tab in order to hide the tab switcher.
     *
     * @param item      The item, which corresponds to the tab, which should be animated, as an instance of
     *                  the class {@link AbstractItem}. The item may not be null
     * @param duration  The duration of the animation in milliseconds as a {@link Long} value
     * @param curveType The curveType, which should be used by the animation, as an instance of the class
     *                  {@link ohos.agp.animation.Animator.CurveType}. The curveType may not be null
     * @param delay     The delay of the animation in milliseconds as a {@link Long} value
     * @param listener  The listener, which should be notified about the animation's progress, as an instance
     *                  of the type {@link Animator.StateChangedListener} or null, if no listener should be notified
     */
    private void animateHideSwitcher(final AbstractItem item, final long duration,
                                     final int curveType, final long delay,
                                     final Animator.StateChangedListener listener) {
        Component view = item.getView();
        animateBottomMargin(view, -(tabInset + tabBorderWidth), duration, delay);
        AnimatorProperty animation = view.createAnimatorProperty();
        LogUtil.loge("=== animateHideSwitcher" + animation.toString());
        animation.setDuration(duration);
        animation.setCurveType(curveType);
        if (listener != null) {
            animation.setStateChangedListener(new AnimationListenerWrapper(listener));
        }
        getArithmetics().animateScale(Arithmetics.Axis.DRAGGING_AXIS, animation, 1);
        getArithmetics().animateScale(Arithmetics.Axis.ORTHOGONAL_AXIS, animation, 1);
        StackLayout.LayoutConfig layoutParams = (StackLayout.LayoutConfig) view.getLayoutConfig();
        getArithmetics().animatePosition(Arithmetics.Axis.ORTHOGONAL_AXIS, animation, item,
                getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ? layoutParams.getMarginTop() :
                        0);
        int selectedTabIndex = getModel().getSelectedTabIndex();

        if (item.getIndex() < selectedTabIndex) {
            getArithmetics().animatePosition(Arithmetics.Axis.DRAGGING_AXIS, animation, item,
                    getArithmetics().getTabContainerSize(Arithmetics.Axis.DRAGGING_AXIS));
        }else {
            getArithmetics().animatePosition(Arithmetics.Axis.DRAGGING_AXIS, animation, item,
                    getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE ? 0 :
                            layoutParams.getMarginTop());
        }

        animation.setDelay(delay);
        animation.start();
    }

    /**
     * Animates the position, size and alpha of a specific tab in order to swipe it orthogonally.
     *
     * @param item            The item, corresponds to the tab, which should be animated, as an instance of the
     *                        class {@link AbstractItem}. The item may not be null
     * @param remove          True, if the tab should be removed after the animation has finished, false otherwise
     * @param delayMultiplier The multiplied, which should be used to calculate the delay after which the animation
     *                        should be started, by being multiplied with the default delay, as an {@link Integer}
     *                        value
     * @param swipeAnimation  The animation, which should be used, as an instance of the class {@link
     *                        SwipeAnimation}. The animation may not be null
     * @param listener        The listener, which should be notified about the progress of the animation, as an
     *                        instance of the type {@link Animator.StateChangedListener} or null, if no listener should be
     *                        notified
     */
    private void animateSwipe(final AbstractItem item, final boolean remove,
                              final int delayMultiplier,
                              final SwipeAnimation swipeAnimation,
                              final Animator.StateChangedListener listener) {
        Component view = item.getView();
        float currentScale = getArithmetics().getScale(item, true);
        float swipePosition = calculateSwipePosition();
        float targetPosition = 0;
        if (remove) {
            if (swipeAnimation.getDirection() == SwipeAnimation.SwipeDirection.LEFT_OR_TOP) {
                targetPosition = -1 * swipePosition;
            } else {
                targetPosition = swipePosition;
            }
        }
        float currentPosition = getArithmetics().getPosition(Arithmetics.Axis.ORTHOGONAL_AXIS, item);
        float distance = (float) Math.abs((double)targetPosition - currentPosition);
        long animationDuration = swipeAnimation.getDuration() != -1 ? swipeAnimation.getDuration() :
                Math.round(swipeAnimationDuration * (distance / swipePosition));
        AnimatorProperty animation = view.createAnimatorProperty();
        LogUtil.loge("=== animateSwipe " + animation.toString() + " orthogonally");

        animation.setCurveType(
                swipeAnimation.getCurveType() != -1 ? swipeAnimation.getCurveType() : Animator.CurveType.ACCELERATE_DECELERATE);
        animation.setStateChangedListener(new AnimationListenerWrapper(listener));
        animation.setDuration(animationDuration);
        getArithmetics()
                .animatePosition(Arithmetics.Axis.ORTHOGONAL_AXIS, animation, item, targetPosition, true);
        getArithmetics().animateScale(Arithmetics.Axis.ORTHOGONAL_AXIS, animation,
                remove ? swipedTabScale * currentScale : currentScale);
        getArithmetics().animateScale(Arithmetics.Axis.DRAGGING_AXIS, animation,
                remove ? swipedTabScale * currentScale : currentScale);
        animation.alpha(remove ? swipedTabAlpha : 1);
        animation.setDelay(delayMultiplier * calculateAnimationDelay(animationDuration));
        animation.start();
    }

    /**
     * Animates a tab to be swiped horizontally.
     *
     * @param tabItem           The tab item, which corresponds to the tab, which should be swiped, as an instance of
     *                          the class {@link TabItem}. The tab item may not be null
     * @param targetPosition    The position on the x-axis, the tab should be moved to, in pixels as a {@link Float}
     *                          value
     * @param selected          True, if the tab should become the selected one, false otherwise
     * @param animationDuration The duration of the animation in milliseconds as a {@link Long} value
     * @param velocity          The velocity of the drag gesture, which caused the tab to be swiped, in pixels per
     *                          second as a {@link Float} value
     */
    private void animateSwipe(final TabItem tabItem, final float targetPosition,
                              final boolean selected, final long animationDuration,
                              final float velocity) {
        Component view = tabItem.getView();
        float currentPosition = getArithmetics().getPosition(Arithmetics.Axis.X_AXIS, tabItem);
        float distance = (float) Math.abs((double)targetPosition - currentPosition);
        float maxDistance = (float) ((double)getArithmetics().getSize(Arithmetics.Axis.X_AXIS, tabItem) + swipedTabDistance);
        long duration = velocity > 0 ? Math.round((distance / velocity) * 1000) :
                Math.round(animationDuration * (distance / maxDistance));
        AnimatorProperty animation = view.createAnimatorProperty();
        LogUtil.loge("=== animateSwipe " + animation.toString() + "horizontally");
        animation.setStateChangedListener(new AnimationListenerWrapper(
                selected ? createSwipeSelectedTabAnimationListener(tabItem) :
                        createSwipeNeighborAnimationListener(tabItem)));
        animation.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animation.setDuration(duration);
        animation.setDelay(0);
        getArithmetics().animatePosition(Arithmetics.Axis.X_AXIS, animation, tabItem, targetPosition, true);
        animation.start();
    }

    /**
     * Animates the removal of a specific tab.
     *
     * @param removedItem    The item, corresponds to the tab, which should be animated, as an instance of the
     *                       class {@link AbstractItem}. The item may not be null
     * @param swipeAnimation The animation, which should be used, as an instance of the class {@link
     *                       SwipeAnimation}. The animation may not be null
     */
    private void animateRemove(final AbstractItem removedItem,
                               final SwipeAnimation swipeAnimation) {
        getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, removedItem,
                getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, removedItem, AbstractDragTabsEventHandler.DragState.SWIPE));
        getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, removedItem,
                getArithmetics().getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, removedItem, AbstractDragTabsEventHandler.DragState.SWIPE));
        animateSwipe(removedItem, true, 0, swipeAnimation,
                createRemoveAnimationListener(removedItem, swipeAnimation));
    }

    /**
     * Animates the position of a specific tab in order to relocate it.
     *
     * @param item            The item, which corresponds to the tab, which should be animated, as an instance of
     *                        the class {@link AbstractItem}. The item may not be null
     * @param position        The position, the tab should be relocated to, in pixels as a {@link Float} value
     * @param tag             The tag, which should be applied to the given item, as an instance of the class
     *                        {@link Tag} or null, if no tag should be applied
     * @param delayMultiplier The multiplier, which should be used to calculate the delay of the relocate
     *                        animation, by being multiplied with the default delay, as an {@link Integer} value
     * @param listener        The listener, which should be notified about the progress of the relocate animation,
     *                        as an instance of the type {@link Animator.StateChangedListener} or null, if no listener should be
     *                        notified
     * @param swipeAnimation  The animation, which has been used to add or remove the tab, which caused the other
     *                        tabs to be relocated, as an instance of the class {@link SwipeAnimation}. The
     *                        animation may not be null
     */
    private void animateRelocate(final AbstractItem item, final float position,
                                 final Tag tag, final int delayMultiplier,
                                 final Animator.StateChangedListener listener,
                                 final SwipeAnimation swipeAnimation) {
        if (tag != null) {
//            item.getView().setTag(R.id.tag_properties, tag);
            Component component = item.getView();
            Component titleComponent = component.findComponentById(ResourceTable.Id_tab_title_container);
            titleComponent.setTag(tag);
            item.setTag(tag);
        }

        Component view = item.getView();
        long animationDuration = swipeAnimation.getRelocateAnimationDuration() != -1 ?
                swipeAnimation.getRelocateAnimationDuration() : relocateAnimationDuration;
        AnimatorProperty animation = view.createAnimatorProperty();
        LogUtil.loge("=== animateRelocate " + animation.toString());
        animation.setStateChangedListener(new AnimationListenerWrapper(listener));
        animation.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animation.setDuration(animationDuration);
        getArithmetics().animatePosition(Arithmetics.Axis.DRAGGING_AXIS, animation, item, position, true);
        animation.setDelay(delayMultiplier * calculateAnimationDelay(animationDuration));
        animation.start();
    }

    /**
     * Animates reverting an overshoot at the start.
     */
    private void animateRevertStartOvershoot() {
        boolean tilted = animateTilt(Animator.CurveType.ACCELERATE, maxStartOvershootAngle,
                createRevertStartOvershootAnimationListener());

        if (!tilted) {
            animateRevertStartOvershoot(Animator.CurveType.ACCELERATE_DECELERATE);
        }
    }

    /**
     * Animates reverting an overshoot at the start using a specific curveType.
     *
     * @param curveType The curveType, which should be used by the animation, as an instance of the type
     *                  {@link ohos.agp.animation.Animator.CurveType}. The curveType may not be null
     */
    private void animateRevertStartOvershoot(final int curveType) {
        AbstractItem item = TabItem.create(getTabSwitcher(), tabViewRecycler, 0);
        getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item,
                getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
        getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                getArithmetics().getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
        float position = getArithmetics().getPosition(Arithmetics.Axis.DRAGGING_AXIS, item);
        float targetPosition = item.getTag().getPosition();
        final float startPosition = getArithmetics().getPosition(Arithmetics.Axis.DRAGGING_AXIS, item);
        AnimatorValue animation = new AnimatorValue();
        float range = (float) ((double)targetPosition - position);
        animation.setDuration((long) Math.round(revertOvershootAnimationDuration * Math.abs(
                ((double)targetPosition - position) /
                        (double) (getStackedTabCount() * getStackedTabSpacing()))));
        animation.setStateChangedListener(new AnimationListenerWrapper(null));
        animation.setCurveType(curveType);
        animation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {

            @Override
            public void onUpdate(AnimatorValue animatorValue, float percent) {
                float animatedValue = percent * range;
                ItemIterator iterator = new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).create();
                AbstractItem item;

                while ((item = iterator.next()) != null) {
                    if (item.getIndex() == 0) {
                        getArithmetics().setPosition(Arithmetics.Axis.DRAGGING_AXIS, item,
                                (float) ((double)startPosition + animatedValue));
                    } else if (item.isInflated()) {
                        AbstractItem firstItem = iterator.first();
                        Component view = item.getView();
                        view.setVisibility(
                                getArithmetics().getPosition(Arithmetics.Axis.DRAGGING_AXIS, firstItem) <=
                                        getArithmetics().getPosition(Arithmetics.Axis.DRAGGING_AXIS, item) ?
                                        Component.INVISIBLE : Component.VISIBLE);
                    }
                }
            }
        });

        animation.start();
    }

    /**
     * Animates reverting an overshoot at the end.
     */
    private void animateRevertEndOvershoot() {
        animateTilt(Animator.CurveType.ACCELERATE_DECELERATE, maxEndOvershootAngle, null);
    }

    /**
     * Animates to rotation of all tabs to be reset to normal.
     *
     * @param curveType The curveType, which should be used by the animation, as an instance of the type
     *                  {@link ohos.agp.animation.Animator.CurveType}. The curveType may not be null
     * @param maxAngle  The angle, the tabs may be rotated by at maximum, in degrees as a {@link Float}
     *                  value
     * @param listener  The listener, which should be notified about the animation's progress, as an instance
     *                  of the type {@link Animator.StateChangedListener} or null, if no listener should be notified
     * @return True, if at least one tab was animated, false otherwise
     */
    private boolean animateTilt(final int curveType, final float maxAngle,
                                final Animator.StateChangedListener listener) {
        ItemIterator iterator =
                new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).reverse(true).create();
        AbstractItem item;
        boolean result = false;

        while ((item = iterator.next()) != null) {
            if (item.isInflated() &&
                    getArithmetics().getRotation(Arithmetics.Axis.ORTHOGONAL_AXIS, item) != 0) {
                Component view = item.getView();
                AnimatorProperty animation = view.createAnimatorProperty();
                LogUtil.loge("=== animateTilt" + animation.toString());
                animation.setStateChangedListener(new AnimationListenerWrapper(
                        createRevertOvershootAnimationListener(item, !result ? listener : null)));
                animation.setDuration(Math.round(revertOvershootAnimationDuration *
                        (Math.abs(getArithmetics().getRotation(Arithmetics.Axis.ORTHOGONAL_AXIS, item)) /
                                maxAngle)));
                animation.setCurveType(curveType);
                getArithmetics().animateRotation(Arithmetics.Axis.ORTHOGONAL_AXIS, animation, 0);
                animation.start();
                result = true;
            }
        }

        return result;
    }

    /**
     * Starts a reveal animation to add a specific tab.
     *
     * @param item            The item, which corresponds to the tab, which should be added, as an instance of the
     *                        class {@link AbstractItem}. The item may not be null
     * @param revealAnimation The reveal animation, which should be started, as an instance of the class {@link
     *                        RevealAnimation}. The reveal animation may not be null
     */
    private void animateReveal(final AbstractItem item,
                               final RevealAnimation revealAnimation) {
        tabViewBottomMargin = -1;
        tabRecyclerAdapter.clearCachedPreviews();
        dragHandler.setCallback(null);
        Component view = item.getView();
        AnimatorProperty animation = view.createAnimatorProperty();
        LogUtil.loge("=== animateReveal:" + animation.toString());
        animation.setCurveType(revealAnimation.getCurveType() != -1 ? revealAnimation.getCurveType() : Animator.CurveType.ACCELERATE_DECELERATE);
        //revert动画中在 createRevealLayoutListener 中已经调用，故不用在重复调用HideSwitcherAnimation监听 zhu_zhonglin update 2021/4/16
//        animation.setStateChangedListener(new AnimationListenerWrapper(createHideSwitcherAnimationListener()));
        animation.setDuration(revealAnimation.getDuration() != -1 ? revealAnimation.getDuration() :
                revealAnimationDuration);
        getArithmetics().animateScale(Arithmetics.Axis.DRAGGING_AXIS, animation, 1);
        getArithmetics().animateScale(Arithmetics.Axis.ORTHOGONAL_AXIS, animation, 1);
        animation.start();
        animateToolbarVisibility(getModel().areToolbarsShown() && getModel().isEmpty(), 0);
    }

    /**
     * Starts a peek animation to add a specific tab.
     *
     * @param item          The item, which corresponds to the tab, which should be added, as an instance of the
     *                      class {@link AbstractItem}. The item may not be null
     * @param duration      The duration of the animation in milliseconds as a {@link Long} value
     * @param curveType     The curveType, which should be used by the animation, as an instance of the type
     *                      {@link ohos.agp.animation.Animator.CurveType}. The curveType may not be null
     * @param peekPosition  The position on the dragging axis, the tab should be moved to, in pixels as a {@link
     *                      Float} value
     * @param peekAnimation The peek animation, which has been used to add the tab, as an instance of the class
     *                      {@link PeekAnimation}. The peek animation may not be null
     */
    private void animatePeek(final AbstractItem item, final long duration,
                             final int curveType, final float peekPosition,
                             final PeekAnimation peekAnimation) {
        LogUtil.loge("=== animatePeek");
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) ((TabItem) item).getViewHolder();
        viewHolder.closeButton.setVisibility(Component.HIDE);
        Component view = item.getView();
        float x = peekAnimation.getX();
        float y = (float) ((double)peekAnimation.getY() + tabTitleContainerHeight);
        StackLayout.LayoutConfig layoutParams = (StackLayout.LayoutConfig) view.getLayoutConfig();
        view.setAlpha(1f);
        getArithmetics().setPivot(Arithmetics.Axis.X_AXIS, item, x);
        getArithmetics().setPivot(Arithmetics.Axis.Y_AXIS, item, y);
        view.setTranslationX(layoutParams.getMarginLeft());
        view.setTranslationY(layoutParams.getMarginTop());
        getArithmetics().setScale(Arithmetics.Axis.DRAGGING_AXIS, item, 0);
        getArithmetics().setScale(Arithmetics.Axis.ORTHOGONAL_AXIS, item, 0);
        AnimatorProperty animation = view.createAnimatorProperty();
        LogUtil.loge("=== animatePeek" + animation.toString());
        animation.setCurveType(curveType);
        animation.setStateChangedListener(
                new AnimationListenerWrapper(createPeekAnimationListener(item, peekAnimation)));
        animation.setDelay(0);
        animation.setDuration(duration);
        getArithmetics().animateScale(Arithmetics.Axis.DRAGGING_AXIS, animation, 1);
        getArithmetics().animateScale(Arithmetics.Axis.ORTHOGONAL_AXIS, animation, 1);
        getArithmetics().animatePosition(Arithmetics.Axis.DRAGGING_AXIS, animation, item, peekPosition, true);
        animation.start();
        int selectedTabIndex = getModel().getSelectedTabIndex();
        TabItem selectedItem = TabItem.create(getModel(), tabViewRecycler, selectedTabIndex);
        tabViewRecycler.inflate(selectedItem);
        selectedItem.getTag().setPosition(0);
        PhoneTabViewHolder selectedTabViewHolder =
                (PhoneTabViewHolder) selectedItem.getViewHolder();
        selectedTabViewHolder.closeButton.setVisibility(Component.HIDE);
        animateShowSwitcher(selectedItem, duration, curveType,
                createZoomOutAnimationListener(selectedItem, peekAnimation));
    }

    /**
     * Creates and returns a layout listener, which allows to animate the position and size of a tab
     * in order to show the tab switcher, once its view has been inflated.
     *
     * @param item The item, which corresponds to the tab, whose view should be animated, as an instance
     *             of the class {@link AbstractItem}. The item may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * ComponentTreeObserver.WindowBoundListener}. The listener may not be null
     */

    private ComponentTreeObserver.WindowBoundListener createShowSwitcherLayoutListener(
            final AbstractItem item) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                animateShowSwitcher(item, createUpdateViewAnimationListener(item));
            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns a layout listener, which allows to animate the position and size of a tab
     * in order to hide the tab switcher, once its view has been inflated.
     *
     * @param item The item, which corresponds to the tab, whose view should be animated, as an instance
     *             of the class {@link AbstractItem}. The item may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * ComponentTreeObserver.WindowBoundListener}. The listener may not be null
     */

    private ComponentTreeObserver.WindowBoundListener createHideSwitcherLayoutListener(
            final AbstractItem item) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                animateHideSwitcher(item, item.getIndex() == getModel().getSelectedTabIndex() ?
                        createHideSwitcherAnimationListener() : null);
            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns a layout listener, which allows to remove a tab, once its view has been
     * inflated.
     *
     * @param item           The item, which corresponds to the tab, which should be removed, as an instance of
     *                       the class {@link AbstractItem}. The item may not be null
     * @param swipeAnimation The animation, which should be used, as an instance of the class {@link
     *                       SwipeAnimation}. The animation may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * ComponentTreeObserver.WindowBoundListener}. The listener may not be null
     */

    private ComponentTreeObserver.WindowBoundListener createRemoveLayoutListener(final AbstractItem item,
                                                                                 final SwipeAnimation swipeAnimation) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                animateRemove(item, swipeAnimation);
            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns a layout listener, which allows to relocate a tab, once its view has been
     * inflated.
     *
     * @param item            The item, which corresponds to the tab, which should be relocated, as an instance of
     *                        the class {@link AbstractItem}. The item may not be null
     * @param position        The position, the tab should be relocated to, in pixels as a {@link Float} value
     * @param tag             The tag, which should be applied to the given item, as an instance of the class
     *                        {@link Tag} or null, if no tag should be applied
     * @param delayMultiplier The multiplier, which should be used to calculate the delay of the relocate
     *                        animation, by being multiplied with the default delay, as an {@link Integer} value
     * @param listener        The listener, which should be notified about the progress of the relocate animation,
     *                        as an instance of the type {@link Animator.StateChangedListener} or null, if no listener should be
     *                        notified
     * @param swipeAnimation  The animation, which has been used to add or remove the tab, which caused the other
     *                        tabs to be relocated, as an instance of the class {@link SwipeAnimation}. The
     *                        animation may not be null
     * @return The listener, which has been created, as an instance of the class {@link
     * ComponentTreeObserver.WindowBoundListener}. The listener may not be null
     */

    private ComponentTreeObserver.WindowBoundListener createRelocateLayoutListener(final AbstractItem item,
                                                                                   final float position,
                                                                                   final Tag tag,
                                                                                   final int delayMultiplier,
                                                                                   final Animator.StateChangedListener listener,
                                                                                   final SwipeAnimation swipeAnimation) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                animateRelocate(item, position, tag, delayMultiplier, listener, swipeAnimation);
            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns a layout listener, which allows to show a tab as the currently selected
     * one, once it view has been inflated.
     *
     * @param item The item, which corresponds to the tab, which has been added, as an instance of the
     *             class {@link AbstractItem}. The item may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * ComponentTreeObserver.WindowBoundListener}. The listener may not be null
     */

    private ComponentTreeObserver.WindowBoundListener createAddSelectedTabLayoutListener(
            final AbstractItem item) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                Component view = item.getView();
                view.setAlpha(1f);
                getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item,
                        getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
                getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                        getArithmetics().getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
//                view.setTranslationX(layoutParams.getMarginLeft());
//                view.setTranslationY(layoutParams.getMarginTop());
                getArithmetics().setScale(Arithmetics.Axis.DRAGGING_AXIS, item, 1);
                getArithmetics().setScale(Arithmetics.Axis.ORTHOGONAL_AXIS, item, 1);

            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns a layout listener, which allows to start a reveal animation to add a tab,
     * once its view has been inflated.
     *
     * @param item            The item, which corresponds to the tab, which should be added, as an instance of the
     *                        class {@link AbstractItem}. The item may not be null
     * @param revealAnimation The reveal animation, which should be started, as an instance of the class {@link
     *                        RevealAnimation}. The reveal animation may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * ComponentTreeObserver.WindowBoundListener}. The listener may not be null
     */

    private ComponentTreeObserver.WindowBoundListener createRevealLayoutListener(final AbstractItem item,
                                                                                 final RevealAnimation revealAnimation) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                Component view = item.getView();
                float x = revealAnimation.getX();
                float y = (float) ((double)revealAnimation.getY() + tabTitleContainerHeight);
                view.setAlpha(1f);
                getArithmetics().setPivot(Arithmetics.Axis.X_AXIS, item, x);
                getArithmetics().setPivot(Arithmetics.Axis.Y_AXIS, item, y);
                //revert动画中监听回调时，位置不对的问题   zhu_zhonglin update 2021/4/16
//                view.setTranslationX(layoutParams.getMarginLeft());
//                view.setTranslationY(layoutParams.getMarginTop());
                view.setTranslationX(0);
                view.setTranslationY(0);
                getArithmetics().setScale(Arithmetics.Axis.DRAGGING_AXIS, item, 0);
                getArithmetics().setScale(Arithmetics.Axis.ORTHOGONAL_AXIS, item, 0);
                animateReveal(item, revealAnimation);

            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns a layout listener, which allows to start a peek animation to add a tab,
     * once its view has been inflated.
     *
     * @param item          The item, which corresponds to the tab, which should be added, as an instance of the
     *                      class {@link AbstractItem}. The item may not be null
     * @param peekAnimation The peek animation, which should be started, as an instance of the class {@link
     *                      PeekAnimation}. The peek animation may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * ComponentTreeObserver.WindowBoundListener}. The listener may not be null
     */
    private ComponentTreeObserver.WindowBoundListener createPeekLayoutListener(final AbstractItem item,
                                                                               final PeekAnimation peekAnimation) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                long totalDuration = peekAnimation.getDuration() != -1 ? peekAnimation.getDuration() : peekAnimationDuration;
                long duration = totalDuration / 3;
                int curveType = peekAnimation.getCurveType() != -1 ? peekAnimation.getCurveType() : Animator.CurveType.ACCELERATE_DECELERATE;
                float peekPosition = getArithmetics().getTabContainerSize(Arithmetics.Axis.DRAGGING_AXIS, false) * 0.66f;
                animatePeek(item, duration, curveType, peekPosition, peekAnimation);
            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    private ComponentTreeObserver.WindowBoundListener createSwipeLayoutListener(
            final AbstractItem[] addedItems,
            final SwipeAnimation swipeAnimation) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                int count = getModel().getCount();
                float previousAttachedPosition =
                        calculateAttachedPosition(count - addedItems.length);
                float attachedPosition = calculateAttachedPosition(count);
                AbstractItem[] items;

                if (count - addedItems.length == 0) {
                    items = calculateInitialItems(-1, -1);
                } else {
                    AbstractItem firstAddedItem = addedItems[0];
                    int index = firstAddedItem.getIndex();
                    boolean isReferencingPredecessor = index > 0;
                    int referenceIndex = -1;
                    if (isReferencingPredecessor) {
                        referenceIndex = index - 1;
                    } else if (index + addedItems.length - 1 < count - 1) {
                        referenceIndex = index + addedItems.length;
                    }
                    AbstractItem referenceItem = referenceIndex != -1 ?
                            TabItem.create(getTabSwitcher(), tabViewRecycler, referenceIndex) :
                            null;
                    State state = referenceItem != null ? referenceItem.getTag().getState() : null;

                    if (state == null || state == State.STACKED_START) {
                        items = relocateWhenAddingStackedTabs(true, addedItems, swipeAnimation);
                    } else if (state == State.STACKED_END) {
                        items = relocateWhenAddingStackedTabs(false, addedItems, swipeAnimation);
                    } else if (state == State.FLOATING ||
                            (state == State.STACKED_START_ATOP && (index > 0 || count <= 2))) {
                        items = relocateWhenAddingFloatingTabs(addedItems, referenceItem,
                                isReferencingPredecessor, attachedPosition,
                                attachedPosition != previousAttachedPosition, swipeAnimation);
                    } else {
                        items = relocateWhenAddingHiddenTabs(addedItems, referenceItem);
                    }
                }

                Tag previousTag = null;

                for (AbstractItem item : items) {
                    Tag tag = item.getTag();

                    if (previousTag == null || tag.getPosition() != previousTag.getPosition()) {
                        createBottomMarginLayoutListener(item).onWindowBound();
                        Component view = item.getView();
                        //只能设置一个Tag,故在title中设置
                        Component component = view.findComponentById(ResourceTable.Id_tab_title_container);
                        component.setTag(tag);
                        view.setAlpha(swipedTabAlpha);
                        float swipePosition = calculateSwipePosition();
                        float scale = getArithmetics().getScale(item, true);
                        getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item, getArithmetics()
                                .getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
                        getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, getArithmetics()
                                .getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
                        getArithmetics().setPosition(Arithmetics.Axis.DRAGGING_AXIS, item, tag.getPosition());
                        getArithmetics().setPosition(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                                swipeAnimation.getDirection() == SwipeAnimation.SwipeDirection.LEFT_OR_TOP ?
                                        -1 * swipePosition : swipePosition);
                        getArithmetics().setScale(Arithmetics.Axis.DRAGGING_AXIS, item, scale);
                        getArithmetics().setScale(Arithmetics.Axis.ORTHOGONAL_AXIS, item, scale);
                        getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item, getArithmetics()
                                .getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.SWIPE));
                        getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, getArithmetics()
                                .getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.SWIPE));
                        getArithmetics().setScale(Arithmetics.Axis.DRAGGING_AXIS, item, swipedTabScale * scale);
                        getArithmetics()
                                .setScale(Arithmetics.Axis.ORTHOGONAL_AXIS, item, swipedTabScale * scale);
                        animateSwipe(item, false, 0, swipeAnimation,
                                createSwipeAnimationListener(item));
                    } else {
                        tabViewRecycler.remove(item);
                    }

                    previousTag = tag;
                }

            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns a layout listener, which allows to position a tab, which is a neighbor of
     * the currently selected tab, when swiping horizontally.
     *
     * @param neighbor     The tab item, which corresponds to the neighboring tab, as an instance of the class
     *                     {@link TabItem}. The tab item may not be null
     * @param dragDistance The distance of the swipe gesture in pixels as a {@link Float} value
     * @return The layout listener, which has been created, as an instance of the type {@link
     * ComponentTreeObserver.WindowBoundListener}. The layout listener may not be null
     */

    private ComponentTreeObserver.WindowBoundListener createSwipeNeighborLayoutListener(
            final TabItem neighbor, final float dragDistance) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                float position;

                if (dragDistance > 0) {
                    position = (float) (-(double)getTabSwitcher().getWidth() + dragDistance - swipedTabDistance);
                } else {
                    position = (float) ((double)getTabSwitcher().getWidth() + dragDistance + swipedTabDistance);
                }

                getArithmetics().setPosition(Arithmetics.Axis.X_AXIS, neighbor, position);
            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns a layout listener, which allows to adapt the bottom margin of a tab, once
     * its view has been inflated.
     *
     * @param item The item, which corresponds to the tab, whose view should be adapted, as an instance
     *             of the class {@link AbstractItem}. The item may not be null
     * @return The layout listener, which has been created, as an instance of the type {@link
     * ComponentTreeObserver.WindowBoundListener}. The layout listener may not be null
     */
    private ComponentTreeObserver.WindowBoundListener createBottomMarginLayoutListener(
            final AbstractItem item) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                Component view = item.getView();

                if (tabViewBottomMargin == -1) {
                    tabViewBottomMargin = calculateBottomMargin(item);
                }

                StackLayout.LayoutConfig layoutParams =
                        (StackLayout.LayoutConfig) view.getLayoutConfig();
                layoutParams.setMarginBottom(tabViewBottomMargin);
                view.setLayoutConfig(layoutParams);
            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns a layout listener, which allows to adapt the size and position of a tab,
     * once its view has been inflated.
     *
     * @param item           The item, which corresponds to the tab, whose view should be adapted, as an instance
     *                       of the class {@link AbstractItem}. The item may not be null
     * @param dragging       True, if the item is currently being dragged, false otherwise
     * @param layoutListener The layout lister, which should be notified, when the created listener is invoked, as
     *                       an instance of the type {@link ComponentTreeObserver.WindowBoundListener} or null, if no listener should
     *                       be notified
     * @return The layout listener, which has been created, as an instance of the type {@link
     * ComponentTreeObserver.WindowBoundListener}. The layout listener may not be null
     */

    private ComponentTreeObserver.WindowBoundListener createInflateViewLayoutListener(final AbstractItem item,
                                                                                      final boolean dragging,
                                                                                      final ComponentTreeObserver.WindowBoundListener layoutListener) {
        return new ComponentTreeObserver.WindowBoundListener() {

            @Override
            public void onWindowBound() {
                adaptViewSize(item);
                updateView(item, dragging);

                if (layoutListener != null) {
                    layoutListener.onWindowBound();
                }
            }

            @Override
            public void onWindowUnbound() {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to adapt the visibility of the
     * toolbar, when an animation, which is used to animate the alpha of the toolbar, has been
     * started or ended, depending on whether the toolbar should be shown, or hidden.
     *
     * @param show True, if the toolbar should be shown by the animation, false otherwise
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createToolbarAnimationListener(final boolean show) {
        return new Animator.StateChangedListener() {

            @Override
            public void onEnd(final Animator animation) {
                if (!show) {
                    toolbar.setVisibility(Component.INVISIBLE);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

            @Override
            public void onStart(final Animator animation) {
                if (show) {
                    toolbar.setVisibility(Component.VISIBLE);
                }
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to update the view, which is used to
     * visualize a specific tab, when an animation has been finished.
     *
     * @param item The item, which corresponds to the tab, whose view should be updated, as an instance
     *             of the class {@link AbstractItem}. The item may not be null
     * @return The animation listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createUpdateViewAnimationListener(final AbstractItem item) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(final Animator animation) {
                inflateOrRemoveView(item, false);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to inflate or remove the views, which
     * are used to visualize tabs, when an animation, which is used to hide the tab switcher, has
     * been finished.
     *
     * @return The animation listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createHideSwitcherAnimationListener() {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                LogUtil.loge("=== createHideSwitcherAnimationListener");
                AbstractItemIterator iterator =
                        new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).create();
                AbstractItem item;

                while ((item = iterator.next()) != null) {
                    if (((TabItem) item).getTab() == getModel().getSelectedTab()) {
                        Pair<Component, Boolean> pair = tabViewRecycler.inflate(item);
                        Component view = pair.f;
                        StackLayout.LayoutConfig layoutParams =
                                (StackLayout.LayoutConfig) view.getLayoutConfig();
                        view.setAlpha(1f);
                        getArithmetics().setScale(Arithmetics.Axis.DRAGGING_AXIS, item, 1);
                        getArithmetics().setScale(Arithmetics.Axis.ORTHOGONAL_AXIS, item, 1);
                        view.setTranslationX(layoutParams.getMarginLeft());
                        view.setTranslationY(layoutParams.getMarginTop());
                    } else {
                        tabViewRecycler.remove(item);
                    }
                }

                tabViewRecycler.clearCache();
                tabRecyclerAdapter.clearCachedPreviews();
                tabViewBottomMargin = -1;
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to remove all tabs, when the
     * animation, which is used to swipe all tabs, has been finished.
     *
     * @return The animation listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createClearAnimationListener() {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(final Animator animation) {
                tabViewRecycler.removeAll();
                setFirstVisibleIndex(-1);
                animateToolbarVisibility(getModel().areToolbarsShown(), 0);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns a listener, which allows to handle, when a tab has been swiped, but was
     * not removed.
     *
     * @param item The item, which corresponds to the tab, which has been swiped, as an instance of the
     *             class {@link AbstractItem}. The item may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createSwipeAnimationListener(final AbstractItem item) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                inflateOrRemoveView(item, false);
                adaptStackOnSwipeAborted(item, item.getIndex() + 1);
                item.getTag().setClosing(false);
                getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item,
                        getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
                animateToolbarVisibility(true, 0);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to adapt the currently selected tab,
     * when swiping the tabs horizontally has been ended.
     *
     * @param tabItem The tab item, which corresponds to the tab, which should be selected, as an instance
     *                of the class {@link TabItem}. The tab item may not be null
     * @return The animation listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createSwipeSelectedTabAnimationListener(
            final TabItem tabItem) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                Tab tab = tabItem.getTab();

                if (getModel().getSelectedTab() != tab) {
                    getModel().selectTab(tab);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to remove the view, which is used to
     * visualize a specific tab, when swiping the tabs horizontally has been ended.
     *
     * @param tabItem The tab item, which corresponds to the tab, whose view should be removed, as an
     *                instance of the class {@link TabItem}. The tab item may not be null
     * @return The animation listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createSwipeNeighborAnimationListener(final TabItem tabItem) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                getTabViewRecycler().remove(tabItem);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns a listener, which allows to relocate all previous tabs, when a tab has
     * been removed.
     *
     * @param removedItem    The item, which corresponds to the tab, which has been removed, as an instance of the
     *                       class {@link AbstractItem}. The item may not be null
     * @param swipeAnimation The animation, which has been used to remove the tab, as an instance of the class
     *                       {@link SwipeAnimation}. The animation may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createRemoveAnimationListener(final AbstractItem removedItem,
                                                                        final SwipeAnimation swipeAnimation) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {
                if (getModel().isEmpty()) {
                    setFirstVisibleIndex(-1);
                    animateToolbarVisibility(getModel().areToolbarsShown(), 0);
                }

                float previousAttachedPosition =
                        calculateAttachedPosition(getModel().getCount() + 1);
                float attachedPosition = calculateAttachedPosition(getModel().getCount());
                State state = removedItem.getTag().getState();

                if (state == State.STACKED_END) {
                    relocateWhenRemovingStackedTab(removedItem, false, swipeAnimation);
                } else if (state == State.STACKED_START) {
                    relocateWhenRemovingStackedTab(removedItem, true, swipeAnimation);
                } else if (state == State.FLOATING || state == State.STACKED_START_ATOP) {
                    relocateWhenRemovingFloatingTab(removedItem, attachedPosition,
                            previousAttachedPosition != attachedPosition, swipeAnimation);
                }
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                tabViewRecycler.remove(removedItem);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to update or remove the view, which
     * is used to visualize a tab, when the animation, which has been used to relocate it, has been
     * ended.
     *
     * @param item The item, which corresponds to the tab, which has been relocated, as an instance of
     *             the class {@link AbstractItem}. The item may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createRelocateAnimationListener(final AbstractItem item) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {
                item.getView().setVisibility(Component.VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (item.getTag().getState() == State.STACKED_START_ATOP) {
                    adaptStackOnSwipeAborted(item, item.getIndex() + 1);
                }

                if (item.isVisible()) {
                    updateView(item, false);
                } else {
                    tabViewRecycler.remove(item);
                }

                ItemIterator iterator =
                        new ItemIterator.Builder(getModel(), getTabViewRecycler()).create();
                AbstractItem item;

                while ((item = iterator.next()) != null) {
                    if (item.getTag().getState() == State.FLOATING) {
                        setFirstVisibleIndex(item.getIndex());
                        break;
                    }
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to adapt the pivot of a specific tab,
     * when an animation, which reverted an overshoot, has been ended.
     *
     * @param item     The item, which corresponds to the tab, whose pivot should be adapted, as an instance
     *                 of the class {@link AbstractItem}. The item may not be null
     * @param listener The listener, which should be notified about the animation's progress, as an instance
     *                 of the type {@link Animator.StateChangedListener} or null, if no listener should be notified
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createRevertOvershootAnimationListener(
            final AbstractItem item, final Animator.StateChangedListener listener) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item,
                        getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
                getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                        getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));

                if (listener != null) {
                    listener.onEnd(animator);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to revert an overshoot at the start,
     * when an animation has been ended.
     *
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createRevertStartOvershootAnimationListener() {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                animateRevertStartOvershoot(Animator.CurveType.DECELERATE);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to hide a tab, which has been added
     * by using a peek animation, when the animation has been ended.
     *
     * @param item          The item, which corresponds to the tab, which has been added by using the peek
     *                      animation, as an instance of the class {@link AbstractItem}. The item may not be
     *                      null
     * @param peekAnimation The peek animation as an instance of the class {@link PeekAnimation}. The peek
     *                      animation may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createPeekAnimationListener(final AbstractItem item,
                                                                      final PeekAnimation peekAnimation) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator a) {
                long totalDuration =
                        peekAnimation.getDuration() != -1 ? peekAnimation.getDuration() :
                                peekAnimationDuration;
                long duration = totalDuration / 3;
                int curveType = peekAnimation.getCurveType() != -1 ? peekAnimation.getCurveType() : Animator.CurveType.ACCELERATE_DECELERATE;
                Component view = item.getView();
                getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item, tabTitleContainerHeight);
                getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                        getArithmetics().getSize(Arithmetics.Axis.ORTHOGONAL_AXIS, item) / 2f);
                AnimatorProperty animator = view.createAnimatorProperty();
                LogUtil.loge("=== createPeekAnimationListener" + animator.toString());
                animator.setDuration(duration);
                animator.setDelay(duration);
                animator.setCurveType(curveType);
                animator.setStateChangedListener(
                        new AnimationListenerWrapper(createRevertPeekAnimationListener(item)));
                animator.alpha(0);
                getArithmetics().animatePosition(Arithmetics.Axis.DRAGGING_AXIS, animator, item,
                        getArithmetics().getPosition(Arithmetics.Axis.DRAGGING_AXIS, item) * 1.5f);
                getArithmetics().animateScale(Arithmetics.Axis.DRAGGING_AXIS, animator, 0);
                getArithmetics().animateScale(Arithmetics.Axis.ORTHOGONAL_AXIS, animator, 0);
                animator.start();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to remove the view of a tab, which
     * has been added by using a peek animation, when the animation, which reverts the peek
     * animation, has been ended.
     *
     * @param item The item, which corresponds to the tab, which has been added by using the peek
     *             animation, as an instance of the class {@link AbstractItem}. The item may not be
     *             null
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createRevertPeekAnimationListener(final AbstractItem item) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                tabViewRecycler.remove(item);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to zoom in the currently selected
     * tab, when a peek animation has been ended.
     *
     * @param selectedItem  The item, which corresponds to the currently selected tab, as an instance of the
     *                      class {@link AbstractItem}. The item may not be null
     * @param peekAnimation The peek animation as an instance of the class {@link PeekAnimation}. The peek
     *                      animation may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */

    private Animator.StateChangedListener createZoomOutAnimationListener(
            final AbstractItem selectedItem, final PeekAnimation peekAnimation) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                getModel().removeListener(PhoneTabSwitcherLayout.this);
                getModel().hideSwitcher();
                long totalDuration =
                        peekAnimation.getDuration() != -1 ? peekAnimation.getDuration() :
                                peekAnimationDuration;
                long duration = totalDuration / 3;
                int curveType = peekAnimation.getCurveType() != -1 ? peekAnimation.getCurveType() : Animator.CurveType.ACCELERATE_DECELERATE;
                animateHideSwitcher(selectedItem, duration, curveType, duration,
                        createZoomInAnimationListener(selectedItem));

            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Creates and returns an animation listener, which allows to restore the original state of a
     * tab, when an animation, which zooms in the tab, has been ended.
     *
     * @param item The item, which corresponds to the tab, which has been zoomed in, as an instance of
     *             the class {@link AbstractItem}. The item may not be null
     * @return The listener, which has been created, as an instance of the type {@link
     * Animator.StateChangedListener}. The listener may not be null
     */
    private Animator.StateChangedListener createZoomInAnimationListener(final AbstractItem item) {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                LogUtil.loge("=== createZoomInAnimationListener");
                getModel().addListener(PhoneTabSwitcherLayout.this);
                tabViewRecycler.inflate(item);
                tabViewRecycler.clearCache();
                tabRecyclerAdapter.clearCachedPreviews();
                tabViewBottomMargin = -1;
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        };
    }

    /**
     * Adapts the stack, which is located at the start, when swiping a tab.
     *
     * @param swipedItem     The item, which corresponds to the swiped tab, as an instance of the class {@link
     *                       AbstractItem}. The item may not be null
     * @param successorIndex The index of the tab, which is located after the swiped tab, as an {@link Integer}
     *                       value
     * @param count          The number of tabs, which are contained by the tab switcher, excluding the swiped
     *                       tab, as an {@link Integer} value
     */
    private void adaptStackOnSwipe(final AbstractItem swipedItem, final int successorIndex,
                                   final int count) {
        if (swipedItem.getTag().getState() == State.STACKED_START_ATOP &&
                successorIndex < getModel().getCount()) {
            AbstractItem item = TabItem.create(getTabSwitcher(), tabViewRecycler, successorIndex);
            State state = item.getTag().getState();

            if (state == State.HIDDEN || state == State.STACKED_START) {
                Pair<Float, State> pair =
                        calculatePositionAndStateWhenStackedAtStart(count, swipedItem.getIndex(),
                                (State) null);
                item.getTag().setPosition(pair.f);
                item.getTag().setState(pair.s);
                inflateOrRemoveView(item, false);
            }
        }
    }

    /**
     * Adapts the stack, which located at the start, when swiping a tab has been aborted.
     *
     * @param swipedItem     The item, which corresponds to the swiped tab, as an instance of the class {@link
     *                       AbstractItem}. The item may not be null
     * @param successorIndex The index of the the tab, which is located after the swiped tab, as an {@link
     *                       Integer} value
     */
    private void adaptStackOnSwipeAborted(final AbstractItem swipedItem,
                                          final int successorIndex) {
        if (swipedItem.getTag().getState() == State.STACKED_START_ATOP &&
                successorIndex < getModel().getCount()) {
            AbstractItem item = TabItem.create(getTabSwitcher(), tabViewRecycler, successorIndex);

            if (item.getTag().getState() == State.STACKED_START_ATOP) {
                Pair<Float, State> pair =
                        calculatePositionAndStateWhenStackedAtStart(getTabSwitcher().getCount(),
                                item.getIndex(), swipedItem);
                item.getTag().setPosition(pair.f);
                item.getTag().setState(pair.s);
                inflateOrRemoveView(item, false);
            }
        }
    }

    /**
     * Adapts the size of the view, which is used to visualize a specific tab.
     *
     * @param item The item, which corresponds to the tab, whose view should be adapted, as an instance
     *             of the class {@link AbstractItem}. The item may not be null
     */
    protected void adaptViewSize(final AbstractItem item) {
        getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item,
                getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
        getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                getArithmetics().getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
        float scale = getArithmetics().getScale(item, true);
        getArithmetics().setScale(Arithmetics.Axis.DRAGGING_AXIS, item, scale);
        getArithmetics().setScale(Arithmetics.Axis.ORTHOGONAL_AXIS, item, scale);
    }

    /**
     * Relocates all previous tabs, when a floating tab has been removed from the tab switcher.
     *
     * @param removedItem             The item, which corresponds to the tab, which has been removed, as an instance of the
     *                                class {@link AbstractItem}. The item may not be null
     * @param attachedPosition        The attached position in pixels as a {@link Float} value
     * @param attachedPositionChanged True, if removing the tab caused the attached position to be changed, false
     *                                otherwise
     * @param swipeAnimation          The animation, which has been used to remove the tab, as an instance of the class
     *                                {@link SwipeAnimation}. The animation may not be null
     */
    private void relocateWhenRemovingFloatingTab(final AbstractItem removedItem,
                                                 final float attachedPosition,
                                                 final boolean attachedPositionChanged,
                                                 final SwipeAnimation swipeAnimation) {
        AbstractItemIterator iterator;
        AbstractItem item;
        float defaultTabSpacing = calculateMaxTabSpacing(null);
        float minTabSpacing = calculateMinTabSpacing();
        int referenceIndex = removedItem.getIndex();
        AbstractItem currentReferenceItem = removedItem;
        float referencePosition = removedItem.getTag().getPosition();

        if (attachedPositionChanged && getModel().getCount() > 0) {
            int neighboringIndex = removedItem.getIndex() > 0 ? referenceIndex - 1 : referenceIndex;
            referencePosition += Math.abs(
                    TabItem.create(getTabSwitcher(), tabViewRecycler, neighboringIndex).getTag()
                            .getPosition() - referencePosition) / 2f;
        }

        referencePosition =
                Math.min(calculateMaxEndPosition(removedItem.getIndex() - 1), referencePosition);
        float initialReferencePosition = referencePosition;

        if (removedItem.getIndex() > 0) {
            int selectedTabIndex = getModel().getSelectedTabIndex();
            AbstractItem selectedItem =
                    TabItem.create(getTabSwitcher(), tabViewRecycler, selectedTabIndex);
            float maxTabSpacing = calculateMaxTabSpacing(selectedItem);
            iterator = new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler)
                    .start(removedItem.getIndex() - 1).reverse(true).create();

            while ((item = iterator.next()) != null) {
                AbstractItem predecessor = iterator.peek();
                float currentTabSpacing = calculateMaxTabSpacing(currentReferenceItem);
                Pair<Float, State> pair;

                if (item.getIndex() == removedItem.getIndex() - 1) {
                    pair = clipPosition(item.getIndex(), referencePosition, predecessor);
                    currentReferenceItem = item;
                    referencePosition = pair.f;
                    referenceIndex = item.getIndex();
                } else if (referencePosition >= attachedPosition - currentTabSpacing) {
                    float position;

                    if (selectedTabIndex > item.getIndex() && selectedTabIndex <= referenceIndex) {
                        position = referencePosition + maxTabSpacing +
                                ((referenceIndex - item.getIndex() - 1) * defaultTabSpacing);
                    } else {
                        position = referencePosition +
                                ((referenceIndex - item.getIndex()) * defaultTabSpacing);
                    }

                    pair = clipPosition(item.getIndex(), position, predecessor);
                } else {
                    AbstractItem successor = iterator.previous();
                    float successorPosition = successor.getTag().getPosition();
                    float position = (attachedPosition * (successorPosition + minTabSpacing)) /
                            (minTabSpacing + attachedPosition - currentTabSpacing);
                    pair = clipPosition(item.getIndex(), position, predecessor);

                    if (pair.f >= attachedPosition - currentTabSpacing) {
                        currentReferenceItem = item;
                        referencePosition = pair.f;
                        referenceIndex = item.getIndex();
                    }
                }

                Tag tag = item.getTag().clone();
                tag.setPosition(pair.f);
                tag.setState(pair.s);

                if (tag.getState() != State.HIDDEN) {
                    int delayMultiplier = Math.abs(removedItem.getIndex() - item.getIndex());

                    if (!item.isInflated()) {
                        Pair<Float, State> pair2 =
                                calculatePositionAndStateWhenStackedAtEnd(item.getIndex());
                        item.getTag().setPosition(pair2.f);
                        item.getTag().setState(pair2.s);
                    }

                    relocate(item, tag.getPosition(), tag, delayMultiplier, swipeAnimation);
                } else {
                    break;
                }
            }
        }

        if (attachedPositionChanged && getModel().getCount() > 2 &&
                removedItem.getTag().getState() != State.STACKED_START_ATOP) {
            iterator = new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler)
                    .start(removedItem.getIndex()).create();
            float previousPosition = initialReferencePosition;
            Tag previousTag = removedItem.getTag();

            while ((item = iterator.next()) != null &&
                    item.getIndex() < getModel().getCount() - 1) {
                float position =
                        calculateSuccessorPosition(previousPosition, calculateMaxTabSpacing(item));
                Pair<Float, State> pair =
                        clipPosition(item.getIndex(), position, previousTag.getState());
                Tag tag = item.getTag().clone();
                tag.setPosition(pair.f);
                tag.setState(pair.s);
                int delayMultiplier = Math.abs(removedItem.getIndex() - item.getIndex()) + 1;

                if (!item.isInflated()) {
                    Pair<Float, State> pair2 =
                            calculatePositionAndStateWhenStackedAtStart(getModel().getCount(),
                                    item.getIndex(), iterator.previous());
                    item.getTag().setPosition(pair2.f);
                    item.getTag().setState(pair2.s);
                }

                relocate(item, tag.getPosition(), tag, delayMultiplier, swipeAnimation);
                previousPosition = pair.f;
                previousTag = tag;

                if (pair.s == State.HIDDEN || pair.s == State.STACKED_START) {
                    break;
                }
            }
        }
    }

    /**
     * Relocates all neighboring tabs, when a stacked tab has been removed from the tab switcher.
     *
     * @param removedItem    The item, which corresponds to the tab, which has been removed, as an instance of the
     *                       class {@link AbstractItem}. The item may not be null
     * @param start          True, if the removed tab was part of the stack, which is located at the start, false,
     *                       if it was part of the stack, which is located at the end
     * @param swipeAnimation The animation, which has been used to remove the tab, as an instance of the class
     *                       {@link SwipeAnimation}. The animation may not be nullD
     */
    private void relocateWhenRemovingStackedTab(final AbstractItem removedItem,
                                                final boolean start,
                                                final SwipeAnimation swipeAnimation) {
        LogUtil.loge("=== relocateWhenRemovingStackedTab");
        int startIndex = (int) ((double)removedItem.getIndex() + (start ? -1 : 0));
        ItemIterator iterator =
                new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).reverse(start)
                        .start(startIndex).create();
        AbstractItem item;
        float previousProjectedPosition = removedItem.getTag().getPosition();

        while ((item = iterator.next()) != null && (item.getTag().getState() == State.HIDDEN ||
                item.getTag().getState() == State.STACKED_START ||
                item.getTag().getState() == State.STACKED_START_ATOP ||
                item.getTag().getState() == State.STACKED_END)) {
            float projectedPosition = item.getTag().getPosition();

            if (item.getTag().getState() == State.HIDDEN) {
                AbstractItem previous = iterator.previous();
                item.getTag().setState(previous.getTag().getState());

                if (item.isVisible()) {
                    Pair<Float, State> pair = start ?
                            calculatePositionAndStateWhenStackedAtStart(getTabSwitcher().getCount(),
                                    item.getIndex(), item) :
                            calculatePositionAndStateWhenStackedAtEnd(item.getIndex());
                    item.getTag().setPosition(pair.f);
                    item.getTag().setState(pair.s);
                    inflateAndUpdateView(item, false, null);
                }

                break;
            } else {
                item.getTag().setPosition(previousProjectedPosition);
                int delayMultiplier = Math.abs(startIndex - item.getIndex()) + 1;
                animateRelocate(item, previousProjectedPosition, null, delayMultiplier,
                        createRelocateAnimationListener(item), swipeAnimation);
            }

            previousProjectedPosition = projectedPosition;
        }
    }

    /**
     * Relocates all previous tabs, when floating tabs have been added to the tab switcher.
     *
     * @param addedItems               An array, which contains the items, which correspond to the tabs, which have been
     *                                 added, as an array of the type {@link AbstractItem}. The array may not be null
     * @param referenceItem            The item, which corresponds to the tab, which is used as a reference, as an instance
     *                                 of the class {@link AbstractItem}. The item may not be null
     * @param isReferencingPredecessor True, if the tab, which is used as a reference, is the predecessor of the added tab,
     *                                 false if it is the successor
     * @param attachedPosition         The current attached position in pixels as a {@link Float} value
     * @param attachedPositionChanged  True, if adding the tab caused the attached position to be changed, false otherwise
     * @param swipeAnimation           The animation, which has been used to add the tabs, as an instance of the class
     *                                 {@link SwipeAnimation}. The animation may not be null
     * @return An array, which contains the items, which correspond to the tabs, which have been
     * added, as an array of the type {@link AbstractItem}. The array may not be null
     */

    private AbstractItem[] relocateWhenAddingFloatingTabs(final AbstractItem[] addedItems,
                                                          final AbstractItem referenceItem,
                                                          final boolean isReferencingPredecessor,
                                                          final float attachedPosition,
                                                          final boolean attachedPositionChanged,
                                                          final SwipeAnimation swipeAnimation) {
        AbstractItem firstAddedItem = addedItems[0];
        AbstractItem lastAddedItem = addedItems[(int) ((double)addedItems.length - 1)];
        float referencePosition = referenceItem.getTag().getPosition();

        if (isReferencingPredecessor && attachedPositionChanged &&
                lastAddedItem.getIndex() < (double)getModel().getCount() - 1) {
            int neighboringIndex = (int) ((double)lastAddedItem.getIndex() + 1);
            referencePosition -= Math.abs(referencePosition -
                    TabItem.create(getTabSwitcher(), tabViewRecycler, neighboringIndex).getTag()
                            .getPosition()) / 2d;
        }

        float initialReferencePosition = referencePosition;
        int selectedTabIndex = getModel().getSelectedTabIndex();
        AbstractItem selectedItem =
                TabItem.create(getTabSwitcher(), tabViewRecycler, selectedTabIndex);
        float defaultTabSpacing = calculateMaxTabSpacing(null);
        float maxTabSpacing = calculateMaxTabSpacing(selectedItem);
        float minTabSpacing = calculateMinTabSpacing();
        AbstractItem currentReferenceItem = referenceItem;
        int referenceIndex = referenceItem.getIndex();
        AbstractItemIterator.AbstractBuilder builder =
                new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler);

        for (AbstractItem addedItem : addedItems) {
            int iterationReferenceIndex = referenceIndex;
            float iterationReferencePosition = referencePosition;
            AbstractItem iterationReferenceItem = currentReferenceItem;
            AbstractItemIterator iterator =
                    builder.start(addedItem.getIndex()).reverse(true).create();
            AbstractItem item;

            while ((item = iterator.next()) != null) {
                AbstractItem predecessor = iterator.peek();
                Pair<Float, State> pair;
                float currentTabSpacing = calculateMaxTabSpacing(iterationReferenceItem);

                if (isReferencingPredecessor && item.getIndex() == addedItem.getIndex()) {
                    State predecessorState =
                            predecessor != null ? predecessor.getTag().getState() : null;
                    pair = clipPosition(item.getIndex(), iterationReferencePosition,
                            predecessorState == State.STACKED_START_ATOP ? State.FLOATING :
                                    predecessorState);
                    currentReferenceItem = iterationReferenceItem = item;
                    initialReferencePosition =
                            referencePosition = iterationReferencePosition = pair.f;
                    referenceIndex = iterationReferenceIndex = item.getIndex();
                } else if (iterationReferencePosition >= attachedPosition - currentTabSpacing) {
                    float position;

                    if (selectedTabIndex > item.getIndex() &&
                            selectedTabIndex <= iterationReferenceIndex) {
                        position = (float) ((double)iterationReferencePosition + maxTabSpacing +
                                                        (((double)iterationReferenceIndex - item.getIndex() - 1) *
                                                                defaultTabSpacing));
                    } else {
                        position = (float) ((double)iterationReferencePosition +
                                                        (((double)iterationReferenceIndex - item.getIndex()) * defaultTabSpacing));
                    }

                    pair = clipPosition(item.getIndex(), position, predecessor);
                } else {
                    AbstractItem successor = iterator.previous();
                    float successorPosition = successor.getTag().getPosition();
                    float position = (float) (((double)attachedPosition * ((double)successorPosition + minTabSpacing)) /
                                                ((double)minTabSpacing + attachedPosition - currentTabSpacing));
                    pair = clipPosition(item.getIndex(), position, predecessor);

                    if ((double)pair.f >= (double)attachedPosition - (double)currentTabSpacing) {
                        iterationReferenceItem = item;
                        iterationReferencePosition = pair.f;
                        iterationReferenceIndex = item.getIndex();
                    }
                }

                if (item.getIndex() >= firstAddedItem.getIndex() &&
                        item.getIndex() <= lastAddedItem.getIndex()) {
                    if (!isReferencingPredecessor && attachedPositionChanged &&
                            getModel().getCount() > 3) {
                        AbstractItem successor = iterator.previous();
                        float successorPosition = successor.getTag().getPosition();
                        float position = (float) ((double)pair.f - Math.abs((double)pair.f - successorPosition) / 2f);
                        pair = clipPosition(item.getIndex(), position, predecessor);
                        initialReferencePosition = pair.f;
                    }

                    Tag tag = addedItems[(int) ((double)item.getIndex() - firstAddedItem.getIndex())].getTag();
                    tag.setPosition(pair.f);
                    tag.setState(pair.s);
                } else {
                    Tag tag = item.getTag().clone();
                    tag.setPosition(pair.f);
                    tag.setState(pair.s);

                    if (!item.isInflated()) {
                        Pair<Float, State> pair2 =
                                calculatePositionAndStateWhenStackedAtEnd(item.getIndex());
                        item.getTag().setPosition(pair2.f);
                        item.getTag().setState(pair2.s);
                    }

                    relocate(item, tag.getPosition(), tag, 0, swipeAnimation);
                }

                if (pair.s == State.HIDDEN || pair.s == State.STACKED_END) {
                    setFirstVisibleIndex((int) ((double)getFirstVisibleIndex() + 1));
                    break;
                }
            }
        }

        if (attachedPositionChanged && getModel().getCount() > 3) {
            AbstractItemIterator iterator =
                    builder.start(lastAddedItem.getIndex() + 1).reverse(false).create();
            AbstractItem item;
            float previousPosition = initialReferencePosition;
            Tag previousTag = lastAddedItem.getTag();

            while ((item = iterator.next()) != null &&
                    item.getIndex() < getModel().getCount() - 1) {
                float position =
                        calculateSuccessorPosition(previousPosition, calculateMaxTabSpacing(item));
                Pair<Float, State> pair =
                        clipPosition(item.getIndex(), position, previousTag.getState());
                Tag tag = item.getTag().clone();
                tag.setPosition(pair.f);
                tag.setState(pair.s);

                if (!item.isInflated()) {
                    Pair<Float, State> pair2 =
                            calculatePositionAndStateWhenStackedAtStart(getModel().getCount(),
                                    item.getIndex(), iterator.previous());
                    item.getTag().setPosition(pair2.f);
                    item.getTag().setState(pair2.s);
                }

                relocate(item, tag.getPosition(), tag, 0, swipeAnimation);
                previousPosition = pair.f;
                previousTag = tag;

                if (pair.s == State.HIDDEN || pair.s == State.STACKED_START) {
                    break;
                }
            }
        }

        return addedItems;
    }

    /**
     * Relocates all neighboring tabs, when stacked tabs have been added to the tab switcher.
     *
     * @param start          True, if the added tab was part of the stack, which is located at the start, false,
     *                       if it was part of the stack, which is located at the end
     * @param addedItems     An array, which contains the items, which correspond to the tabs, which have been
     *                       added, as an array of the type {@link AbstractItem}. The array may not be null
     * @param swipeAnimation The animation, which has been used to add the tabs, as an instance of the class
     *                       {@link SwipeAnimation}. The animation may not be null
     * @return An array, which contains the items, which correspond to the tabs, which have been
     * added, as an array of the type {@link AbstractItem}. The array may not be null
     */

    private AbstractItem[] relocateWhenAddingStackedTabs(final boolean start,
                                                         final AbstractItem[] addedItems,
                                                         final SwipeAnimation swipeAnimation) {
        if (!start) {
            setFirstVisibleIndex(getFirstVisibleIndex() + addedItems.length);
        }

        AbstractItem firstAddedItem = addedItems[0];
        AbstractItem lastAddedItem = addedItems[addedItems.length - 1];
        AbstractItemIterator iterator = new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler)
                .start(start ? lastAddedItem.getIndex() : firstAddedItem.getIndex()).reverse(start)
                .create();
        AbstractItem item;

        while ((item = iterator.next()) != null &&
                (item.getTag().getState() == State.STACKED_START ||
                        item.getTag().getState() == State.STACKED_START_ATOP ||
                        item.getTag().getState() == State.STACKED_END ||
                        item.getTag().getState() == State.HIDDEN)) {
            AbstractItem predecessor = start ? iterator.peek() : iterator.previous();
            Pair<Float, State> pair = start ?
                    calculatePositionAndStateWhenStackedAtStart(getModel().getCount(),
                            item.getIndex(), predecessor) :
                    calculatePositionAndStateWhenStackedAtEnd(item.getIndex());

            if (start && predecessor != null && predecessor.getTag().getState() == State.FLOATING) {
                float predecessorPosition = predecessor.getTag().getPosition();
                float distance = (float) ((double)predecessorPosition - pair.f);

                if (distance > calculateMinTabSpacing()) {
                    float position = calculateSuccessorPosition(item, predecessor);
                    pair = clipPosition(item.getIndex(), position, predecessor);
                }
            }

            if (item.getIndex() >= firstAddedItem.getIndex() &&
                    item.getIndex() <= lastAddedItem.getIndex()) {
                Tag tag = addedItems[item.getIndex() - firstAddedItem.getIndex()].getTag();
                tag.setPosition(pair.f);
                tag.setState(pair.s);
            } else if (item.isInflated()) {
                Tag tag = item.getTag().clone();
                tag.setPosition(pair.f);
                tag.setState(pair.s);
                animateRelocate(item, tag.getPosition(), tag, 0,
                        createRelocateAnimationListener(item), swipeAnimation);
            } else {
                break;
            }
        }

        return addedItems;
    }

    /**
     * Calculates the position and state of hidden tabs, which have been added to the tab switcher.
     *
     * @param addedItems    An array, which contains the items, which correspond to the tabs, which have been
     *                      added, as an array of the type {@link AbstractItem}. The array may not be null
     * @param referenceItem The item, which corresponds to the tab, which is used as a reference, as an instance
     *                      of the class {@link AbstractItem}. The item may not be null
     * @return An array, which contains the items, which correspond to the tabs, which have been
     * added, as an array of the type {@link AbstractItem}. The array may not be null
     */

    private AbstractItem[] relocateWhenAddingHiddenTabs(final AbstractItem[] addedItems,
                                                        final AbstractItem referenceItem) {
        boolean stackedAtStart = isStackedAtStart(referenceItem.getIndex());

        for (AbstractItem item : addedItems) {
            Pair<Float, State> pair;

            if (stackedAtStart) {
                AbstractItem predecessor = item.getIndex() > 0 ?
                        TabItem.create(getTabSwitcher(), tabViewRecycler, item.getIndex() - 1) :
                        null;
                pair = calculatePositionAndStateWhenStackedAtStart(getModel().getCount(),
                        item.getIndex(), predecessor);
            } else {
                pair = calculatePositionAndStateWhenStackedAtEnd(item.getIndex());
            }

            Tag tag = item.getTag();
            tag.setPosition(pair.f);
            tag.setState(pair.s);
        }

        return addedItems;
    }

    /**
     * Relocates a specific tab. If its view is now yet inflated, it is inflated f.
     *
     * @param item             The item, which corresponds to the tab, which should be relocated, as an instance of
     *                         the class {@link AbstractItem}. The item may not be null
     * @param relocatePosition The position, the tab should be moved to, in pixels as an {@link Float} value
     * @param tag              The tag, which should be applied to the tab, once it has been relocated, as an
     *                         instance of the class {@link Tag} or null, if no tag should be applied
     * @param delayMultiplier  The multiplier, which should be used to calculate the delay of the relocate
     *                         animation, by being multiplied with the default delay, as an {@link Integer} value
     * @param swipeAnimation   The animation, which has been used to add or remove the tab, which caused the other
     *                         tabs to be relocated, as an instance of the class {@link SwipeAnimation}. The
     *                         animation may not be null
     */
    private void relocate(final AbstractItem item, final float relocatePosition,
                          final Tag tag, final int delayMultiplier,
                          final SwipeAnimation swipeAnimation) {
        LogUtil.loge("=== relocate");
        if (item.isInflated()) {
            animateRelocate(item, relocatePosition, tag, delayMultiplier,
                    createRelocateAnimationListener(item), swipeAnimation);
        } else {
            inflateAndUpdateView(item, false,
                    createRelocateLayoutListener(item, relocatePosition, tag, delayMultiplier,
                            createRelocateAnimationListener(item), swipeAnimation));
            item.getView().setVisibility(Component.INVISIBLE);
        }
    }

    /**
     * Swipes a specific tab.
     *
     * @param item     The item, which corresponds to the tab, which should be swiped, as an instance of the
     *                 class {@link AbstractItem}. The item may not be null
     * @param distance The distance, the tab should be swiped by, in pixels as a {@link Float} value
     */
    private void swipe(final AbstractItem item, final float distance) {
        Component view = item.getView();

        if (!item.getTag().isClosing()) {
            adaptStackOnSwipe(item, item.getIndex() + 1, getModel().getCount() - 1);
        }

        item.getTag().setClosing(true);
        float dragDistance = distance;

        if (!((TabItem) item).getTab().isCloseable()) {
            dragDistance = (float) Math.pow(Math.abs(distance), 0.75);
            dragDistance = distance < 0 ? dragDistance * -1 : dragDistance;
        }

        getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item,
                getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.SWIPE));
        getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                getArithmetics().getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.SWIPE));
        float scale = getArithmetics().getScale(item, true);
        float ratio = (float) (1 - ((double)Math.abs(dragDistance) / calculateSwipePosition()));
        float scaledClosedTabScale = swipedTabScale * scale;
        float targetScale = (float) ((double)scaledClosedTabScale + ratio * ((double)scale - scaledClosedTabScale));
        getArithmetics().setScale(Arithmetics.Axis.DRAGGING_AXIS, item, targetScale);
        getArithmetics().setScale(Arithmetics.Axis.ORTHOGONAL_AXIS, item, targetScale);
        view.setAlpha((float) ((double)swipedTabAlpha + ratio * (1 - (double)swipedTabAlpha)));
        getArithmetics().setPosition(Arithmetics.Axis.ORTHOGONAL_AXIS, item, dragDistance);
    }

    /**
     * Moves the first tab to overlap the other tabs, when overshooting at the start.
     *
     * @param position The position of the first tab in pixels as a {@link Float} value
     */
    private void startOvershoot(final float position) {
        ItemIterator iterator =
                new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.getIndex() == 0) {
                getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item,
                        getArithmetics().getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
                getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                        getArithmetics().getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.NONE));
                getArithmetics().setPosition(Arithmetics.Axis.DRAGGING_AXIS, item, position);
            } else if (item.isInflated()) {
                AbstractItem firstItem = iterator.first();
                Component view = item.getView();
                view.setVisibility(getArithmetics().getPosition(Arithmetics.Axis.DRAGGING_AXIS, firstItem) <=
                        getArithmetics().getPosition(Arithmetics.Axis.DRAGGING_AXIS, item) ? Component.INVISIBLE :
                        Component.VISIBLE);
            }
        }
    }

    /**
     * Tilts the tabs, when overshooting at the start.
     *
     * @param angle The angle, the tabs should be rotated by, in degrees as a {@link Float} value
     */
    private void tiltOnStartOvershoot(final float angle) {
        ItemIterator iterator =
                new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            Component view = item.getView();

            if (item.getIndex() == 0) {
                //setCameraDistance 为沿着Z轴旋转，视角转换，暂未找到替代
                view.setRotationSensitivity(maxCameraDistance);
//                view.setCameraDistance(maxCameraDistance);
                getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item, getArithmetics()
                        .getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.OVERSHOOT_START));
                getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, getArithmetics()
                        .getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.OVERSHOOT_START));
                getArithmetics().setRotation(Arithmetics.Axis.ORTHOGONAL_AXIS, item, angle);
            } else if (item.isInflated()) {
                item.getView().setVisibility(Component.INVISIBLE);
            }
        }
    }

    /**
     * Tilts the tabs, when overshooting at the end.
     *
     * @param angle The angle, the tabs should be rotated by, in degrees as a {@link Float} value
     */
    private void tiltOnEndOvershoot(final float angle) {
        float minCameraDistance = maxCameraDistance / 2f;
        int firstVisibleIndex = -1;
        ItemIterator iterator =
                new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).create();
        AbstractItem item;

        while ((item = iterator.next()) != null) {
            if (item.isInflated()) {
                Component view = item.getView();

                if (!iterator.hasNext()) {
                    //setCameraDistance 为沿着Z轴旋转，视角转换，暂未找到替代
                    view.setRotationSensitivity(maxCameraDistance);
//                    view.setCameraDistance(maxCameraDistance);
                } else if (firstVisibleIndex == -1) {
                    //setCameraDistance 为沿着Z轴旋转，视角转换，暂未找到替代
                    view.setRotationSensitivity(minCameraDistance);
//                    view.setCameraDistance(minCameraDistance);

                    if (item.getTag().getState() == State.FLOATING) {
                        firstVisibleIndex = item.getIndex();
                    }
                } else {
                    int diff = item.getIndex() - firstVisibleIndex;
                    float ratio =
                            (float) diff / (float) (getModel().getCount() - firstVisibleIndex);
                    //setCameraDistance 为沿着Z轴旋转，视角转换，暂未找到替代
                    view.setRotationSensitivity((float) ((double)minCameraDistance + ((double)maxCameraDistance - minCameraDistance) * ratio));
//                    view.setCameraDistance(minCameraDistance + (maxCameraDistance - minCameraDistance) * ratio);
                }

                getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item, getArithmetics()
                        .getPivot(Arithmetics.Axis.DRAGGING_AXIS, item, AbstractDragTabsEventHandler.DragState.OVERSHOOT_END));
                getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, getArithmetics()
                        .getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, AbstractDragTabsEventHandler.DragState.OVERSHOOT_END));
                getArithmetics().setRotation(Arithmetics.Axis.ORTHOGONAL_AXIS, item, angle);
            }
        }
    }

    /**
     * Creates a new layout, which implements the functionality of a {@link TabSwitcher} on
     * smartphones.
     *
     * @param tabSwitcher          The tab switcher, the layout belongs to, as an instance of the class {@link
     *                             TabSwitcher}. The tab switcher may not be null
     * @param model                The model of the tab switcher, the layout belongs to, as an instance of the class
     *                             {@link TabSwitcherModel}. The model may not be null
     * @param arithmetics          The arithmetics, which should be used by the layout, as an instance of the class
     *                             {@link PhoneArithmetics}. The arithmetics may not be null
     * @param style                The style, which allows to retrieve style attributes of the tab switcher, as an
     *                             instance of the class {@link TabSwitcherStyle}. The style may not be null
     * @param touchEventDispatcher The dispatcher, which is used to dispatch touch events to event handlers, as an
     *                             instance of the class {@link TouchEventDispatcher}. The dispatcher may not be null
     */
    public PhoneTabSwitcherLayout(final TabSwitcher tabSwitcher,
                                  final TabSwitcherModel model,
                                  final PhoneArithmetics arithmetics,
                                  final TabSwitcherStyle style,
                                  final TouchEventDispatcher touchEventDispatcher) {
        super(tabSwitcher, model, arithmetics, style, touchEventDispatcher);
        stackedTabCount = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_phone_stacked_tab_count);
        tabInset = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_tab_inset)));
        tabBorderWidth = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_tab_border_width)));
        tabTitleContainerHeight = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_tab_title_container_height)));
        maxCameraDistance = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_max_camera_distance)));
        swipedTabScale = 0.5f;
        swipedTabAlpha = 0;
        showSwitcherAnimationDuration = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_show_switcher_animation_duration);
        hideSwitcherAnimationDuration = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_hide_switcher_animation_duration);
        toolbarVisibilityAnimationDuration = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_toolbar_visibility_animation_duration);
        toolbarVisibilityAnimationDelay = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_toolbar_visibility_animation_delay);
        swipeAnimationDuration = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_swipe_animation_duration);
        relocateAnimationDuration = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_relocate_animation_duration);
        revertOvershootAnimationDuration = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_revert_overshoot_animation_duration);
        revealAnimationDuration = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_reveal_animation_duration);
        peekAnimationDuration = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_peek_animation_duration);
        emptyViewAnimationDuration = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_empty_view_animation_duration);
        maxStartOvershootAngle = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_max_start_overshoot_angle);
        maxEndOvershootAngle = convertIntegerValue(tabSwitcher.getContext(), ResourceTable.Integer_max_end_overshoot_angle);
        swipedTabDistance = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(tabSwitcher.getContext().getString(ResourceTable.String_swiped_tab_distance)));
        tabViewBottomMargin = -1;
        toolbarAnimation = null;
    }


    private int convertIntegerValue(Context context, int id) {
        int value = 0;
        try {
            value = context.getResourceManager().getElement(id).getInteger();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    @Override
    public final AbstractDragTabsEventHandler<?> getDragHandler() {
        return dragHandler;
    }

    @Override
    protected void onInflateLayout(LayoutScatter inflater, boolean tabsOnly) {
        if (tabsOnly) {
            toolbar = (Toolbar) getTabSwitcher().findComponentById(ResourceTable.Id_primary_toolbar);
            toolbar.addMenuClickListener();
            tabContainer = (ComponentContainer) getTabSwitcher().findComponentById(ResourceTable.Integer_tab_container);
        } else {
            LogUtil.loge("===" + (inflater != null));


//            Component container = inflater.parse(ResourceTable.Layout_phone_toolbar, getTabSwitcher(), true);
//            toolbar = (Toolbar) container.findComponentById(ResourceTable.Id_primary_toolbar);
//            LogUtil.loge("===" + (container instanceof StackLayout));

            //解析失败，parse解析只能出现一个对象，故直接new 一个新的对象
            toolbar = new Toolbar(getTabSwitcher().getContext());
            toolbar.setId(ResourceTable.Id_primary_toolbar);
            // 设置padding
            toolbar.setPadding(60, 0, 60, 0);
            //手动添加监听和修改数量
            TabSwitcher.setupWithToolbar(getTabSwitcher(), toolbar, createTabSwitcherButtonListener());

            //actionBarSize
            int actionBarSize = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(getContext().getString(ResourceTable.String_actionBarSize)));
            StackLayout.LayoutConfig toolbarConfig = new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, actionBarSize);
            getTabSwitcher().addComponent(toolbar, toolbarConfig);
            tabContainer = new StackLayout(getContext());
            tabContainer.setId(ResourceTable.Integer_tab_container);
            getTabSwitcher().addComponent(tabContainer, StackLayout.LayoutConfig.MATCH_PARENT,
                    StackLayout.LayoutConfig.MATCH_PARENT);
        }

        contentViewRecycler = new ViewRecycler<>(inflater, getContext());
        tabRecyclerAdapter = new PhoneTabRecyclerAdapter(getTabSwitcher(), getModel(), getStyle(),
                contentViewRecycler);
        getModel().addListener(tabRecyclerAdapter);
        tabViewRecycler = new AttachedViewRecycler<>(tabContainer, inflater,
                Collections.reverseOrder(new ItemComparator(getTabSwitcher())));
        tabViewRecycler.setAdapter(tabRecyclerAdapter);
        tabRecyclerAdapter.setViewRecycler(tabViewRecycler);
        dragHandler =
                new PhoneDragTabsEventHandler(getTabSwitcher(), getArithmetics(), tabViewRecycler);
        adaptDecorator();
        adaptToolbarMargin();
    }

    private Component.ClickedListener createTabSwitcherButtonListener() {
        return new Component.ClickedListener() {

            @Override
            public void onClick(final Component view) {
                getTabSwitcher().toggleSwitcherVisibility();
            }

        };
    }

    @Override
    protected final Pair<Integer, Float> onDetachLayout(final boolean tabsOnly) {
        Pair<Integer, Float> result = null;

        if (getTabSwitcher().isSwitcherShown() && getFirstVisibleIndex() != -1) {
            TabItem tabItem =
                    TabItem.create(getModel(), getTabViewRecycler(), getFirstVisibleIndex());
            Tag tag = tabItem.getTag();

            if (tag.getState() != State.HIDDEN) {
                float position = tag.getPosition();
                float draggingAxisSize =
                        getArithmetics().getTabContainerSize(Arithmetics.Axis.DRAGGING_AXIS, false);
                float orthogonalAxisSize =
                        getArithmetics().getTabContainerSize(Arithmetics.Axis.ORTHOGONAL_AXIS, false);
                result = Pair.create(getFirstVisibleIndex(),
                        position / Math.max(draggingAxisSize, orthogonalAxisSize));
            }
        }

        contentViewRecycler.removeAll();
        contentViewRecycler.clearCache();
        tabRecyclerAdapter.clearCachedPreviews();
        detachEmptyView();

        if (!tabsOnly) {
            getModel().removeListener(tabRecyclerAdapter);
        }

        return result;
    }

    @Override
    public final AbstractViewRecycler<Tab, Void> getContentViewRecycler() {
        return contentViewRecycler;
    }

    @Override
    protected final AttachedViewRecycler<AbstractItem, Integer> getTabViewRecycler() {
        return tabViewRecycler;
    }

    @Override
    protected final AbstractTabRecyclerAdapter getTabRecyclerAdapter() {
        return tabRecyclerAdapter;
    }

    @Override
    protected final void updateView(final AbstractItem item, final boolean dragging) {
        Component view = item.getView();
        view.setAlpha(1f);
        view.setVisibility(Component.VISIBLE);
        getArithmetics().setPivot(Arithmetics.Axis.DRAGGING_AXIS, item, getArithmetics()
                .getPivot(Arithmetics.Axis.DRAGGING_AXIS, item,
                        AbstractDragTabsEventHandler.DragState.NONE));
        getArithmetics().setPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item, getArithmetics()
                .getPivot(Arithmetics.Axis.ORTHOGONAL_AXIS, item,
                        AbstractDragTabsEventHandler.DragState.NONE));
        super.updateView(item, dragging);
        getArithmetics().setRotation(Arithmetics.Axis.ORTHOGONAL_AXIS, item, 0);
    }

    @Override
    protected final float calculateAttachedPosition(final int count) {
        float totalSpace = getArithmetics().getTabContainerSize(Arithmetics.Axis.DRAGGING_AXIS, false);
        float attachedPosition;

        if (count == 3) {
            attachedPosition = totalSpace * 0.66f;
        } else if (count == 4) {
            attachedPosition = totalSpace * 0.6f;
        } else {
            attachedPosition = totalSpace * 0.5f;
        }

        return attachedPosition;
    }

    @Override
    protected final void inflateAndUpdateView(final AbstractItem item,
                                              final boolean dragging,
                                              final ComponentTreeObserver.WindowBoundListener listener) {
        LogUtil.loge("=== inflateAndUpdateView");
        inflateView(item, createInflateViewLayoutListener(item, dragging, listener),
                tabViewBottomMargin);
    }

    @Override
    protected final int getStackedTabCount() {
        return stackedTabCount;
    }


    @Override
    protected final Pair<Float, State> calculatePositionAndStateWhenStackedAtStart(final int count,
                                                                                   final int index,
                                                                                   final State predecessorState) {
        if ((count - index) <= getStackedTabCount()) {
            float position = getStackedTabSpacing() * (count - (index + 1));
            return Pair.create(position,
                    (predecessorState == null || predecessorState == State.FLOATING) ?
                            State.STACKED_START_ATOP : State.STACKED_START);
        } else {
            float position = getStackedTabSpacing() * getStackedTabCount();
            return Pair.create(position,
                    (predecessorState == null || predecessorState == State.FLOATING) ?
                            State.STACKED_START_ATOP : State.HIDDEN);
        }
    }


    @Override
    protected final Pair<Float, State> calculatePositionAndStateWhenStackedAtEnd(final int index) {
        float size = getArithmetics().getTabContainerSize(Arithmetics.Axis.DRAGGING_AXIS, false);

        if (index < getStackedTabCount()) {
            float position = (float) ((double)size - tabInset - (getStackedTabSpacing() * ((double)index + 1)));
            return Pair.create(position, State.STACKED_END);
        } else {
            float position = (float) ((double)size - tabInset - ((double)getStackedTabSpacing() * getStackedTabCount()));
            return Pair.create(position, State.HIDDEN);
        }
    }

    @Override
    protected final boolean isOvershootingAtStart() {
        if (getTabSwitcher().getCount() <= 1) {
            return true;
        } else {
            AbstractItemIterator iterator =
                    new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).create();
            AbstractItem item = iterator.getItem(0);
            return item.getTag().getState() == State.STACKED_START_ATOP;
        }
    }

    @Override
    protected final boolean isOvershootingAtEnd(final AbstractDragTabsEventHandler.DragState dragState,
                                                final AbstractItemIterator iterator) {
        if (getTabSwitcher().getCount() <= 1) {
            return dragState != AbstractDragTabsEventHandler.DragState.DRAG_TO_START;
        } else {
            AbstractItem lastItem = iterator.getItem(getTabSwitcher().getCount() - 1);
            AbstractItem predecessor = iterator.getItem(getTabSwitcher().getCount() - 2);
            return Math.round(predecessor.getTag().getPosition()) >=
                    Math.round(calculateMaxTabSpacing(lastItem));
        }
    }

    @Override
    protected final float calculateMaxEndPosition(final int index) {
        float defaultMaxTabSpacing = calculateMaxTabSpacing(null);
        int selectedTabIndex = getTabSwitcher().getSelectedTabIndex();

        if (selectedTabIndex > index) {
            AbstractItemIterator iterator =
                    new ItemIterator.Builder(getTabSwitcher(), tabViewRecycler).create();
            AbstractItem selectedItem = iterator.getItem(selectedTabIndex);
            float selectedTabSpacing = calculateMaxTabSpacing(selectedItem);
            return (float) (((double)getTabSwitcher().getCount() - 2 - index) * defaultMaxTabSpacing +
                                selectedTabSpacing);
        }

        return (getTabSwitcher().getCount() - 1 - index) * defaultMaxTabSpacing;
    }

    @Override
    protected final float calculateSuccessorPosition(final AbstractItem item,
                                                     final AbstractItem predecessor) {
        float predecessorPosition = predecessor.getTag().getPosition();
        float maxTabSpacing = calculateMaxTabSpacing(item);
        return calculateSuccessorPosition(predecessorPosition, maxTabSpacing);
    }

    @Override
    protected final float calculatePredecessorPosition(final AbstractItem item,
                                                       final AbstractItem successor) {
        float successorPosition = successor.getTag().getPosition();
        return (float) ((double)successorPosition + calculateMaxTabSpacing(successor));
    }


    @Override
    public final ComponentContainer getTabContainer() {
        return tabContainer;
    }


    @Override
    public final Toolbar[] getToolbars() {
        return toolbar != null ? new Toolbar[]{toolbar} : null;
    }

    @Override
    public final void onDecoratorChanged(final TabSwitcherDecorator decorator) {
        adaptDecorator();
        super.onDecoratorChanged(decorator);
    }

    @Override
    public final void onSwitcherShown() {
        getLogger().logInfo(getClass(), "Showed tab switcher");
        animateShowSwitcher(-1, -1);
    }

    @Override
    public final void onSwitcherHidden() {
        getLogger().logInfo(getClass(), "Hid tab switcher");
        animateHideSwitcher();
    }

    @Override
    public final void onSelectionChanged(final int previousIndex, final int index,
                                         final Tab selectedTab,
                                         final boolean switcherHidden) {
        getLogger().logInfo(getClass(), "Selected tab at index " + index);

        if (switcherHidden) {
            animateHideSwitcher();
        } else {
            tabViewRecycler
                    .remove(TabItem.create(getTabSwitcher(), tabViewRecycler, previousIndex));
            tabViewRecycler.inflate(TabItem.create(getTabSwitcher(), tabViewRecycler, index));
        }
    }

    @Override
    public final void onTabAdded(final int index, final Tab tab,
                                 final int previousSelectedTabIndex, final int selectedTabIndex,
                                 final boolean selectionChanged,
                                 final boolean switcherVisibilityChanged,
                                 final Animation animation) {
        LogUtil.loge("=== onTabAdded");
        getLogger().logInfo(getClass(),
                "Added tab at index " + index + " using a " + animation.getClass().getSimpleName());

        if (animation instanceof PeekAnimation && getModel().getCount() > 1) {
            Condition.INSTANCE.ensureTrue(switcherVisibilityChanged,
                    animation.getClass().getSimpleName() +
                            " not supported when the tab switcher is shown");
            PeekAnimation peekAnimation = (PeekAnimation) animation;
            AbstractItem item = TabItem.create(getModel(), 0, tab);
            inflateView(item, createPeekLayoutListener(item, peekAnimation));
        } else if (animation instanceof RevealAnimation && switcherVisibilityChanged) {
            AbstractItem item = TabItem.create(getModel(), 0, tab);
            RevealAnimation revealAnimation = (RevealAnimation) animation;
            inflateView(item, createRevealLayoutListener(item, revealAnimation));
        } else {
            addAllTabs(index, new Tab[]{tab}, animation);
        }

        adaptEmptyView(
                getModel().isSwitcherShown() ? getModel().getEmptyViewAnimationDuration() : 0);
    }

    @Override
    public final void onAllTabsAdded(final int index, final Tab[] tabs,
                                     final int previousSelectedTabIndex, final int selectedTabIndex,
                                     final boolean selectionChanged,
                                     final Animation animation) {
        Condition.INSTANCE.ensureTrue(animation instanceof SwipeAnimation,
                animation.getClass().getSimpleName() + " not supported for adding multiple tabs");
        getLogger().logInfo(getClass(),
                "Added " + tabs.length + " tabs at index " + index + " using a " +
                        animation.getClass().getSimpleName());
        addAllTabs(index, tabs, animation);
    }

    @Override
    public final void onTabRemoved(final int index, final Tab tab,
                                   final int previousSelectedTabIndex, final int selectedTabIndex,
                                   final boolean selectionChanged,
                                   final Animation animation) {
        LogUtil.loge("=== onTabRemoved ");
        Condition.INSTANCE.ensureTrue(animation instanceof SwipeAnimation,
                animation.getClass().getSimpleName() + " not supported for removing tabs");
        getLogger().logInfo(getClass(), "Removed tab at index " + index + " using a " +
                animation.getClass().getSimpleName());
        AbstractItem removedItem = TabItem.create(getModel(), tabViewRecycler, index, tab);

        if (!getModel().isSwitcherShown()) {
            tabViewRecycler.remove(removedItem);

            if (getModel().isEmpty()) {
                toolbar.setAlpha(getModel().areToolbarsShown() ? 1 : 0);
            } else if (selectionChanged) {
                tabViewRecycler.inflate(
                        TabItem.create(getTabSwitcher(), tabViewRecycler, selectedTabIndex));
            }
        } else {
            adaptStackOnSwipe(removedItem, removedItem.getIndex(), getModel().getCount());
            removedItem.getTag().setClosing(true);
            SwipeAnimation swipeAnimation =
                    animation instanceof SwipeAnimation ? (SwipeAnimation) animation :
                            new SwipeAnimation.Builder().create();

            if (removedItem.isInflated()) {
                animateRemove(removedItem, swipeAnimation);
            } else {
                boolean start = isStackedAtStart(index);
                AbstractItem predecessor =
                        TabItem.create(getTabSwitcher(), tabViewRecycler, index - 1);
                Pair<Float, State> pair = start ?
                        calculatePositionAndStateWhenStackedAtStart(getModel().getCount(), index,
                                predecessor) : calculatePositionAndStateWhenStackedAtEnd(index);
                removedItem.getTag().setPosition(pair.f);
                removedItem.getTag().setState(pair.s);
                inflateAndUpdateView(removedItem, false,
                        createRemoveLayoutListener(removedItem, swipeAnimation));
            }
        }

        adaptEmptyView(
                getModel().isSwitcherShown() ? getModel().getEmptyViewAnimationDuration() : 0);
    }

    @Override
    public final void onAllTabsRemoved(final Tab[] tabs,
                                       final Animation animation) {
        Condition.INSTANCE.ensureTrue(animation instanceof SwipeAnimation,
                animation.getClass().getSimpleName() + " not supported for removing tabs ");
        getLogger().logInfo(getClass(),
                "Removed all tabs using a " + animation.getClass().getSimpleName());

        if (!getModel().isSwitcherShown()) {
            tabViewRecycler.removeAll();
            toolbar.setAlpha(getModel().areToolbarsShown() ? 1 : 0);
        } else {
            SwipeAnimation swipeAnimation =
                    animation instanceof SwipeAnimation ? (SwipeAnimation) animation :
                            new SwipeAnimation.Builder().create();
            AbstractItemIterator iterator =
                    new ArrayItemIterator.Builder(getModel(), tabViewRecycler, tabs, 0)
                            .reverse(true).create();
            AbstractItem item;
            int delayMultiplier = 0;

            while ((item = iterator.next()) != null) {
                AbstractItem previous = iterator.previous();

                if (item.getTag().getState() == State.FLOATING ||
                        (previous != null && previous.getTag().getState() == State.FLOATING)) {
                    delayMultiplier++;
                }

                if (item.isInflated()) {
                    animateSwipe(item, true, delayMultiplier, swipeAnimation,
                            !iterator.hasNext() ? createClearAnimationListener() : null);
                }
            }
        }

        adaptEmptyView(
                getModel().isSwitcherShown() ? getModel().getEmptyViewAnimationDuration() : 0);
    }

    @Override
    public final void onPaddingChanged(final int left, final int top, final int right,
                                       final int bottom) {
        adaptToolbarMargin();
    }

    @Override
    public final void onApplyPaddingToTabsChanged(final boolean applyPaddingToTabs) {

    }

    @Override
    public void onEmptyViewChanged(Component view, long animationDuration) {
        if (getModel().isEmpty()) {
            adaptEmptyView(0);
        }
    }

    @Override
    public final void onWindowBound() {
        if (getModel().isSwitcherShown()) {
            AbstractItem[] items = calculateInitialItems(getModel().getReferenceTabIndex(),
                    getModel().getReferenceTabPosition());
            AbstractItemIterator iterator = new InitialItemIteratorBuilder(items).create();
            AbstractItem item;

            while ((item = iterator.next()) != null) {
                if (item.isVisible()) {
                    inflateAndUpdateView(item, false, createBottomMarginLayoutListener(item));
                }
            }
        } else if (getModel().getSelectedTab() != null) {
            AbstractItem item = TabItem.create(getTabSwitcher(), tabViewRecycler,
                    getModel().getSelectedTabIndex());
            tabViewRecycler.inflate(item);
        }

        boolean showToolbar = getModel().areToolbarsShown() &&
                (getModel().isEmpty() || getModel().isSwitcherShown());
        toolbar.setAlpha(showToolbar ? 1 : 0);
        toolbar.setVisibility(showToolbar ? Component.VISIBLE : Component.INVISIBLE);
        adaptEmptyView(0);
    }

    @Override
    public void onWindowUnbound() {

    }

    @Override
    public final void onRevertStartOvershoot() {
        animateRevertStartOvershoot();
        getLogger().logVerbose(getClass(), "Reverting overshoot at the start");
    }

    @Override
    public final void onRevertEndOvershoot() {
        animateRevertEndOvershoot();
        getLogger().logVerbose(getClass(), "Reverting overshoot at the end");
    }

    public final void onStartOvershoot(final float position) {
        startOvershoot(position);
        getLogger().logVerbose(getClass(),
                "Overshooting at the start using a position of " + position + " pixels");
    }

    @Override
    public final void onTiltOnStartOvershoot(final float angle) {
        tiltOnStartOvershoot(angle);
        getLogger().logVerbose(getClass(),
                "Tilting on start overshoot using an angle of " + angle + " degrees");
    }

    @Override
    public final void onTiltOnEndOvershoot(final float angle) {
        tiltOnEndOvershoot(angle);
        getLogger().logVerbose(getClass(),
                "Tilting on end overshoot using an angle of " + angle + " degrees");
    }

    @Override
    public final void onSwipe(final TabItem tabItem, final float distance) {
        swipe(tabItem, distance);
        getLogger().logVerbose(getClass(),
                "Swiping tab at index " + tabItem.getIndex() + ". Current swipe distance is " +
                        distance + " pixels");
    }

    @Override
    public final void onSwipeEnded(final TabItem tabItem, final boolean remove,
                                   final float velocity) {
        if (remove) {
            SwipeAnimation.SwipeDirection direction =
                    getArithmetics().getPosition(Arithmetics.Axis.ORTHOGONAL_AXIS, tabItem) < 0 ?
                            SwipeAnimation.SwipeDirection.LEFT_OR_TOP : SwipeAnimation.SwipeDirection.RIGHT_OR_BOTTOM;
            long animationDuration =
                    velocity > 0 ? Math.round((calculateSwipePosition() / velocity) * 1000) : -1;
            Animation animation = new SwipeAnimation.Builder().setDirection(direction)
                    .setDuration(animationDuration).create();
            getModel().removeTab(tabItem.getTab(), animation);
        } else {
            animateSwipe(tabItem, false, 0, new SwipeAnimation.Builder().create(),
                    createSwipeAnimationListener(tabItem));
        }

        getLogger().logVerbose(getClass(),
                "Ended swiping tab at index " + tabItem.getIndex() + ". Tab will " +
                        (remove ? "" : "not ") + "be removed");
    }

    @Override
    public final void onSwitchingBetweenTabs(final int selectedTabIndex, final float distance) {
        LogUtil.loge("=== onSwitchingBetweenTabs");
        TabItem tabItem = TabItem.create(getModel(), getTabViewRecycler(), selectedTabIndex);

        if (distance == 0 || (distance > 0 && selectedTabIndex < getModel().getCount() - 1) ||
                (distance < 0 && selectedTabIndex > 0)) {
            getArithmetics().setPosition(Arithmetics.Axis.X_AXIS, tabItem, distance);
            float position = getArithmetics().getPosition(Arithmetics.Axis.X_AXIS, tabItem);

            if (distance != 0) {
                TabItem neighbor = TabItem.create(getModel(), getTabViewRecycler(),
                        position > 0 ? selectedTabIndex + 1 : selectedTabIndex - 1);

                if (Math.abs(position) >= swipedTabDistance) {
                    inflateView(neighbor, createSwipeNeighborLayoutListener(neighbor, distance));
                } else {
                    getTabViewRecycler().remove(neighbor);
                }
            }
        } else {
            float position = (float) Math.pow(Math.abs(distance), 0.75);
            position = distance < 0 ? position * -1 : position;
            getArithmetics().setPosition(Arithmetics.Axis.X_AXIS, tabItem, position);
        }

        getLogger().logVerbose(getClass(), "Swiping content of tab at index " + selectedTabIndex +
                ". Current swipe distance is " + distance + " pixels");
    }

    @Override
    public final void onSwitchingBetweenTabsEnded(final int selectedTabIndex,
                                                  final int previousSelectedTabIndex,
                                                  final boolean selectionChanged,
                                                  final float velocity,
                                                  final long animationDuration) {
        TabItem selectedTabItem =
                TabItem.create(getModel(), getTabViewRecycler(), selectedTabIndex);
        animateSwipe(selectedTabItem, 0, true, animationDuration, velocity);
        TabItem neighbor = null;
        boolean left = false;

        if (selectionChanged) {
            neighbor = TabItem.create(getModel(), getTabViewRecycler(), previousSelectedTabIndex);
            left = selectedTabIndex < previousSelectedTabIndex;
        } else if (getArithmetics().getPosition(Arithmetics.Axis.X_AXIS, selectedTabItem) > 0) {
            if (selectedTabIndex + 1 < getModel().getCount()) {
                neighbor = TabItem.create(getModel(), getTabViewRecycler(), selectedTabIndex + 1);
                left = true;
            }
        } else {
            if (selectedTabIndex - 1 >= 0) {
                neighbor = TabItem.create(getModel(), getTabViewRecycler(), selectedTabIndex - 1);
            }
        }

        if (neighbor != null && neighbor.isInflated()) {
            float width = getArithmetics().getSize(Arithmetics.Axis.X_AXIS, neighbor);
            float targetPosition =
                    (float) (left ? ((double)width + swipedTabDistance) * -1 : (double)width + swipedTabDistance);
            animateSwipe(neighbor, targetPosition, false, animationDuration, velocity);
        }
    }

    @Override
    public final void onPulledDown() {
        getModel().removeListener(this);
        getModel().showSwitcher();
        getModel().addListener(this);

        if (getTabSwitcher().getLayout() == Layout.PHONE_LANDSCAPE) {
            animateShowSwitcher(-1, -1);
        } else {
            animateShowSwitcher(getModel().getSelectedTabIndex(), 0);
        }

        getDragHandler().getDragHelper().reset();
        getDragHandler().setPointerId(0);
        getDragHandler().setDragState(AbstractDragTabsEventHandler.DragState.PULLING_DOWN);
    }
}
