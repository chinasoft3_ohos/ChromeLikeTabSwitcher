/*
 * Copyright 2016 - 2020 Michael Rapp
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package de.mrapp.tabswitcher.layout.phone;


import de.mrapp.tabswitcher.TabSwitcher;
import de.mrapp.tabswitcher.layout.AbstractTabViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;

/**
 * A view holder, which allows to store references to the views, a tab of a {@link TabSwitcher}
 * consists of, when using the smartphone layout.
 *
 * @author Michael Rapp
 * @since 0.1.0
 */
public class PhoneTabViewHolder extends AbstractTabViewHolder {

    public DirectionalLayout tabContainer;

    /**
     * The view group, which contains the view, which is associated with a tab.
     */
    public ComponentContainer contentContainer;

    /**
     * The view, which is associated with a tab.F
     */
    public Component content;

    /**
     * The image view, which is used to display the preview of a tab.
     */
    public Image previewImageView;

    /**
     * The view, which is used to display a border around the preview of a tab.
     */
    public Component borderView;

}