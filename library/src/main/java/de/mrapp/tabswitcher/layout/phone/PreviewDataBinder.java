package de.mrapp.tabswitcher.layout.phone;

import de.mrapp.tabswitcher.Tab;
import de.mrapp.tabswitcher.model.Model;
import de.mrapp.tabswitcher.model.TabItem;
import de.mrapp.tabswitcher.util.LogUtil;
import de.mrapp.util.Condition;
import de.mrapp.ohos_util.multithreading.AbstractDataBinder;
import de.mrapp.ohos_util.view.ViewRecycler;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.eventhandler.EventRunner;

import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.utils.LruBuffer;
import ohos.utils.Pair;

/**
 * A data binder, which allows to asynchronously render preview images of tabs and display them
 * afterwards.
 */
public class PreviewDataBinder extends AbstractDataBinder<PixelMap, Tab, Image, TabItem> {

    /**
     * The parent view of the tab switcher, the tabs belong to.
     */
    private final ComponentContainer parent;

    /**
     * The view recycler, which is used to inflate the views, which are associated with tabs.
     */
    private final ViewRecycler<Tab, Void> contentViewRecycler;

    /**
     * The model of the tab switcher, the tabs belong to.
     */
    private final Model model;

    public PreviewDataBinder(final ComponentContainer parent, final ViewRecycler<Tab, Void> contentViewRecycler, final Model model) {
        super(parent.getContext().getApplicationContext(), new LruBuffer<Tab, PixelMap>(7));
        Condition.INSTANCE.ensureNotNull(parent, "The parent may not be null");
        Condition.INSTANCE
                .ensureNotNull(contentViewRecycler, "The content view recycler may not be null");
        this.parent = parent;
        this.contentViewRecycler = contentViewRecycler;
        this.model = model;
    }

    @Override
    protected void onPreExecute(Image view, TabItem... params) {
        LogUtil.loge("=== onPreExecute PreviewDataBinder ");
        TabItem tabItem = params[0];
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) tabItem.getViewHolder();
        Component component = viewHolder.content;

        Tab tab = tabItem.getTab();

        if (component == null) {
            Pair<Component, ?> pair = contentViewRecycler.inflate(tab, viewHolder.contentContainer);
            component = pair.f;
        } else {
            contentViewRecycler.getAdapter().onShowView(getContext(), component, tab, false);
        }

        viewHolder.content = component;
    }

    @Override
    protected PixelMap doInBackground(Tab key, TabItem... params) {
        if (EventRunner.current() == null) {
            EventRunner.create();
        }

        TabItem tabItem = params[0];
        PhoneTabViewHolder viewHolder = (PhoneTabViewHolder) tabItem.getViewHolder();
        Component content = viewHolder.content;
        viewHolder.content = null;
        int width = parent.getWidth();
        int height = parent.getHeight();
        content.estimateSize(Component.EstimateSpec.getSizeWithMode(width, Component.EstimateSpec.PRECISE),
                Component.EstimateSpec.getSizeWithMode(height, Component.EstimateSpec.PRECISE));
        content.setLayoutConfig(new ComponentContainer.LayoutConfig(content.getEstimatedWidth(), content.getEstimatedHeight()));
        //将content转为预览PixelMap, api中暂无对应方法
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(width, height);
        PixelMap pixelMap = PixelMap.create(options);
        Canvas canvas = new Canvas();
        PixelMapElement element = new PixelMapElement(pixelMap);
        element.drawToCanvas(canvas);
//        pixelMap = foregroundElement.getPixelMap();
        return pixelMap;
    }

    @Override
    protected void onPostExecute(Image view, PixelMap data, long duration, TabItem... params) {

        view.setPixelMap(data);

        if (data != null) {
            //预览页面的显示和隐藏
            boolean useFadeAnimation = duration > model.getTabPreviewFadeThreshold();
            view.setAlpha(useFadeAnimation ? 0f : 1f);
            view.setVisibility(Component.VISIBLE);

            if (useFadeAnimation) {
                view.createAnimatorProperty().alpha(1f).setDuration(model.getTabPreviewFadeDuration())
                        .setCurveType(AnimatorProperty.CurveType.ACCELERATE_DECELERATE).start();
            }
        } else {
            view.setVisibility(Component.INVISIBLE);
        }

        view.setVisibility(data != null ? Component.VISIBLE : Component.HIDE);
        TabItem tabItem = params[0];
        contentViewRecycler.remove(tabItem.getTab());
    }

}
