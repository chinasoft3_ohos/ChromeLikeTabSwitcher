package de.mrapp.tabswitcher.model;

import de.mrapp.tabswitcher.TabSwitcher;
import de.mrapp.util.Condition;
import ohos.agp.components.Component;

public abstract class AbstractItem {

    /**
     * The index of the item.
     */
    private final int index;

    /**
     * The view, which is used to visualize the item.
     */
    private Component view;

    /**
     * The tag, which is associated with the item.
     */
    private Tag tag;

    /**
     * Creates a new item, which contains information about a child view of a {@link TabSwitcher}.
     *
     * @param index The index of the item as an {@link Integer} value. The index must be at least 0
     */
    public AbstractItem(final int index) {
        Condition.INSTANCE.ensureAtLeast(index, 0, "The index must be at least 0");
        this.index = index;
        this.view = null;
        this.tag = new Tag();
    }

    /**
     * Returns the index of the item. The index allows to identify the item's view among all child
     * views, which are contained by the tab switcher.
     *
     * @return The index of the item as an {@link Integer} value. The index must be at least 0
     */
    public final int getIndex() {
        return index;
    }

    /**
     * Returns the view, which is used to visualize the item.
     *
     * @return The view, which is used to visualize the item, as an instance of the class {@link
     * Component} or null, if no such view is currently inflated
     */
    public final Component getView() {
        return view;
    }

    /**
     * Sets the view, which is used to visualize the item.
     *
     * @param view The view, which should be set, as an instance of the class {@link Component} or null, if
     *             no view should be set
     */
    public final void setView(final Component view) {
        this.view = view;
    }

    /**
     * Returns the tag, which is associated with the item.
     *
     * @return The tag as an instance of the class {@link Tag}. The tag may not be null
     */

    public final Tag getTag() {
        return tag;
    }

    /**
     * Sets the tag, which is associated with the item.
     *
     * @param tag The tag, which should be set, as an instance of the class {@link Tag}. The tag may
     *            not be null
     */
    public final void setTag(final Tag tag) {
        Condition.INSTANCE.ensureNotNull(tag, "The tag may not be null");
        this.tag = tag;
    }

    /**
     * Returns, whether the item is currently visible, or not.
     *
     * @return True, if the item is currently visible, false otherwise
     */
    public final boolean isVisible() {
        return tag.getState() != State.HIDDEN || tag.isClosing();
    }

    /**
     * Returns, whether a view, which is used to visualize the item, is currently inflated, or not.
     *
     * @return True, if a view, which is used to visualize the item, is currently inflated, false
     * otherwise
     */
    public boolean isInflated() {
        return view != null;
    }

}
