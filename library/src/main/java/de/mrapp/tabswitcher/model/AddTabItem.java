package de.mrapp.tabswitcher.model;

import de.mrapp.tabswitcher.TabSwitcher;
import de.mrapp.ohos_util.view.AttachedViewRecycler;
import ohos.agp.components.Component;

public class AddTabItem extends AbstractItem {
    /**
     * Creates a new item, which contains information about a child view of a {@link TabSwitcher}.
     *
     * @param index The index of the item as an {@link Integer} value. The index must be at least 0
     */
    public AddTabItem(int index) {
        super(index);
    }

    /**
     * Creates a new item, which contains information about a button of a {@link TabSwitcher}, which
     * allows to add a new tab.
     *
     * @param viewRecycler
     *         The view recycler, which is used to reuse the views, which are used to visualize
     *         tabs, as an instance of the class AttachedViewRecycler. The view recycler may not be
     *         null
     * @return The item, which has been created, as an instance of the class {@link AddTabItem}. The
     * item may not be null
     */
    
    public static AddTabItem create(
             final AttachedViewRecycler<AbstractItem, ?> viewRecycler) {
        AddTabItem addTabItem = new AddTabItem(0);
        Component view = viewRecycler.getView(addTabItem);

        if (view != null) {
            addTabItem.setView(view);
            Tag tag = (Tag) view.getTag();

            if (tag != null) {
                addTabItem.setTag(tag);
            }
        }

        return addTabItem;
    }

    @Override
    public final String toString() {
        return "AddTabItem [index = " + getIndex() + "]";
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + getIndex();
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == null)
            return false;
        if (obj.getClass() != getClass())
            return false;
        AddTabItem other = (AddTabItem) obj;
        return getIndex() == other.getIndex();
    }
}
