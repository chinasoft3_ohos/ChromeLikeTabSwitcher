package de.mrapp.tabswitcher.model;

import ohos.utils.PacMap;

public interface Restorable {
    /**
     * Saves the current state.
     *
     * @param outState
     *         The bundle, which should be used to store the saved state, as an instance of the
     *         class {@link ohos.utils.PacMap}. The bundle may not be null
     */
    void saveInstanceState(PacMap outState);

    /**
     * Restores a previously saved state.
     *
     * @param savedInstanceState
     *         The saved state as an instance of the class {@link ohos.utils.PacMap} or null, if no saved state
     *         is available
     */
    void restoreInstanceState(PacMap savedInstanceState);
}
