/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.tabswitcher.snackbar;


import ohos.agp.text.Font;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

class Snack implements Sequenceable {
    final String mMessage;

    final String mActionMessage;

    final int mActionIcon;

    final Sequenceable mToken;

    final short mDuration;

    final int mBtnTextColor;

    final int mBackgroundColor;

    final int mHeight;

    Font mTypeface;

    /**
     * 构造
     * @param message message
     * @param actionMessage actionMessage
     * @param actionIcon actionIcon
     * @param token token
     * @param duration duration
     * @param textColor textColor
     * @param backgroundColor backgroundColor
     * @param height height
     * @param typeFace typeFace
     */
    public Snack(String message, String actionMessage, int actionIcon,Sequenceable token, short duration, int textColor, int backgroundColor, int height, Font typeFace) {
        mMessage = message;
        mActionMessage = actionMessage;
        mActionIcon = actionIcon;
        mToken = token;
        mDuration = duration;
        mBtnTextColor = textColor;
        mBackgroundColor = backgroundColor;
        mHeight = height;
        mTypeface = typeFace;
    }

    /**
     * 构造
     * @param parcel parcel
     */
    public Snack(Parcel parcel) {
        mMessage = parcel.readString();
        mActionMessage = parcel.readString();
        mActionIcon = parcel.readInt();
        mToken = parcel.readSequenceableList(Sequenceable.class).get(0);
        mDuration = (short) parcel.readInt();
        mBtnTextColor = parcel.readInt();
        mBackgroundColor = parcel.readInt();
        mHeight = parcel.readInt();
        mTypeface = (Font) parcel.readValue();
    }


    /**
     * writes data to parcel
     * @param out out
     * @param flags flags
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mMessage);
        out.writeString(mActionMessage);
        out.writeInt(mActionIcon);
        out.writeSequenceable(mToken);
        out.writeInt((int) mDuration);
        out.writeInt(mBtnTextColor);
        out.writeInt(mBackgroundColor);
        out.writeInt(mHeight);
        out.writeValue(mTypeface);
    }

    /**
     * describeContents
     * @return int
     */
    public int describeContents() {
        return 0;
    }

    /**
     * creates snack array
     */
    public static final Producer<Snack> CREATOR = new Producer<Snack>() {

        /**
         * 通过Parcel创建
         * @param in Parcel
         * @return Snack
         */
        public Snack createFromParcel(Parcel in) {
            return new Snack(in);
        }

        /**
         * 新数组newArray
         * @param size 数组大小
         * @return Snack[]
         */
        public Snack[] newArray(int size) {
            return new Snack[size];
        }
    };

    @Override
    public boolean marshalling(Parcel parcel) {
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }
}
