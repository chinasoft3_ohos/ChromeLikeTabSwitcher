package de.mrapp.tabswitcher.util;

public class DimensionUtil {

    public static float parseDimension(String dimens) {
        float dimensFloat = 0f;
        String newDimens = dimens.replace("vp", "").replace("fp", "");
        try {
            dimensFloat = Float.parseFloat(newDimens);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return dimensFloat;
    }
}
