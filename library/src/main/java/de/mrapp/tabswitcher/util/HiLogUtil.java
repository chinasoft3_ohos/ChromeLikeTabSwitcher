package de.mrapp.tabswitcher.util;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class HiLogUtil {
    private static final int DOMAIN = 0x001;

    public static void info(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.info(logLabel, format, objects);
    }

    public static void debug(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.debug(logLabel, format, objects);
    }

    public static void error(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.error(logLabel, format, objects);
    }

    public static void warn(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.warn(logLabel, format, objects);
    }

    public static void fatal(String tag, String format, Object... objects) {
        HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.fatal(logLabel, format, objects);
    }
}
