package de.mrapp.tabswitcher.util;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.rpc.RemoteObject;

public class PixelUtil {
    private static Display display;

    public static void initContext(Context context) {
        display = DisplayManager.getInstance().getDefaultDisplay(context).get();
    }

    public static int screenWidth() {
        return display.getAttributes().width;
    }

    public static int screenHeight() {
        return display.getAttributes().height;
    }

    public static float fp2px(float fp) {
        float sca = display.getAttributes().scalDensity;
        return (int) (fp * sca + 0.5d * (fp >= 0 ? 1 : -1));
    }

    public static float vp2px(float vp) {
        float dpi = display.getAttributes().densityPixels;
        return (int) (vp * dpi + 0.5d * (vp >= 0 ? 1 : -1));
    }

    public static float px2vp(int px) {
        float dpi = display.getAttributes().densityPixels;
        return px / dpi;
    }
}
