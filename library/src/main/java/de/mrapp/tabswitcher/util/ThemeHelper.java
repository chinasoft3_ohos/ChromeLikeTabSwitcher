package de.mrapp.tabswitcher.util;

import de.mrapp.tabswitcher.Layout;
import de.mrapp.util.Condition;
import de.mrapp.ohos_util.ThemeUtil;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.Element;

public class ThemeHelper {

    private final Context context;

    private final int globalTheme;

    private final int phoneTheme;

    private final int tabletTheme;

    private int obtainThemeFromXmlAttributes(final Layout layout) {
        int result = layout == Layout.TABLET ? tabletTheme : phoneTheme;

        if (result == 0) {
            result = globalTheme;
        }

        if (result != 0) {
            return result;
        }

        throw new RuntimeException("layout result not found");
    }


    private int obtainThemeFromThemeAttributes(final Layout layout,
                                               final int themeResourceId) {
//        int resourceId = layout == Layout.TABLET ? R.attr.tabSwitcherThemeTablet :
//                R.attr.tabSwitcherThemePhone;
//        int result = ThemeUtil.getResId(context, themeResourceId, resourceId, 0);
//
//        if (result == 0) {
//            result = ThemeUtil.getResId(context, R.attr.tabSwitcherThemeGlobal, 0);
//
//            if (result == 0) {
//                result = R.style.TabSwitcher_Light;
//            }
//        }
        int result = 0;
        return result;
    }


    public ThemeHelper(final Context context, final int globalTheme, final int phoneTheme,
                       final int tabletTheme) {
        Condition.INSTANCE.ensureNotNull(context, "The context may not be null");
        this.context = context;
        this.globalTheme = globalTheme;
        this.phoneTheme = phoneTheme;
        this.tabletTheme = tabletTheme;
    }


    public final int getThemeResourceId(final Layout layout) {
        int themeResourceId;

        try {
            themeResourceId = obtainThemeFromXmlAttributes(layout);
        } catch (Exception e) {
            
            themeResourceId = obtainThemeFromThemeAttributes(layout, -1);
        }

        return themeResourceId;
    }


    public int getColor(final Layout layout, final int resourceId) {
        try {
            return ThemeUtil.getColor(context, resourceId);
        } catch (Exception e1) {
            int themeResourceId = getThemeResourceId(layout);

            try {
                return ThemeUtil.getColor(context, themeResourceId, resourceId);
            } catch (Exception e) {
                themeResourceId = obtainThemeFromThemeAttributes(layout, themeResourceId);
                return ThemeUtil.getColor(context, themeResourceId, resourceId);
            }
        }
    }


    public Color getColorStateList(final Layout layout,
                                   final int resourceId) {
        try {
            return ThemeUtil.getColorStateList(context, resourceId);
        } catch (Exception e1) {
            int themeResourceId = getThemeResourceId(layout);

            try {
                return ThemeUtil.getColorStateList(context, themeResourceId, resourceId);
            } catch (Exception e) {
                themeResourceId = obtainThemeFromThemeAttributes(layout, themeResourceId);
                return ThemeUtil.getColorStateList(context, themeResourceId, resourceId);
            }
        }
    }


    public Element getDrawable(final Layout layout, final int resourceId) {
        try {
            return ThemeUtil.getDrawable(context, resourceId);
        } catch (Exception e1) {
            int themeResourceId = getThemeResourceId(layout);

            try {
                return ThemeUtil.getDrawable(context, themeResourceId, resourceId);
            } catch (Exception e) {
                themeResourceId = obtainThemeFromThemeAttributes(layout, themeResourceId);
                return ThemeUtil.getDrawable(context, themeResourceId, resourceId);
            }
        }
    }


    public CharSequence getText(final Layout layout, final int resourceId) {
        try {
            return ThemeUtil.getText(context, resourceId);
        } catch (Exception e1) {
            int themeResourceId = getThemeResourceId(layout);

            try {
                return ThemeUtil.getText(context, themeResourceId, resourceId);
            } catch (Exception e) {
                themeResourceId = obtainThemeFromThemeAttributes(layout, themeResourceId);
                return ThemeUtil.getText(context, themeResourceId, resourceId);
            }
        }
    }

    public int getResourceId(final Layout layout, final int resourceId,
                             final int defaultValue) {
        int result = ThemeUtil.getResId(context, resourceId, 0);

        if (result == 0) {
            int themeResourceId = getThemeResourceId(layout);
            result = ThemeUtil.getResId(context, themeResourceId, resourceId, 0);

            if (result == 0) {
                themeResourceId = obtainThemeFromThemeAttributes(layout, themeResourceId);
                return ThemeUtil.getResId(context, themeResourceId, resourceId, defaultValue);
            }
        }

        return result;
    }

    public int getInteger(final Layout layout, final int resourceId,
                          final int defaultValue) {
        int result = ThemeUtil.getInt(context, resourceId, 0);

        if (result == 0) {
            int themeResourceId = getThemeResourceId(layout);
            result = ThemeUtil.getInt(context, themeResourceId, resourceId, 0);

            if (result == 0) {
                themeResourceId = obtainThemeFromThemeAttributes(layout, themeResourceId);
                return ThemeUtil.getInt(context, themeResourceId, resourceId, defaultValue);
            }
        }

        return result;
    }

}
