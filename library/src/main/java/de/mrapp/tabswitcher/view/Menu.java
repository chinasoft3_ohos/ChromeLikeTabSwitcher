package de.mrapp.tabswitcher.view;

import de.mrapp.tabswitcher.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.Resource;

public class Menu extends Image {
    public Menu(Context context) {
        this(context, null);
    }

    public Menu(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public Menu(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        setImageElement(convertResToElement(getContext(), ResourceTable.Media_more_menu));
    }

    public void setImageSrc(int drawableRes){
        setImageElement(convertResToElement(getContext(), drawableRes));
    }

    private Element convertResToElement(Context mContext, int drawableRes) {
        Element element = null;
        try {
            Resource resource = mContext.getResourceManager().getResource(drawableRes);
            element = new PixelMapElement(resource);
            return element;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return element;
    }

    public void clear() {
        setImageElement(null);
        if (getVisibility() != Component.HIDE) {
            setVisibility(Component.HIDE);
        }
    }
}
