package de.mrapp.tabswitcher.view;

import de.mrapp.tabswitcher.ResourceTable;
import de.mrapp.tabswitcher.TabSwitcher;
import de.mrapp.tabswitcher.dialog.MenuDialog;
import de.mrapp.tabswitcher.menu.OnMenuItemClickListener;
import de.mrapp.tabswitcher.util.DimensionUtil;
import de.mrapp.tabswitcher.util.PixelUtil;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

public class Toolbar extends DirectionalLayout implements Component.ClickedListener {
    private CharSequence mTitle;
    private Image navigationView;
    private Element navigationIcon;

    private Text titleTv;
    private TabSwitcherButton actionView;
    private Menu menuImage;

    private TabSwitcher mTabSwitcher;
    private int menuId;
    private OnMenuItemClickListener toolbarMenuItemListener;
    private MenuDialog menuDialog;

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public Toolbar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        ShapeElement shapeElement = new ShapeElement();
        RgbColor rgbColor = new RgbColor(0, 0, 0);
        shapeElement.setRgbColor(rgbColor);
//        setBackground(shapeElement);

        setOrientation(HORIZONTAL);
        setAlignment(LayoutAlignment.VERTICAL_CENTER);
        setHeight(LayoutConfig.MATCH_PARENT);
        setWidth(LayoutConfig.MATCH_PARENT);

        titleTv = new Text(getContext());
        titleTv.setTextSize((int) PixelUtil.fp2px(14));
        DirectionalLayout.LayoutConfig layoutConfig = new LayoutConfig();
        layoutConfig.width = 0;
        layoutConfig.height = LayoutConfig.MATCH_CONTENT;
        layoutConfig.weight = 1;
        titleTv.setLayoutConfig(layoutConfig);
        addComponent(titleTv);

        actionView = new TabSwitcherButton(getContext());
        DirectionalLayout.LayoutConfig actionConfig = new LayoutConfig();
        int tabHeight = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(getContext().getString(ResourceTable.String_tablet_tab_height)));
        actionConfig.height = tabHeight;
        actionConfig.width = tabHeight;
        addComponent(actionView, actionConfig);

        int menuHeight = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(getContext().getString(ResourceTable.String_menu_height)));
        DirectionalLayout.LayoutConfig menuConfig = new LayoutConfig();
        menuConfig.height = menuHeight;
        menuConfig.width = menuHeight;
        menuImage = new Menu(getContext());
        addComponent(menuImage, menuConfig);
    }

    public void setButtonColor(float matrix, Color textColor) {
        if (actionView == null)
            return;
        actionView.setButtonColor(matrix, textColor);
    }

    public void addMenuClickListener() {
        if (menuImage != null) {
            menuImage.setClickedListener(component -> {
                showMenu();
            });
        }
    }

    private int convertIntegerValue(Context context, int id) {
        int value = 0;
        try {
            value = context.getResourceManager().getElement(id).getInteger();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public CharSequence getTitle() {
        return mTitle;
    }

    public void setTitle(CharSequence title) {
        this.mTitle = title;
        if (titleTv != null) {
            titleTv.setText((String) title);
        }
    }

    public void setActionView(Element icon) {
        if (actionView != null) {
            actionView.setVisibility(icon != null ? Component.VISIBLE : Component.HIDE);
            if (icon != null) {
                actionView.setBackground(icon);
            }
        }
    }

    public TabSwitcherButton getActionView() {
        return actionView;
    }

    public Element getNavigationIcon() {
        return navigationIcon;
    }

    //添加navigationIcon
    public void setNavigationIcon(Element icon) {
        this.navigationIcon = icon;
        if (navigationView == null) {
            navigationView = new Image(getContext());
            navigationView.setVisibility(icon != null ? Component.VISIBLE : Component.HIDE);
            if (icon != null) {
                navigationView.setImageElement(icon);
            }
            DirectionalLayout.LayoutConfig navigationConfig = new LayoutConfig();
            int tabHeight = (int) PixelUtil.fp2px(DimensionUtil.parseDimension(getContext().getString(ResourceTable.String_tablet_tab_height)));
            navigationConfig.height = tabHeight;
            navigationConfig.width = tabHeight;
            addComponent(navigationView, 0, navigationConfig);
        } else {
            Component firstComponent = getComponentAt(0);
            if (firstComponent instanceof Image) {
                navigationView.setVisibility(icon != null ? Component.VISIBLE : Component.HIDE);
                if (icon != null) {
                    navigationView.setBackground(icon);
                }
                invalidate();
            }
        }
    }

    public void setNavigationOnClickListener(Component.ClickedListener toolbarNavigationIconListener) {
        if (navigationView != null) {
            navigationView.setClickedListener(toolbarNavigationIconListener);
        }
    }

    public Menu getMenu() {
        return menuImage;
    }

    public void inflateMenu(int menuId) {
        this.menuId = menuId;
    }

    //显示menu
    private void showMenu() {
        if (mTabSwitcher != null) {
            int screenWidth = (int) PixelUtil.vp2px(180);
            int menuHeight = (int) PixelUtil.vp2px(120);
            menuDialog = new MenuDialog(getContext(), menuImage, screenWidth, menuHeight, menuId);
            menuDialog.initListener(this::onClick);
            menuDialog.show();
        }
    }

    private void hideMenu() {
        if (menuDialog != null) {
            menuDialog.hide();
        }
    }


    public void setOnMenuItemClickListener(TabSwitcher tabSwitcher, OnMenuItemClickListener toolbarMenuItemListener) {
        this.mTabSwitcher = tabSwitcher;
        this.toolbarMenuItemListener = toolbarMenuItemListener;

    }

    @Override
    public void onClick(Component component) {
        hideMenu();
        if (toolbarMenuItemListener != null) {
            toolbarMenuItemListener.onMenuItemClick(component);
        }
    }
}
