package de.mrapp.ohos_util;

import ohos.aafwk.ability.Ability;

import java.io.File;

/**
 * An utility class, which provides static methods, which allow to start system apps.
 */
public final class AppUtil {
    /**
     * Creates a new utility class, which provides static methods, which allow to start apps.
     */
    private AppUtil(){

    }

    public static void startCameraApp(final Ability ability, final int requestCode,
                                      final File file) {
//        Condition.INSTANCE.ensureNotNull(file, "The file may not be null");
//        Condition.INSTANCE
//                .ensureFileIsNoDirectory(file, "The file must exist and must not be a directory");
//        startCameraApp(ability, requestCode, Uri.fromFile(file));
    }
}
