package de.mrapp.ohos_util;

import de.mrapp.util.Condition;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;


/**
 * An utility class, which provides static methods for handling instances of the class
 */
public final class ViewUtil {

    /**
     * Creates a new utility class, which provides static methods for handling instances of the
     */
    private ViewUtil() {

    }

    public static void setBackground(final Component component, final Element background) {
        Condition.INSTANCE.ensureNotNull(component, "The view may not be null");
        try {
            component.setBackground(background);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeOnGlobalLayoutListener( final ComponentTreeObserver observer,
                                                     final ComponentTreeObserver.WindowBoundListener listener) {
        Condition.INSTANCE.ensureNotNull(observer, "The view tree observer may not be null");


            try {
                observer.removeWindowBoundListener(listener);
            }catch (Exception e){
                e.printStackTrace();
            }
    }

    public static void removeFromParent( final Component component) {
        Condition.INSTANCE.ensureNotNull(component, "The view may not be null");

        try {
            component.release();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
