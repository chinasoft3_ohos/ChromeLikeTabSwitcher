/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mrapp.ohos_util.logging;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogUtil {
    private static Logger logger;

    private static Logger createLogger(String tag) {
        if (logger == null) {
            logger = Logger.getLogger(tag);
        }
        return logger;
    }

    public static void loge(String msg) {
        createLogger("log").log(Level.SEVERE, msg);
    }

    public static void loge(String tag, String msg) {
        createLogger(tag).log(Level.SEVERE, msg);
    }
}
