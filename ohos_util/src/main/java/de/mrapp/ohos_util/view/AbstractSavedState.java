package de.mrapp.ohos_util.view;

import de.mrapp.util.Condition;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

public abstract class AbstractSavedState implements Sequenceable {

    private final Sequenceable superState;

    protected AbstractSavedState(final Parcel source) {
        Condition.INSTANCE.ensureNotNull(source, "The parcel may not be null");
        superState = (Sequenceable) source.readParcelableEx(getClass().getClassLoader());
    }

    protected AbstractSavedState( final Sequenceable superState) {
        this.superState = superState;
    }

    public final Sequenceable getSuperState() {
        return superState;
    }

    public void writeToParcel(final Parcel out) {
        out.writeSequenceable(superState);
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }
}
